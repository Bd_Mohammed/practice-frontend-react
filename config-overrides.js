import { addBabelPlugin, override } from 'customize-cra';

module.exports = override(
	addBabelPlugin([
		'babel-plugin-root-import',
		{
			rootPathSuffix: 'src'
		}
	])
);
