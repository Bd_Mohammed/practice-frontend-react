#!/usr/bin/env sh
. "$(dirname -- "$0")/_/husky.sh"

echo "lint-staged $1";

yarn run lint-staged && yarn run test
exit 1
