import { Mode } from '../actions/actionTypes';
import { initialState } from '../states';

import modeReducer from './modeReducers';

export interface IAction {
	type: number;
	payload: Record<string, unknown>;
	actionType: number;
}

export const rootReducer = (state = initialState, action: IAction) => {
	switch (action.actionType) {
		case Mode.CHANGE_MODE:
			return modeReducer(state, action);
		default:
			return state;
	}
};
