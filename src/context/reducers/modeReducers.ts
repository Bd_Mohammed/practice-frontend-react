import { Mode } from '../actions/actionTypes';
import { initialState } from '../states';

import { IAction } from '.';

const modeReducer = (state: any = initialState, action: IAction) => {
	switch (action.type) {
		case Mode.CHANGE_THEME:
			return {
				...state,
				css: { ...state.css, ...action.payload }
			};
		default:
			return state;
	}
};

export default modeReducer;
