import { Mode } from './actionTypes';

export const changeMode = (user: any) => {
	return {
		actionType: Mode.CHANGE_MODE,
		type: Mode.CHANGE_THEME,
		payload: user
	};
};

export const changeCssPrefix = (user: any) => {
	return {
		actionType: Mode.CHANGE_MODE,
		type: Mode.CHANGE_CSS_PREFIX,
		payload: user
	};
};
