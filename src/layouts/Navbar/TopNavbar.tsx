// import { useEffect, useRef } from "react";
// import { Nav, Container, Navbar } from "react-bootstrap";
// import { Outlet, useNavigate } from "react-router-dom";
// // import NavBar from "../../components/ui/Navbar";

// export const TopNavBar = () => {
//   const navigate = useNavigate();
//   const SingUp = useRef<HTMLDivElement | null>(null);
//   const SingIn = useRef<HTMLDivElement | null>(null);

//   const SignInDiv = () => {
//     SingUp?.current?.classList?.add("mystyle");
//     SingIn?.current?.classList?.remove("mystyle");
//     navigate("/signin");
//   };
//   const SignUpDiv = () => {
//     SingUp?.current?.classList?.remove("mystyle");
//     SingIn?.current?.classList?.add("mystyle");
//     navigate("/signup");
//   };

//   useEffect(() => {
//     switch (window.location.pathname) {
//       case "/signin": {
//         SingUp?.current?.classList?.add("mystyle");
//         SingIn?.current?.classList?.remove("mystyle");
//         break;
//       }
//       case "/signup": {
//         SingUp?.current?.classList?.remove("mystyle");
//         SingIn?.current?.classList?.add("mystyle");
//         break;
//       }
//       case "/": {
//         SingUp?.current?.classList?.remove("mystyle");
//         SingIn?.current?.classList?.add("mystyle");
//         break;
//       }
//     }
//   }, []);

//   return (
//     <>
//       <Navbar bg="light" expand="sm" variant="light" sticky="top" fixed="top">
//         <Container as={"div"} bsPrefix="container">
//           <Navbar.Brand href="#home">Blogs</Navbar.Brand>
//           <Navbar.Toggle aria-controls="basic-navbar-nav" />
//           <Navbar.Collapse id="basic-navbar-nav">
//             <Nav className="me-auto">
//             </Nav>
//             <div
//               id="myDIV"
//               ref={SingUp}
//               onClick={() => SignInDiv()}
//               className="assessmenttab"
//             >
//               Sign In
//             </div>
//             <div
//               id="myDIVin"
//               ref={SingIn}
//               onClick={() => SignUpDiv()}
//               className="assessmenttab"
//             >
//               Sign Up
//             </div>
//           </Navbar.Collapse>
//         </Container>
//       </Navbar>
//       <Outlet />
//     </>
//   );
// };
