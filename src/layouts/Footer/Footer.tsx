// import styled from "styled-components";
// import { IconComponent } from "../../components/ui/Icon";
// import ebay from "../../Images/EBay_logo.png";

// const BoxContainer = styled.div``;
// const Box = styled.div`
//   padding: 80px 60px;
//   background: black;
//   position: relative;
//   bottom: 0px;
//   width: 100%;

//   @media (max-width: 1000px) {
//     padding: 70px 30px;
//   }
// `;

// const Container = styled.div`
//   display: flex;
//   flex-direction: column;
//   justify-content: center;
//   max-width: 1000px;
//   margin: 50px auto;
//   /* background: red; */
// `;

// const Column = styled.div`
//   display: flex;
//   flex-direction: column;
//   text-align: left;
//   margin-left: 60px;
// `;

// const Row = styled.div`
//   display: grid;
//   grid-template-columns: repeat(auto-fill, minmax(185px, 1fr));
//   grid-gap: 20px;

//   @media (max-width: 1000px) {
//     grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
//   }
// `;

// const FooterLink = styled.a`
//   color: #fff;
//   margin-bottom: 20px;
//   font-size: 18px;
//   text-decoration: none;

//   &:hover {
//     color: green;
//     transition: 200ms ease-in;
//   }
// `;

// const Heading = styled.p`
//   font-size: 24px;
//   color: #fff;
//   margin-bottom: 40px;
//   font-weight: bold;
// `;

// const FooterHeading = styled.h1`
//   color: darkgrey;
//   text-align: center;
//   margin-top: -80px;
// `;

// const FooterLogo = styled.picture``;
// const FabIconSpan = styled.span`
//   margin-left: 10px;
// `;
// const FabIcon = styled(IconComponent)``;
// const Footer = () => {
//   return (
//     <BoxContainer >
//       <Box>
//         <FooterLogo>
//           <source srcSet={ebay} media="(min-width: 100px)" />
//           <img src={ebay} alt="ebay logo" />
//         </FooterLogo>
//         <FooterHeading>Ebay: Buy & Sell With A Good Reasons</FooterHeading>
//         <Container>
//           <Row>
//             <Column>
//               <Heading>About Us</Heading>
//               <FooterLink href="#">Aim</FooterLink>
//               <FooterLink href="#">Vision</FooterLink>
//               <FooterLink href="#">Testimonials</FooterLink>
//             </Column>
//             <Column>
//               <Heading>Services</Heading>
//               <FooterLink href="#">Writing</FooterLink>
//               <FooterLink href="#">Internships</FooterLink>
//               <FooterLink href="#">Coding</FooterLink>
//               <FooterLink href="#">Teaching</FooterLink>
//             </Column>
//             <Column>
//               <Heading>Contact Us</Heading>
//               <FooterLink href="#">Uttar Pradesh</FooterLink>
//               <FooterLink href="#">Ahemdabad</FooterLink>
//               <FooterLink href="#">Indore</FooterLink>
//               <FooterLink href="#">Mumbai</FooterLink>
//             </Column>
//             <Column>
//               <Heading>Social Media</Heading>
//               <FooterLink href="#">
//                 <FabIcon dataTestId="icon-facebook" className="fab fa-facebook-f">
//                   <FabIconSpan>
//                     Facebook
//                   </FabIconSpan>
//                 </FabIcon>
//               </FooterLink>
//               <FooterLink href="#">
//                 <FabIcon dataTestId="icon-instagram" className="fab fa-instagram">
//                   <FabIconSpan>
//                     Instagram
//                   </FabIconSpan>
//                 </FabIcon>
//               </FooterLink>
//               <FooterLink href="#">
//                 <FabIcon dataTestId='icon-twitter' className="fab fa-twitter">
//                   <FabIconSpan>
//                     Twitter
//                   </FabIconSpan>
//                 </FabIcon>
//               </FooterLink>
//               <FooterLink href="#">
//                 <FabIcon dataTestId='icon-youtube' className="fab fa-youtube">
//                   <FabIconSpan>
//                     Youtube
//                   </FabIconSpan>
//                 </FabIcon>
//               </FooterLink>
//             </Column>
//           </Row>
//         </Container>
//       </Box>
//     </BoxContainer >
//   );
// };
// export default Footer;
