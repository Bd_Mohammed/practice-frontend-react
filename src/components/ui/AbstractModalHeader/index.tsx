import useEventCallback from '@restart/hooks/useEventCallback';
import React from 'react';

import { CloseButtonVariant } from '../../../utils/helper';
import { CloseButton } from '../CloseButton';
import ModalContext from '../Modal/ModalContext';

interface AbstractModalHeaderProps
	extends React.HTMLAttributes<HTMLDivElement> {
	closeLabel?: string;
	closeVariant?: CloseButtonVariant;
	closeButton?: boolean;
	onHide?: () => void;
}

export const AbstractModalHeader: React.ForwardRefExoticComponent<
	AbstractModalHeaderProps & React.RefAttributes<HTMLDivElement>
> = React.forwardRef(
	(
		{
			closeLabel,
			closeVariant,
			closeButton,
			onHide,
			children,
			...props
		}: AbstractModalHeaderProps,
		ref
	) => {
		const modalContext = React.useContext(ModalContext);

		const handleClick = useEventCallback(() => {
			modalContext == null ? void 0 : modalContext.onHide();
			onHide == null ? void 0 : onHide();
		});
		return (
			<div
				ref={ref}
				{...props}
				children={[
					children,
					closeButton && (
						<CloseButton
							variant={closeVariant}
							onClick={handleClick}
							aria-label={closeLabel}
						/>
					)
				]}
			/>
		);
	}
);
