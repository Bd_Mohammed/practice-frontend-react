// import {
// 	IComponentPrefixRefForwardingComponent,
// 	IPrefixWithAsProps
// } from '../../../utils/helper';

import useCommittedRef from '@restart/hooks/useCommittedRef';
import useEventCallback from '@restart/hooks/useEventCallback';
import useTimeout from '@restart/hooks/useTimeout';
import useUpdateEffect from '@restart/hooks/useUpdateEffect';
import * as React from 'react';
import { useUncontrolled } from 'uncontrollable';

import { classnames } from '../../../utils/classnames';
import ElementChildren from '../../../utils/elementChildren';
import { useThemeIsRTL, useThemePrefix } from '../../../utils/ThemePrefix';
import { transitionEndListener } from '../../../utils/transitionEndListener';
import triggerBrowserReflow from '../../../utils/triggerBrowserReflow';
import Anchor from '../Anchor';
import { TransitionWrapper } from '../TransitionWrapper';

// type CarouselVariant = 'dark' | string;
// interface CarouselRef {
// 	element?: HTMLElement;
// 	prev: (e?: React.SyntheticEvent) => void;
// 	next: (e?: React.SyntheticEvent) => void;
// }
// interface CarouselProps
// 	extends IPrefixWithAsProps,
// 		Omit<React.HTMLAttributes<HTMLElement>, 'onSelect'> {
// 	slide?: boolean;
// 	fade?: boolean;
// 	controls?: boolean;
// 	indicators?: boolean;
// 	indicatorLabels?: string[];
// 	activeIndex?: number;
// 	onSelect?: (eventKey: number, event: Record<string, unknown> | null) => void;
// 	defaultActiveIndex?: number;
// 	onSlide?: (eventKey: number, direction: 'start' | 'end') => void;
// 	onSlid?: (eventKey: number, direction: 'start' | 'end') => void;
// 	interval?: number | null;
// 	keyboard?: boolean;
// 	pause?: 'hover' | false;
// 	wrap?: boolean;
// 	touch?: boolean;
// 	prevIcon?: React.ReactNode;
// 	prevLabel?: React.ReactNode;
// 	nextIcon?: React.ReactNode;
// 	nextLabel?: React.ReactNode;
// 	ref?: React.Ref<CarouselRef>;
// 	variant?: CarouselVariant;
// }

const SWIPE_THRESHOLD = 40;
const defaultProps: any = {
	slide: true,
	fade: false,
	controls: true,
	indicators: true,
	indicatorLabels: [],
	defaultActiveIndex: 0,
	interval: 5000,
	keyboard: true,
	pause: 'hover',
	wrap: true,
	touch: true,
	prevIcon: (
		<span
			aria-hidden={true}
			className="carousel-control-prev-icon"
		/>
	),
	prevLabel: 'Previous',
	nextIcon: (
		<span
			aria-hidden={true}
			className="carousel-control-next-icon"
		/>
	),
	nextLabel: 'Next'
};

function isVisible(element: any) {
	if (
		!element ||
		!element.style ||
		!element.parentNode ||
		!element.parentNode.style
	) {
		return false;
	}
	const elementStyle = getComputedStyle(element);
	return (
		elementStyle.display !== 'none' &&
		elementStyle.visibility !== 'hidden' &&
		getComputedStyle(element.parentNode as Element).display !== 'none'
	);
}
//IComponentPrefixRefForwardingComponent<'div', CarouselProps>
const Carousel = React.forwardRef((uncontrolledProps: any, ref) => {
	const {
		// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
		as: Component = 'div',
		componentPrefix,
		slide,
		fade,
		controls,
		indicators,
		indicatorLabels,
		activeIndex,
		onSelect,
		onSlide,
		onSlid,
		interval,
		keyboard,
		onKeyDown,
		pause,
		onMouseOver,
		onMouseOut,
		wrap,
		touch,
		onTouchStart,
		onTouchMove,
		onTouchEnd,
		prevIcon,
		prevLabel,
		nextIcon,
		nextLabel,
		variant,
		className,
		children,
		...props
	} = useUncontrolled(uncontrolledProps, {
		activeIndex: 'onSelect'
	});
	const prefix = useThemePrefix(componentPrefix, 'carousel');
	const isRTL = useThemeIsRTL();
	const nextDirectionRef = React.useRef<string | null>(null);
	const [direction, setDirection] = React.useState('next');
	const [paused, setPaused] = React.useState(false);
	const [isSliding, setIsSliding] = React.useState(false);
	const [renderedActiveIndex, setRenderedActiveIndex] = React.useState(
		activeIndex || 0
	);

	React.useEffect(() => {
		if (!isSliding && activeIndex !== renderedActiveIndex) {
			if (nextDirectionRef.current) {
				setDirection(nextDirectionRef.current);
			} else {
				setDirection((activeIndex || 0) > renderedActiveIndex ? 'next' : 'prev');
			}
			if (slide) {
				setIsSliding(true);
			}
			setRenderedActiveIndex(activeIndex || 0);
		}
	}, [activeIndex, isSliding, renderedActiveIndex, slide]);

	React.useEffect(() => {
		if (nextDirectionRef.current) {
			nextDirectionRef.current = null;
		}
	});

	let numChildren = 0;
	let activeChildInterval;

	// Iterate to grab all of the children's interval values
	// (and count them, too)
	ElementChildren.forEach(children, (child, index) => {
		++numChildren;
		if (index === activeIndex) {
			activeChildInterval = child.props.interval;
		}
	});
	const activeChildIntervalRef = useCommittedRef(activeChildInterval);
	const prev = React.useCallback(
		(event?: any) => {
			if (isSliding) {
				return;
			}
			let nextActiveIndex = renderedActiveIndex - 1;
			if (nextActiveIndex < 0) {
				if (!wrap) {
					return;
				}
				nextActiveIndex = numChildren - 1;
			}
			nextDirectionRef.current = 'prev';
			onSelect == null ? void 0 : onSelect(nextActiveIndex, event);
		},
		[isSliding, renderedActiveIndex, onSelect, wrap, numChildren]
	);

	// This is used in the setInterval, so it should not invalidate.
	const next = useEventCallback((event?: any) => {
		if (isSliding) {
			return;
		}
		let nextActiveIndex = renderedActiveIndex + 1;
		if (nextActiveIndex >= numChildren) {
			if (!wrap) {
				return;
			}
			nextActiveIndex = 0;
		}
		nextDirectionRef.current = 'next';
		onSelect == null ? void 0 : onSelect(nextActiveIndex, event);
	});
	const elementRef = React.useRef<HTMLElement | null>(null);
	React.useImperativeHandle(ref, () => ({
		element: elementRef.current as any,
		prev,
		next
	}));

	// This is used in the setInterval, so it should not invalidate.
	const nextWhenVisible = useEventCallback(() => {
		if (!document.hidden && isVisible(elementRef.current)) {
			if (isRTL) {
				prev();
			} else {
				next();
			}
		}
	});
	const slideDirection = direction === 'next' ? 'start' : 'end';
	useUpdateEffect(() => {
		if (slide) {
			// These callbacks will be handled by the <Transition> callbacks.
			return;
		}
		onSlide == null ? void 0 : onSlide(renderedActiveIndex, slideDirection);
		onSlid == null ? void 0 : onSlid(renderedActiveIndex, slideDirection);
	}, [renderedActiveIndex]);
	const orderClassName = `${prefix}-item-${direction}`;
	const directionalClassName = `${prefix}-item-${slideDirection}`;
	const handleEnter = React.useCallback(
		(node: any) => {
			triggerBrowserReflow(node);
			onSlide == null ? void 0 : onSlide(renderedActiveIndex, slideDirection);
		},
		[onSlide, renderedActiveIndex, slideDirection]
	);
	const handleEntered = React.useCallback(() => {
		setIsSliding(false);
		onSlid == null ? void 0 : onSlid(renderedActiveIndex, slideDirection);
	}, [onSlid, renderedActiveIndex, slideDirection]);
	const handleKeyDown = React.useCallback(
		(event: any) => {
			if (keyboard && !/input|textarea/i.test(event.target.tagName)) {
				switch (event.key) {
					case 'ArrowLeft':
						event.preventDefault();
						if (isRTL) {
							next(event);
						} else {
							prev(event);
						}
						return;
					case 'ArrowRight':
						event.preventDefault();
						if (isRTL) {
							prev(event);
						} else {
							next(event);
						}
						return;
					default:
				}
			}
			onKeyDown == null ? void 0 : onKeyDown(event);
		},
		[keyboard, onKeyDown, prev, next, isRTL]
	);
	const handleMouseOver = React.useCallback(
		(event: any) => {
			if (pause === 'hover') {
				setPaused(true);
			}
			onMouseOver == null ? void 0 : onMouseOver(event);
		},
		[pause, onMouseOver]
	);
	const handleMouseOut = React.useCallback(
		(event: any) => {
			setPaused(false);
			onMouseOut == null ? void 0 : onMouseOut(event);
		},
		[onMouseOut]
	);
	const touchStartXRef = React.useRef(0);
	const touchDeltaXRef = React.useRef(0);
	const touchUnpauseTimeout = useTimeout();
	const handleTouchStart = React.useCallback(
		(event: any) => {
			touchStartXRef.current = event.touches[0].clientX;
			touchDeltaXRef.current = 0;
			if (pause === 'hover') {
				setPaused(true);
			}
			onTouchStart == null ? void 0 : onTouchStart(event);
		},
		[pause, onTouchStart]
	);
	const handleTouchMove = React.useCallback(
		(event: any) => {
			if (event.touches && event.touches.length > 1) {
				touchDeltaXRef.current = 0;
			} else {
				touchDeltaXRef.current = event.touches[0].clientX - touchStartXRef.current;
			}
			onTouchMove == null ? void 0 : onTouchMove(event);
		},
		[onTouchMove]
	);
	const handleTouchEnd = React.useCallback(
		(event: any) => {
			if (touch) {
				const touchDeltaX = touchDeltaXRef.current;
				if (Math.abs(touchDeltaX) > SWIPE_THRESHOLD) {
					if (touchDeltaX > 0) {
						prev(event);
					} else {
						next(event);
					}
				}
			}
			if (pause === 'hover') {
				touchUnpauseTimeout.set(() => {
					setPaused(false);
				}, interval || undefined);
			}
			onTouchEnd == null ? void 0 : onTouchEnd(event);
		},
		[touch, pause, prev, next, touchUnpauseTimeout, interval, onTouchEnd]
	);
	const shouldPlay = interval != null && !paused && !isSliding;
	const intervalHandleRef = React.useRef<number | null>(null);
	React.useEffect(() => {
		let _ref, _activeChildIntervalR;
		if (!shouldPlay) {
			return undefined;
		}
		const nextFunc = isRTL ? prev : next;
		intervalHandleRef.current = window.setInterval(
			document.visibilityState ? nextWhenVisible : nextFunc,
			(_ref =
				(_activeChildIntervalR = activeChildIntervalRef.current) != null
					? _activeChildIntervalR
					: interval) != null
				? _ref
				: undefined
		);
		return () => {
			if (intervalHandleRef.current !== null) {
				clearInterval(intervalHandleRef.current);
			}
		};
	}, [
		shouldPlay,
		prev,
		next,
		activeChildIntervalRef,
		interval,
		nextWhenVisible,
		isRTL
	]);

	const indicatorOnClicks = React.useMemo(
		() =>
			indicators &&
			Array.from(
				{
					length: numChildren
				},
				(_, index) => (event: any) => {
					onSelect == null ? void 0 : onSelect(index, event);
				}
			),
		[indicators, numChildren, onSelect]
	);

	return (
		<Component
			ref={elementRef}
			{...props}
			onKeyDown={handleKeyDown}
			onMouseOver={handleMouseOver}
			onMouseOut={handleMouseOut}
			onTouchStart={handleTouchStart}
			onTouchMove={handleTouchMove}
			onTouchEnd={handleTouchEnd}
			className={classnames(
				className,
				prefix,
				slide && 'slide',
				fade && `${prefix}-fade`,
				variant && `${prefix}-${variant}`
			)}
		>
			{indicators && (
				<div className={`${prefix}-indicators`}>
					{ElementChildren.map(children, (_, index) => (
						<button
							type="button"
							data-bs-target="" // Bootstrap requires this in their css.
							aria-label={
								indicatorLabels != null && indicatorLabels.length
									? indicatorLabels[index]
									: `Slide ${index + 1}`
							}
							className={index === renderedActiveIndex ? 'active' : undefined}
							onClick={indicatorOnClicks ? indicatorOnClicks[index] : undefined}
							aria-current={index === renderedActiveIndex}
						>
							{index}
						</button>
					))}
				</div>
			)}

			<div className={`${prefix}-inner`}>
				{ElementChildren.map(children, (child, index) => {
					const isActive = index === renderedActiveIndex;
					return slide ? (
						<TransitionWrapper
							in={isActive}
							onEnter={isActive ? handleEnter : undefined}
							onEntered={isActive ? handleEntered : undefined}
							addEndListener={transitionEndListener}
						>
							{(status: any, innerProps: any) =>
								React.cloneElement(child, {
									...innerProps,
									className: classnames(
										child.props.className,
										isActive && status !== 'entered' && orderClassName,
										(status === 'entered' || status === 'exiting') && 'active',
										(status === 'entering' || status === 'exiting') &&
											directionalClassName
									)
								})
							}
						</TransitionWrapper>
					) : (
						React.cloneElement(child, {
							className: classnames(child.props.className, isActive && 'active')
						})
					);
				})}
			</div>

			{controls && (
				<>
					{(wrap || activeIndex !== 0) && (
						<Anchor
							className={`${prefix}-control-prev`}
							onClick={prev}
						>
							{prevIcon}
							{prevLabel && <span className="visually-hidden">{prevLabel}</span>}
						</Anchor>
					)}

					{(wrap || activeIndex !== numChildren - 1) && (
						<Anchor
							className={`${prefix}-control-next`}
							onClick={next}
						>
							{nextIcon}
							{nextLabel && <span className="visually-hidden">{nextLabel}</span>}
						</Anchor>
					)}
				</>
			)}
		</Component>
	);
});
Carousel.defaultProps = defaultProps;
export default Carousel;
