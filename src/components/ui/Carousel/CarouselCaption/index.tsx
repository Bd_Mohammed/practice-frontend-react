import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IPrefixRefForwardingComponent } from '../../../../utils/helper';

const CarouselCaption: IPrefixRefForwardingComponent<'div', unknown> =
	createWithComponentPrefix('carousel-caption');

export default CarouselCaption;
