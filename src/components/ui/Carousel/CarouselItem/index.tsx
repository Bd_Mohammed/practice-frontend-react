import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

interface CarouselItemProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	interval?: number;
}

const CarouselItem: IPrefixRefForwardingComponent<'div', CarouselItemProps> =
	React.forwardRef(
		(
			{
				// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
				as: Component = 'div',
				componentPrefix,
				className,
				...props
			},
			ref
		) => {
			const finalClassName = classnames(
				className,
				useThemePrefix(componentPrefix, 'carousel-item')
			);
			return (
				<Component
					ref={ref}
					{...props}
					className={finalClassName}
				/>
			);
		}
	);

export default CarouselItem;
