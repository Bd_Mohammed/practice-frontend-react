import React from 'react';
import { CollapseProps } from 'react-bootstrap';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import { Collapse } from '../../Collapse';
import { AccordionContext, isAccordionItemSelected } from '../AccordionContext';

export interface AccordionCollapseProps
	extends IPrefixWithAsProps,
		CollapseProps {
	eventKey: string;
}

/**
 * This component accepts all of [`Collapse`'s props](/utilities/transitions/#collapse-props).
 */
const AccordionCollapse: IComponentPrefixRefForwardingComponent<
	'div',
	AccordionCollapseProps
> = React.forwardRef<any, AccordionCollapseProps>(
	(
		{
			as: Component = 'div',
			componentPrefix,
			className,
			children,
			eventKey,
			...props
		},
		ref
	) => {
		const { activeEventKey } = React.useContext(AccordionContext);

		componentPrefix = useThemePrefix(componentPrefix, 'accordion-collapse');

		return (
			<Collapse
				ref={ref}
				in={isAccordionItemSelected(activeEventKey, eventKey)}
				{...props}
				className={classnames(className, componentPrefix)}
			>
				<Component>{React.Children.only(children)}</Component>
			</Collapse>
		);
	}
);

export default AccordionCollapse;
