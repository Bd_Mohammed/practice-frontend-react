import * as React from 'react';

export interface AccordionItemContextValue {
	eventKey: string;
}

const AccordionItemContext: React.Context<AccordionItemContextValue> =
	React.createContext<AccordionItemContextValue>({
		eventKey: ''
	});
export default AccordionItemContext;
