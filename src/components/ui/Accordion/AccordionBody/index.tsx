import { TransitionCallbacks } from '@restart/ui/types';
import React from 'react';

import { forwardRef, useContext } from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IPrefixWithAsProps,
	IComponentPrefixRefForwardingComponent
} from '../../../../utils/helper';

import { useThemePrefix } from '../../../../utils/ThemePrefix';
import AccordionCollapse from '../AccordionCollapse';
import AccordionItemContext from '../AccordionItemContext';

export interface AccordionBodyProps
	extends IPrefixWithAsProps,
		TransitionCallbacks,
		React.HTMLAttributes<HTMLDivElement> {}
const AccordionBody: IComponentPrefixRefForwardingComponent<
	'div',
	AccordionBodyProps
> = forwardRef<HTMLDivElement, AccordionBodyProps>(
	(
		{
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'div',
			componentPrefix,
			className,
			onEnter,
			onEntering,
			onEntered,
			onExit,
			onExiting,
			onExited,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'accordion-body');
		const { eventKey } = useContext(AccordionItemContext);
		return (
			<AccordionCollapse
				eventKey={eventKey}
				onEnter={onEnter}
				onEntering={onEntering}
				onEntered={onEntered}
				onExit={onExit}
				onExiting={onExiting}
				onExited={onExited}
			>
				<Component
					ref={ref}
					{...props}
					className={classnames(className, componentPrefix)}
				></Component>
			</AccordionCollapse>
		);
	}
);

export default AccordionBody;
