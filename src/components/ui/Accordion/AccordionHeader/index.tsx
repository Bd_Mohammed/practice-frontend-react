import React, { forwardRef } from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import AccordionButton from '../AccordionButton';

export interface AccordionHeaderProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {}

const AccordionHeader: IComponentPrefixRefForwardingComponent<
	'h2',
	AccordionHeaderProps
> = forwardRef<any, AccordionHeaderProps>(
	(
		{
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'h2',
			componentPrefix,
			className,
			children,
			onClick,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'accordion-header');
		return (
			<Component
				ref={ref}
				{...props}
				className={classnames(className, componentPrefix)}
			>
				<AccordionButton onClick={onClick}>{children}</AccordionButton>
			</Component>
		);
	}
);

export default AccordionHeader;
