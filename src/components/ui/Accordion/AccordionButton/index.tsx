import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IPrefixWithAsProps,
	IComponentPrefixRefForwardingComponent
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import { AccordionContext, isAccordionItemSelected } from '../AccordionContext';
import AccordionItemContext from '../AccordionItemContext';

type EventHandler = React.EventHandler<React.SyntheticEvent>;

export interface AccordionButtonProps
	extends React.ButtonHTMLAttributes<HTMLButtonElement>,
		IPrefixWithAsProps {}

function useAccordionButton(
	eventKey: string,
	onClick?: EventHandler
): EventHandler {
	const { activeEventKey, onSelect, alwaysOpen } =
		React.useContext(AccordionContext);

	return (e) => {
		/*
          Compare the event key in context with the given event key.
          If they are the same, then collapse the component.
        */
		let eventKeyPassed = eventKey === activeEventKey ? null : [eventKey];
		if (alwaysOpen) {
			if (Array.isArray(activeEventKey)) {
				if (activeEventKey.includes(eventKey)) {
					eventKeyPassed = activeEventKey.filter((k) => k !== eventKey);
				} else {
					eventKeyPassed = [...activeEventKey, eventKey];
				}
			} else {
				eventKeyPassed = [eventKey];
			}
		}
		onSelect == null ? void 0 : onSelect(eventKeyPassed, e);
		onClick == null ? void 0 : onClick(e);
	};
}

const AccordionButton: IComponentPrefixRefForwardingComponent<
	'div',
	AccordionButtonProps
> = React.forwardRef(
	(
		{
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'button',
			componentPrefix,
			className,
			onClick,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'accordion-button');
		const { eventKey } = React.useContext(AccordionItemContext);
		const accordionOnClick = useAccordionButton(eventKey, onClick);
		const { activeEventKey } = React.useContext(AccordionContext);
		if (Component === 'button') {
			props.type = 'button';
		}
		return (
			<Component
				ref={ref}
				onClick={accordionOnClick}
				{...props}
				aria-expanded={
					Array.isArray(activeEventKey)
						? activeEventKey.includes(eventKey)
						: eventKey === activeEventKey
				}
				className={classnames(
					className,
					componentPrefix,
					!isAccordionItemSelected(activeEventKey, eventKey) && 'collapsed'
				)}
			/>
		);
	}
);

export default AccordionButton;
