import React, { forwardRef, useMemo } from 'react';

import { useUncontrolled } from 'uncontrollable';

import { classnames } from '../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

import AccordionBody from './AccordionBody';
import AccordionButton from './AccordionButton';
import AccordionCollapse from './AccordionCollapse';
import {
	AccordionContext,
	AccordionEventKey,
	AccordionSelectCallback
} from './AccordionContext';
import AccordionHeader from './AccordionHeader';
import AccordionItem from './AccordionItem';

interface AccordionProps
	extends Omit<React.HTMLAttributes<HTMLElement>, 'onSelect'>,
		IPrefixWithAsProps {
	activeKey?: AccordionEventKey;
	defaultActiveKey?: AccordionEventKey;
	onSelect?: AccordionSelectCallback;
	flush?: boolean;
	alwaysOpen?: boolean;
}

export const Accordion: IComponentPrefixRefForwardingComponent<
	'div',
	AccordionProps
> = forwardRef((props, ref) => {
	const {
		as: Component = 'div',
		activeKey,
		componentPrefix,
		className,
		onSelect,
		flush,
		alwaysOpen,
		...controlledProps
	} = useUncontrolled(props, {
		activeKey: 'onSelect'
	});
	const prefix = useThemePrefix(componentPrefix, 'accordion');
	const contextValue = useMemo(
		() => ({
			activeEventKey: activeKey,
			onSelect,
			alwaysOpen
		}),
		[activeKey, onSelect, alwaysOpen]
	);

	return (
		<AccordionContext.Provider value={contextValue}>
			<Component
				ref={ref}
				{...controlledProps}
				className={classnames(className, prefix, flush && `${prefix}-flush`)}
			/>
		</AccordionContext.Provider>
	);
});

const _default: IComponentPrefixRefForwardingComponent<
	'div',
	AccordionProps
> & {
	Button: IComponentPrefixRefForwardingComponent<
		'div',
		import('./AccordionButton').AccordionButtonProps
	>;
	Collapse: IComponentPrefixRefForwardingComponent<
		'div',
		import('./AccordionCollapse').AccordionCollapseProps
	>;
	Item: IComponentPrefixRefForwardingComponent<
		'div',
		import('./AccordionItem').AccordionItemProps
	>;
	Header: IComponentPrefixRefForwardingComponent<
		'h2',
		import('./AccordionHeader').AccordionHeaderProps
	>;
	Body: IComponentPrefixRefForwardingComponent<
		'div',
		import('./AccordionBody').AccordionBodyProps
	>;
} = Object.assign(Accordion, {
	Button: AccordionButton,
	Collapse: AccordionCollapse,
	Item: AccordionItem,
	Header: AccordionHeader,
	Body: AccordionBody
});
export default _default;
