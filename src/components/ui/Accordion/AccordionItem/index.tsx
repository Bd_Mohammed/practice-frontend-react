import React, { forwardRef, useMemo } from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import AccordionItemContext from '../AccordionItemContext';

export interface AccordionItemProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	eventKey: string;
}

const AccordionItem: IComponentPrefixRefForwardingComponent<
	'div',
	AccordionItemProps
> = forwardRef<any, AccordionItemProps>(
	(
		{
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'div',
			componentPrefix,
			className,
			eventKey,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'accordion-item');
		const contextValue = useMemo(
			() => ({
				eventKey
			}),
			[eventKey]
		);
		return (
			<AccordionItemContext.Provider value={contextValue}>
				<Component
					ref={ref}
					{...props}
					className={classnames(className, componentPrefix)}
				></Component>
			</AccordionItemContext.Provider>
		);
	}
);

export default AccordionItem;
