import * as React from 'react';

import {
	IPrefixWithAsProps,
	IComponentPrefixRefForwardingComponent
} from '../../../utils/helper';

export interface BreadcrumbProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	label?: string;
	listProps?: React.OlHTMLAttributes<HTMLOListElement>;
}

declare const _default: IComponentPrefixRefForwardingComponent<
	'nav',
	BreadcrumbProps
> & {
	Item: IComponentPrefixRefForwardingComponent<
		'li',
		import('./BreadcrumbItem').BreadcrumbItemProps
	>;
};

export default _default;
