import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import Anchor from '../../Anchor';

export interface BreadcrumbItemProps
	extends IPrefixWithAsProps,
		Omit<React.HTMLAttributes<HTMLElement>, 'title'> {
	active?: boolean;
	href?: string;
	linkAs?: React.ElementType;
	target?: string;
	title?: React.ReactNode;
	linkProps?: Record<string, any>;
}

const defaultProps = {
	active: false,
	linkProps: {}
};

const BreadcrumbItem: IComponentPrefixRefForwardingComponent<
	'li',
	BreadcrumbItemProps
> = React.forwardRef(
	(
		{
			componentPrefix,
			active,
			children,
			className,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'li',
			linkAs: LinkComponent = Anchor,
			linkProps,
			href,
			title,
			target,
			...props
		},
		ref
	) => {
		const prefix = useThemePrefix(componentPrefix, 'breadcrumb-item');
		return (
			<Component
				ref={ref}
				{...props}
				className={classnames(prefix, className, {
					active
				})}
				aria-current={active ? 'page' : undefined}
			>
				<LinkComponent
					{...linkProps}
					href={href}
					title={title}
					target={target}
				>
					{children}
				</LinkComponent>
			</Component>
		);
	}
);
BreadcrumbItem.defaultProps = defaultProps;

export default BreadcrumbItem;
