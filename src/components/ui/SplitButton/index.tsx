import { ButtonType } from '@restart/ui/Button';
import * as React from 'react';

import { IPrefixWithAsProps } from '../../../utils/helper';
import Button from '../Button';
import { DropdownProps } from '../Dropdown';
import Dropdown from '../Dropdown';
import { PropsFromToggle } from '../Dropdown/DropdownToggle';

interface SplitButtonProps
	extends Omit<DropdownProps, 'title'>,
		PropsFromToggle,
		IPrefixWithAsProps {
	menuRole?: string;
	renderMenuOnMount?: boolean;
	rootCloseEvent?: 'click' | 'mousedown';
	target?: string;
	title: React.ReactNode;
	toggleLabel?: string;
	type?: ButtonType;
	flip?: boolean;
}

const defaultProps: any = {
	toggleLabel: 'Toggle dropdown',
	type: 'button'
};

/**
 * A convenience component for simple or general use split button dropdowns. Renders a
 * `ButtonGroup` containing a `Button` and a `Button` toggle for the `Dropdown`. All `children`
 * are passed directly to the default `Dropdown.Menu`. This component accepts all of [`Dropdown`'s
 * props](#dropdown-props).
 *
 * _All unknown props are passed through to the `Dropdown` component._
 * The Button `variant`, `size` and `componentPrefix` props are passed to the button and toggle,
 * and menu-related props are passed to the `Dropdown.Menu`
 */
const SplitButton: React.ForwardRefExoticComponent<
	SplitButtonProps & React.RefAttributes<HTMLElement>
> = React.forwardRef(
	(
		{
			id,
			componentPrefix,
			size,
			variant,
			title,
			type,
			toggleLabel,
			children,
			onClick,
			href,
			target,
			menuRole,
			renderMenuOnMount,
			rootCloseEvent,
			flip,
			...props
		},
		ref
	) => (
		<Dropdown
			ref={ref}
			as={(Button as any).Group}
			{...props}
		>
			<>
				<Button
					size={size}
					variant={variant}
					disabled={props.disabled}
					componentPrefix={componentPrefix}
					href={href}
					target={target}
					onClick={onClick}
					type={type}
				>
					{title}
				</Button>
				<Dropdown.Toggle
					split
					id={id}
					size={size}
					variant={variant}
					disabled={props.disabled}
					childBsPrefix={componentPrefix}
				>
					<span className="visually-hidden">{toggleLabel}</span>
				</Dropdown.Toggle>
				<Dropdown.Menu
					role={menuRole}
					renderOnMount={renderMenuOnMount}
					rootCloseEvent={rootCloseEvent}
					flip={flip}
				>
					{children}
				</Dropdown.Menu>
			</>
		</Dropdown>
	)
);

SplitButton.defaultProps = defaultProps;
export default SplitButton;
