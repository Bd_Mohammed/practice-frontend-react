import useEventCallback from '@restart/hooks/useEventCallback';
import React from 'react';
import { useUncontrolled } from 'uncontrollable';

// import { TransitionComponent } from '@restart/ui/types';
import { classnames } from '../../../utils/classnames';
import { createWithComponentPrefix } from '../../../utils/createWithComponenetPrefix';
import { divWithClassName } from '../../../utils/divWithClassName';
import {
	CloseButtonVariant,
	IPrefixRefForwardingComponent,
	TransitionType,
	Variant
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

import Anchor, { AnchorProp } from '../Anchor';
import { CloseButton } from '../CloseButton';
import { Fade } from '../Fade';

export interface AlertProps extends React.HTMLAttributes<HTMLDivElement> {
	bsPrefix?: string;
	variant?: Variant;
	dismissible?: boolean;
	show?: boolean;
	onClose?: (a: any, b: any) => void;
	closeLabel?: string;
	closeVariant?: CloseButtonVariant;
	transition?: TransitionType;
}

const defaultProps = {
	variant: 'primary',
	show: true,
	transition: Fade,
	closeLabel: 'Close alert'
};

const DivStyledAsH4 = divWithClassName('h4');
DivStyledAsH4.displayName = 'DivStyledAsH4';
const AlertHeading: IPrefixRefForwardingComponent<
	React.ForwardRefExoticComponent<
		AnchorProp & React.RefAttributes<HTMLAnchorElement>
	>,
	unknown
> = createWithComponentPrefix('alert-heading', {
	Component: DivStyledAsH4
});
const AlertLink: IPrefixRefForwardingComponent<
	React.ForwardRefExoticComponent<
		Pick<
			React.DetailedHTMLProps<
				React.HTMLAttributes<HTMLDivElement>,
				HTMLDivElement
			>,
			'key' | keyof React.HTMLAttributes<HTMLDivElement>
		> &
			React.RefAttributes<HTMLDivElement>
	>,
	unknown
> = createWithComponentPrefix('alert-link', {
	Component: Anchor
});

type props = React.ForwardRefExoticComponent<
	AlertProps & React.RefAttributes<HTMLDivElement>
>;
// {
//     Link: IPrefixRefForwardingComponent<React.ForwardRefExoticComponent<import("@restart/ui/Anchor").AnchorProps & React.RefAttributes<HTMLAnchorElement>>, unknown>;
//     Heading: IPrefixRefForwardingComponent<React.ForwardRefExoticComponent<Pick<React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>, "key" | keyof React.HTMLAttributes<HTMLDivElement>> & React.RefAttributes<HTMLDivElement>>, unknown>;
// }

const Alert: props = React.forwardRef((uncontrolledProps, ref) => {
	const {
		bsPrefix,
		show,
		closeLabel,
		closeVariant,
		className,
		children,
		variant,
		onClose,
		dismissible,
		transition,
		...props
	} = useUncontrolled(uncontrolledProps, {
		show: 'onClose'
	});
	const prefix = useThemePrefix(bsPrefix, 'alert');
	const handleClose = useEventCallback((e) => {
		if (onClose) {
			onClose(false, e);
		}
	});
	const Transition = transition === true ? Fade : transition;
	const alert = (
		<div
			role={'alert'}
			{...(!Transition ? props : undefined)}
			ref={ref}
			className={classnames(
				className,
				prefix,
				variant && `${prefix}-${variant}`,
				dismissible && `${prefix}-dismissible`
			)}
		>
			{dismissible && (
				<>
					{' '}
					<CloseButton
						onClick={handleClose}
						aria-label={closeLabel}
						variant={closeVariant}
					/>{' '}
					{children}
				</>
			)}
		</div>
	);
	if (!Transition) return show ? alert : null;
	return (
		<Transition
			unmountOnExit={true}
			{...props}
			ref={undefined}
			in={show}
			children={alert}
		></Transition>
	);
});

Alert.defaultProps = defaultProps;
const _default = Object.assign(Alert, {
	Link: AlertLink,
	Heading: AlertHeading
});
export default _default;
