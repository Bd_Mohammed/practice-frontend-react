import useBreakpoint from '@restart/hooks/useBreakpoint';
import useEventCallback from '@restart/hooks/useEventCallback';
import { ModalProps as BaseModalProps } from '@restart/ui/Modal';
import React from 'react';

import { classnames } from '../../../utils/classnames';
import { IPrefixProps } from '../../../utils/helper';
import { ModalManager } from '../../../utils/ModalManager';
import { useThemePrefix } from '../../../utils/ThemePrefix';
import { Fade, FadeProps } from '../Fade';
import { Modal } from '../Modal';
import ModalContext from '../Modal/ModalContext';
import { getSharedManager } from '../ModalManager';
import NavbarContext from '../Navbar/NavbarContext';

import { OffcanvasToggling, OffcanvasTogglingProps } from './offcanvasToggling';

export type OffcanvasPlacement = 'start' | 'end' | 'top' | 'bottom';

export interface OffcanvasProps
	extends Omit<
			BaseModalProps,
			| 'role'
			| 'renderBackdrop'
			| 'renderDialog'
			| 'transition'
			| 'backdrop'
			| 'backdropTransition'
		>,
		IPrefixProps {
	backdropClassName?: string;
	scroll?: boolean;
	placement?: OffcanvasPlacement;
	responsive?: 'sm' | 'md' | 'lg' | 'xl' | 'xxl' | string;
	renderStaticNode?: boolean;
}

const defaultProps = {
	show: false,
	backdrop: true,
	keyboard: true,
	scroll: false,
	autoFocus: true,
	enforceFocus: true,
	restoreFocus: true,
	placement: 'start',
	renderStaticNode: false
};

function DialogTransition(props: OffcanvasTogglingProps) {
	return <OffcanvasToggling {...props} />;
}
function BackdropTransition(props: FadeProps) {
	return <Fade {...props} />;
}

const Offcanvas = React.forwardRef(
	(
		{
			componentPrefix,
			className,
			children,
			'aria-labelledby': ariaLabelledby,
			placement,
			responsive,
			/* BaseModal props */
			show,
			backdrop,
			keyboard,
			scroll,
			onEscapeKeyDown,
			onShow,
			onHide,
			container,
			autoFocus,
			enforceFocus,
			restoreFocus,
			restoreFocusOptions,
			onEntered,
			onExit,
			onExiting,
			onEnter,
			onEntering,
			onExited,
			backdropClassName,
			manager: propsManager,
			renderStaticNode,
			...props
		}: OffcanvasProps,
		ref
	) => {
		const modalManager = React.useRef(null);
		componentPrefix = useThemePrefix(componentPrefix, 'offcanvas');
		const { onToggle } = React.useContext(NavbarContext);

		const [showOffcanvas, setShowOffcanvas] = React.useState(false);
		//@ts-expect-error
		const hideResponsiveOffcanvas = useBreakpoint(responsive || 'xs', 'up');
		React.useEffect(() => {
			// Handles the case where screen is resized while the responsive
			// offcanvas is shown. If `responsive` not provided, just use `show`.
			setShowOffcanvas(responsive ? show && !hideResponsiveOffcanvas : show);
		}, [show, responsive, hideResponsiveOffcanvas]);

		const handleHide = useEventCallback(() => {
			onToggle == null ? void 0 : onToggle();
			onHide == null ? void 0 : onHide();
		});
		const modalContext = React.useMemo(
			() => ({
				onHide: handleHide
			}),
			[handleHide]
		);

		function getModalManager() {
			if (propsManager) return propsManager;
			if (scroll) {
				// Have to use a different modal manager since the shared
				// one handles overflow.
				//@ts-ignore
				if (!modalManager.current)
					modalManager.current = new ModalManager({
						handleContainerOverflow: false
					}) as any;
				return modalManager.current;
			}
			return getSharedManager();
		}
		// const handleEnter = (node: HTMLElement, ...args: any[]) => {
		//     if (node) node.style.visibility = 'visible';
		//     onEnter == null ? void 0 : onEnter(node, ...args);
		// };

		const handleExited = (node: HTMLElement, ...args: any[]) => {
			if (node) node.style.visibility = '';
			onExited == null ? void 0 : onExited(...args);
		};

		const renderBackdrop = React.useCallback(
			(backdropProps: any) => (
				<div
					{...backdropProps}
					className={classnames(`${componentPrefix}-backdrop`, backdropClassName)}
				/>
			),
			[backdropClassName, componentPrefix]
		);

		const renderDialog = (dialogProps: any) => (
			<div
				{...dialogProps}
				{...props}
				className={classnames(
					className,
					responsive ? `${componentPrefix}-${responsive}` : componentPrefix,
					`${componentPrefix}-${placement}`
				)}
				aria-labelledby={ariaLabelledby}
				children={children}
			/>
		);

		return (
			<>
				{!showOffcanvas && (responsive || renderStaticNode) && renderDialog({})}
				<ModalContext.Provider value={modalContext}>
					<Modal
						show={showOffcanvas}
						ref={ref}
						backdrop={backdrop}
						container={container}
						keyboard={keyboard}
						autoFocus={autoFocus}
						enforceFocus={enforceFocus && !scroll}
						restoreFocus={restoreFocus}
						restoreFocusOptions={restoreFocusOptions}
						onEscapeKeyDown={onEscapeKeyDown}
						onShow={onShow}
						onHide={handleHide}
						onEnter={onEnter}
						onEntering={onEntering}
						onEntered={onEntered}
						onExit={onExit}
						onExiting={onExiting}
						onExited={handleExited}
						manager={getModalManager()}
						transition={DialogTransition}
						backdropTransition={BackdropTransition}
						renderBackdrop={renderBackdrop}
						renderDialog={renderDialog}
					/>
				</ModalContext.Provider>
			</>
		);
	}
);
Offcanvas.defaultProps = defaultProps;
export default Offcanvas;
