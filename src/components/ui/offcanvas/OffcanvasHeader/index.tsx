import React from 'react';
import { AbstractModalHeaderProps } from 'react-bootstrap/esm/AbstractModalHeader';

import { classnames } from '../../../../utils/classnames';
import { IPrefixProps } from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import { AbstractModalHeader } from '../../AbstractModalHeader';

export interface OffcanvasHeaderProps
	extends AbstractModalHeaderProps,
		IPrefixProps {}
const defaultProps = {
	closeLabel: 'Close',
	closeButton: false
};
const OffcanvasHeader: React.ForwardRefExoticComponent<
	OffcanvasHeaderProps & React.RefAttributes<HTMLDivElement>
> = React.forwardRef(
	({ componentPrefix, className, ...props }: OffcanvasHeaderProps, ref) => {
		componentPrefix = useThemePrefix(componentPrefix, 'offcanvas-header');

		return (
			<AbstractModalHeader
				ref={ref}
				{...props}
				className={classnames(className, componentPrefix)}
			/>
		);
	}
);

OffcanvasHeader.defaultProps = defaultProps;
export { OffcanvasHeader };
