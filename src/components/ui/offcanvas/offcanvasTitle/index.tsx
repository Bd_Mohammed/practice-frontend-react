import React from 'react';

import { classnames } from '../../../../utils/classnames';
import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

const divWithClassName = (
	className: string
): React.ForwardRefExoticComponent<
	Pick<
		React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
		'key' | keyof React.HTMLAttributes<HTMLDivElement>
	> &
		React.RefAttributes<HTMLDivElement>
> =>
	React.forwardRef((p, ref) => (
		<div
			{...p}
			ref={ref}
			className={classnames(p.className, className)}
		/>
	));
const DivStyledAsH5 = divWithClassName('h5');

export const offcannvasTitle: IComponentPrefixRefForwardingComponent<
	React.ForwardRefExoticComponent<
		Pick<
			React.DetailedHTMLProps<
				React.HTMLAttributes<HTMLDivElement>,
				HTMLDivElement
			>,
			'key' | keyof React.HTMLAttributes<HTMLDivElement>
		> &
			React.RefAttributes<HTMLDivElement>
	>,
	unknown
> = createWithComponentPrefix('offcanvas-title', {
	Component: DivStyledAsH5
});
