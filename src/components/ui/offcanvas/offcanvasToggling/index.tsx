import React from 'react';
import Transition from 'react-transition-group/Transition';

import { classnames } from '../../../../utils/classnames';
import { IPrefixProps, TransitionCallbacks } from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import { transitionEndListener } from '../../../../utils/transitionEndListener';
import { TransitionWrapper } from '../../TransitionWrapper';

export interface OffcanvasTogglingProps
	extends TransitionCallbacks,
		IPrefixProps {
	className?: string;
	in?: boolean;
	mountOnEnter?: boolean;
	unmountOnExit?: boolean;
	appear?: boolean;
	timeout?: number;
	children: React.ReactElement;
}

const defaultProps = {
	in: false,
	mountOnEnter: false,
	unmountOnExit: false,
	appear: false
};
const transitionStyles = {
	entering: 'show',
	entered: 'show'
};
const OffcanvasToggling: React.ForwardRefExoticComponent<
	OffcanvasTogglingProps & React.RefAttributes<Transition<any>>
> = React.forwardRef(
	({ componentPrefix, className, children, ...props }, ref) => {
		componentPrefix = useThemePrefix(componentPrefix, 'offcanvas');
		return (
			<TransitionWrapper
				ref={ref}
				addEndListener={transitionEndListener}
				{...props}
				childRef={(children as any).ref}
				children={(status: keyof typeof transitionStyles, innerProps: any) =>
					React.cloneElement(children, {
						...innerProps,
						className: classnames(
							className,
							children.props.className,
							(status === 'entering' || status === 'entered') &&
								`${componentPrefix}-toggling`,
							transitionStyles[status]
						)
					})
				}
			/>
		);
	}
);

OffcanvasToggling.defaultProps = defaultProps;
export { OffcanvasToggling };
