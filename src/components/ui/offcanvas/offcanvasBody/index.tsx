import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

export const OffcanvasBody: IComponentPrefixRefForwardingComponent<'div', any> =
	createWithComponentPrefix('offcanvas-body');
