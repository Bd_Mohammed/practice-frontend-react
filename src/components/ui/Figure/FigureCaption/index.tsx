import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IPrefixRefForwardingComponent } from '../../../../utils/helper';

const FigureCaption: IPrefixRefForwardingComponent<'figcaption', unknown> =
	createWithComponentPrefix('figure-caption', {
		Component: 'figcaption'
	});

export default FigureCaption;
