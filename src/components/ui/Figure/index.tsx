import { createWithComponentPrefix } from '../../../utils/createWithComponenetPrefix';

import FigureCaption from './FigureCaption';
import FigureImage from './FigureImage';

const Figure = createWithComponentPrefix('figure', {
	Component: 'figure'
});
const _default = Object.assign(Figure, {
	Image: FigureImage,
	Caption: FigureCaption
});
export default _default;
