import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import Image, { ImageProps } from '../../Image';

const FigureImage: React.ForwardRefExoticComponent<
	ImageProps & React.RefAttributes<HTMLImageElement>
> = React.forwardRef(({ className, ...props }, ref) => (
	<Image
		ref={ref}
		{...props}
		className={classnames(className, 'figure-img')}
	/>
));
export default FigureImage;
