import React, { forwardRef } from 'react';

import { classnames } from '../../../utils/classnames';

interface ContainerProps extends React.HTMLAttributes<HTMLElement> {
	fluid: boolean | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';
	className?: string;
	bsPrefix?: string;
	as?: React.ElementType;
	children?: JSX.Element;
}
const defaultProps = {
	fluid: false
};

const Container = forwardRef(
	(
		{
			bsPrefix,
			fluid,
			as: Component = 'div',
			className,
			...props
		}: ContainerProps,
		ref
	) => {
		const prefix = bsPrefix || 'container';
		const suffix = typeof fluid === 'string' ? `-${fluid}` : '-fluid';

		return (
			<Component
				ref={ref}
				{...props}
				className={classnames(className, fluid ? `${prefix}${suffix}` : prefix)}
			/>
		);
	}
);

Container.displayName = 'Container';
Container.defaultProps = defaultProps;

export default Container;
