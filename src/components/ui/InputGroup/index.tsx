import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import { createWithComponentPrefix } from '../../../utils/createWithComponenetPrefix';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';
import FormCheckInput from '../Form/FormCheck/FormCheckInput';

import InputGroupContext from './InputGroupContext';

interface InputGroupProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	size?: 'sm' | 'lg';
	hasValidation?: boolean;
}

const InputGroupText = createWithComponentPrefix('input-group-text', {
	Component: 'span'
});

const InputGroupCheckbox = (props: any) => (
	<InputGroupText>
		<FormCheckInput
			type="checkbox"
			{...props}
		/>
	</InputGroupText>
);

const InputGroupRadio = (props: any) => (
	<InputGroupText>
		<FormCheckInput
			type="radio"
			{...props}
		/>
	</InputGroupText>
);

const InputGroup: IComponentPrefixRefForwardingComponent<
	'div',
	InputGroupProps
> = React.forwardRef(
	(
		{
			componentPrefix,
			size,
			hasValidation,
			className,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'div',
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'input-group');

		// Intentionally an empty object. Used in detecting if a dropdown
		// exists under an input group.
		const contextValue = React.useMemo(() => ({}), []);
		return (
			<InputGroupContext.Provider value={contextValue}>
				<Component
					ref={ref}
					{...props}
					className={classnames(
						className,
						componentPrefix,
						size && `${componentPrefix}-${size}`,
						hasValidation && 'has-validation'
					)}
				/>
			</InputGroupContext.Provider>
		);
	}
);

const _default = Object.assign(InputGroup, {
	Text: InputGroupText,
	Radio: InputGroupRadio,
	Checkbox: InputGroupCheckbox
});

export default _default;
