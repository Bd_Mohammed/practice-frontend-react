import * as React from 'react';

export type InputGroup = {};

const InputGroupContext: React.Context<unknown> = React.createContext<unknown>(
	{}
);
export default InputGroupContext;
