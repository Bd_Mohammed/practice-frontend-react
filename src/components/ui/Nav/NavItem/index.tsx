import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

const NavItem: IComponentPrefixRefForwardingComponent<'div', unknown> =
	createWithComponentPrefix('nav-item');
export default NavItem;
