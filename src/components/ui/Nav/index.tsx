import { NavProps as BaseNavProps } from '@restart/ui/Nav';
import N_A_V from '@restart/ui/Nav';
import { EventKey } from '@restart/ui/types';
import * as React from 'react';

import { useUncontrolled } from 'uncontrollable';

import { classnames } from '../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

import CardHeaderContext from '../Card/CardHeader/CardHeaderContext';
import NavbarContext from '../Navbar/NavbarContext';

import NavItem from './NavItem';
import NavLink from './NavLink';

interface NavProps extends IPrefixWithAsProps, BaseNavProps {
	navbarBsPrefix?: string;
	cardHeaderBsPrefix?: string;
	variant?: 'tabs' | 'pills' | string;
	defaultActiveKey?: EventKey;
	fill?: boolean;
	justify?: boolean;
	navbar?: boolean;
	navbarScroll?: boolean;
}

const defaultProps = {
	justify: false,
	fill: false
};

const Nav: IComponentPrefixRefForwardingComponent<'div', NavProps> =
	React.forwardRef((uncontrolledProps, ref) => {
		const {
			as = 'div',
			componentPrefix: initialBsPrefix,
			variant,
			fill,
			justify,
			navbar,
			navbarScroll,
			className,
			activeKey,
			...props
		} = useUncontrolled(uncontrolledProps, {
			activeKey: 'onSelect'
		});
		const bsPrefix = useThemePrefix(initialBsPrefix, 'nav');
		let navbarComponentPrefix;
		let cardHeaderBsPrefix;
		let isNavbar = false;
		const navbarContext = React.useContext(NavbarContext);
		const cardHeaderContext = React.useContext(CardHeaderContext);
		if (navbarContext) {
			navbarComponentPrefix = navbarContext.componentPrefix;
			isNavbar = navbar == null ? true : navbar;
		} else if (cardHeaderContext) {
			({ cardHeaderBsPrefix } = cardHeaderContext);
		}
		return (
			<N_A_V
				as={as}
				ref={ref}
				activeKey={activeKey}
				className={classnames(className, {
					[bsPrefix]: !isNavbar,
					[`${navbarComponentPrefix}-nav`]: isNavbar,
					[`${navbarComponentPrefix}-nav-scroll`]: isNavbar && navbarScroll,
					[`${cardHeaderBsPrefix}-${variant}`]: !!cardHeaderBsPrefix,
					[`${bsPrefix}-${variant}`]: !!variant,
					[`${bsPrefix}-fill`]: fill,
					[`${bsPrefix}-justified`]: justify
				})}
				{...props}
			/>
		);
	});
Nav.defaultProps = defaultProps;

const _default = Object.assign(N_A_V, {
	Item: NavItem,
	Link: NavLink
});

export default _default;
