import NavItem from '@restart/ui/NavItem';
import { makeEventKey } from '@restart/ui/SelectableContext';
import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import Anchor from '../../Anchor';

export interface NavLinkProps
	extends IPrefixWithAsProps,
		Omit<
			//BaseNavItemProps
			any,
			'as'
		> {}

const defaultProps = {
	disabled: false
};

const NavLink: IComponentPrefixRefForwardingComponent<'a', NavLinkProps> =
	React.forwardRef(
		(
			{
				componentPrefix,
				className,
				as: Component = Anchor,
				active,
				eventKey,
				...props
			},
			ref
		) => {
			componentPrefix = useThemePrefix(componentPrefix, 'nav-link');
			const [navItemProps, meta] = NavItem({
				key: makeEventKey(eventKey, props.href),
				active,
				...props
			}) as any;
			return (
				<Component
					{...props}
					{...navItemProps}
					ref={ref}
					className={classnames(
						className,
						componentPrefix,
						props.disabled && 'disabled',
						meta.isActive && 'active'
					)}
				/>
			);
		}
	);
NavLink.defaultProps = defaultProps;
export default NavLink;
