import { EventKey } from '@restart/ui/types';
import * as React from 'react';
interface NavContextType {
	role?: string;
	activeKey: EventKey | null;
	getControlledId: (key: EventKey | null) => string;
	getControllerId: (key: EventKey | null) => string;
}
const NavContext: React.Context<NavContextType | null> =
	React.createContext<NavContextType | null>(null);
export default NavContext;
