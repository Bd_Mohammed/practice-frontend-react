import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

const TabContent: IComponentPrefixRefForwardingComponent<'div', unknown> =
	createWithComponentPrefix('tab-content');
export default TabContent;
