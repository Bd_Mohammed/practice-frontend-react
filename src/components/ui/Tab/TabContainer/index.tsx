import { TabsProps } from '@restart/ui/Tabs';
import Tabs from '@restart/ui/Tabs';
import React from 'react';
import { TransitionType } from 'react-bootstrap/esm/helpers';

import getTabTransitionComponent from '../../../../utils/getTabTransitionComponent';

interface TabContainerProps extends Omit<TabsProps, 'transition'> {
	transition?: TransitionType;
}

const TabContainer = ({ transition, ...props }: TabContainerProps) => (
	<Tabs
		{...props}
		transition={getTabTransitionComponent(transition)}
	/>
);

export default TabContainer;
