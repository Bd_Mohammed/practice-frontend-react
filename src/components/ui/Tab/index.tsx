import Tab from '@restart/ui/Tabs';
import React from 'react';
import { TabPaneProps } from 'react-bootstrap';

import ElementChildren from '../../../utils/elementChildren';
import getTabTransitionComponent from '../../../utils/getTabTransitionComponent';
import Nav from '../Nav';
import NavItem from '../Nav/NavItem';
import NavLink from '../Nav/NavLink';

import TabContent from './TabContent';
import TabPane from './TabPane';

interface TabProps extends Omit<TabPaneProps, 'title'> {
	title: React.ReactNode;
	disabled?: boolean;
	tabClassName?: string;
	tabAttrs?: Record<string, any>;
}

const defaultProps = {
	variant: 'tabs',
	mountOnEnter: false,
	unmountOnExit: false
};

function getDefaultActiveKey(children: any) {
	let defaultActiveKey: any;
	ElementChildren.forEach(children, (child) => {
		if (defaultActiveKey == null) {
			defaultActiveKey = child.props.eventKey;
		}
	});
	return defaultActiveKey;
}
function renderTab(child: any) {
	const { title, eventKey, disabled, tabClassName, tabAttrs, id } = child.props;
	if (title == null) {
		return null;
	}
	return (
		<NavItem
			as="li"
			role="presentation"
		>
			<NavLink
				as="button"
				type="button"
				eventKey={eventKey}
				disabled={disabled}
				id={id}
				className={tabClassName}
				{...tabAttrs}
			>
				{title}
			</NavLink>
		</NavItem>
	);
}

const Tabs: React.FC<TabProps> = ({
	id,
	onSelect,
	transition,
	mountOnEnter,
	unmountOnExit,
	children,
	activeKey = getDefaultActiveKey(children),
	...controlledProps
}: any) => {
	return (
		<Tab
			id={id}
			activeKey={activeKey}
			onSelect={onSelect}
			transition={getTabTransitionComponent(transition)}
			mountOnEnter={mountOnEnter}
			unmountOnExit={unmountOnExit}
			{...controlledProps}
		>
			<Nav
				role="tablist"
				as="ul"
			>
				{ElementChildren.map(children, renderTab)}
			</Nav>
			<TabContent>
				{ElementChildren.map(children, (child) => {
					const { title, disabled, tabClassName, tabAttrs, ...childProps } =
						child.props;
					return <TabPane {...childProps} />;
				})}
			</TabContent>
		</Tab>
	);
};

Tabs.defaultProps = defaultProps;

export default Tabs;
