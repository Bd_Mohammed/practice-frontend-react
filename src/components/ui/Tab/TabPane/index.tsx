import SelectableContext from '@restart/ui/SelectableContext';
import TabContext from '@restart/ui/TabContext';
import { useTabPanel } from '@restart/ui/TabPanel';
import { EventKey, TransitionCallbacks } from '@restart/ui/types';
import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import getTabTransitionComponent from '../../../../utils/getTabTransitionComponent';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps,
	TransitionType
} from '../../../../utils/helper';

import { useThemePrefix } from '../../../../utils/ThemePrefix';

import { Fade } from '../../Fade';

interface TabPaneProps
	extends TransitionCallbacks,
		IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	eventKey?: EventKey;
	active?: boolean;
	transition?: TransitionType;
	mountOnEnter?: boolean;
	unmountOnExit?: boolean;
}
const TabPane: IComponentPrefixRefForwardingComponent<'div', TabPaneProps> =
	React.forwardRef(({ componentPrefix, transition, ...props }, ref) => {
		const [
			{
				className,
				// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
				as: Component = 'div',
				...rest
			},
			{
				isActive,
				onEnter,
				onEntering,
				onEntered,
				onExit,
				onExiting,
				onExited,
				mountOnEnter,
				unmountOnExit,
				transition: Transition = Fade
			}
		] = useTabPanel({
			...props,
			transition: getTabTransitionComponent(transition)
		});
		const prefix = useThemePrefix(componentPrefix, 'tab-pane');

		// We provide an empty the TabContext so `<Nav>`s in `<TabPanel>`s don't
		// conflict with the top level one.
		return (
			<TabContext.Provider value={null}>
				<SelectableContext.Provider value={null}>
					<Transition
						in={isActive}
						onEnter={onEnter}
						onEntering={onEntering}
						onEntered={onEntered}
						onExit={onExit}
						onExiting={onExiting}
						onExited={onExited}
						mountOnEnter={mountOnEnter}
						unmountOnExit={unmountOnExit}
					>
						<Component
							{...rest}
							ref={ref}
							className={classnames(className, prefix, isActive && 'active')}
						/>
					</Transition>
				</SelectableContext.Provider>
			</TabContext.Provider>
		);
	});
export default TabPane;
