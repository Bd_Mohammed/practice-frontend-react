import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import { IPrefixWithAsProps } from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

const ROUND_PRECISION = 1000;

interface ProgressBarProps
	extends React.HTMLAttributes<HTMLDivElement>,
		IPrefixWithAsProps {
	min: number;
	now: number;
	max: number;
	label?: React.ReactNode;
	visuallyHidden?: boolean;
	striped?: boolean;
	animated?: boolean;
	variant?: 'success' | 'danger' | 'warning' | 'info' | string;
	isChild?: boolean;
}

// /**
//  * Validate that children, if any, are instances of `<ProgressBar>`.
//  */
// function onlyProgressBar(props, propName, componentName) {
//     const children = props[propName];
//     if (!children) {
//         return null;
//     }
//     let error = null;
//     React.Children.forEach(children, child => {
//         if (error) {
//             return;
//         }

//         /**
//          * Compare types in a way that works with libraries that patch and proxy
//          * components like react-hot-loader.
//          *
//          * see https://github.com/gaearon/react-hot-loader#checking-element-types
//          */
//         // eslint-disable-next-line @typescript-eslint/no-use-before-define
//         const element = /*#__PURE__*/(0, _jsxRuntime.jsx)(ProgressBar, {});
//         if (child.type === element.type) return;
//         const childType = child.type;
//         const childIdentifier = /*#__PURE__*/React.isValidElement(child) ? childType.displayName || childType.name || childType : child;
//         error = new Error(`Children of ${componentName} can contain only ProgressBar ` + `components. Found ${childIdentifier}.`);
//     });
//     return error;
// }
const defaultProps = {
	min: 0,
	max: 100,
	animated: false,
	isChild: false,
	visuallyHidden: false,
	striped: false
};
function getPercentage(now: number, min: number, max: number) {
	const percentage = ((now - min) / (max - min)) * 100;
	return Math.round(percentage * ROUND_PRECISION) / ROUND_PRECISION;
}

const renderProgressBar = (
	{
		min,
		now,
		max,
		label,
		visuallyHidden,
		striped,
		animated,
		className,
		style,
		variant,
		componentPrefix,
		...props
	}: ProgressBarProps,
	ref: React.LegacyRef<HTMLDivElement> | undefined
) => {
	return (
		<div
			ref={ref}
			role="progressbar"
			className={classnames(className, `${componentPrefix}-bar`, {
				[`bg-${variant}`]: variant,
				[`${componentPrefix}-bar-animated`]: animated,
				[`${componentPrefix}-bar-striped`]: animated || striped
			})}
			style={{
				width: `${getPercentage(now, min, max)}%`,
				...style
			}}
			aria-valuenow={now}
			aria-valuemin={min}
			aria-valuemax={max}
			{...props}
		>
			{visuallyHidden ? <span className="visually-hidden">{label}</span> : label}
		</div>
	);
};

const ProgressBar: React.ForwardRefExoticComponent<
	ProgressBarProps & React.RefAttributes<HTMLDivElement>
> = React.forwardRef<HTMLDivElement, ProgressBarProps>(
	({ isChild, ...props }, ref) => {
		props.componentPrefix = useThemePrefix(props.componentPrefix, 'progress');

		if (isChild) {
			return renderProgressBar(props, ref);
		}

		const {
			min,
			now,
			max,
			label,
			visuallyHidden,
			striped,
			animated,
			componentPrefix,
			variant,
			className,
			children,
			...wrapperProps
		} = props;

		return (
			<div
				ref={ref}
				{...wrapperProps}
				className={classnames(className, componentPrefix)}
			>
				{children
					? React.Children.map(children, (child) =>
							React.cloneElement(child as any, {
								isChild: true
							})
						)
					: renderProgressBar(
							{
								min,
								now,
								max,
								label,
								visuallyHidden,
								striped,
								animated,
								componentPrefix,
								variant
							},
							ref
						)}
			</div>
		);
	}
);

ProgressBar.defaultProps = defaultProps;
export default ProgressBar;
