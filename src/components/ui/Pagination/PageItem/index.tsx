import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixProps
} from '../../../../utils/helper';
import Anchor from '../../Anchor';
interface PageItemProps
	extends React.HTMLAttributes<HTMLElement>,
		IPrefixProps {
	disabled?: boolean;
	active?: boolean;
	activeLabel?: string;
	href?: string;
}
const defaultProps = {
	active: false,
	disabled: false,
	activeLabel: '(current)'
};
const PageItem: IComponentPrefixRefForwardingComponent<'li', PageItemProps> =
	React.forwardRef<any, PageItemProps>(
		(
			{ active, disabled, className, style, activeLabel, children, ...props },
			ref
		) => {
			const Component = active || disabled ? 'span' : Anchor;
			return (
				<li
					ref={ref}
					style={style}
					className={classnames(className, 'page-item', {
						active,
						disabled
					})}
				>
					<Component
						className="page-link"
						{...props}
					>
						{children}
						{active && activeLabel && (
							<span className="visually-hidden">{activeLabel}</span>
						)}
					</Component>
				</li>
			);
		}
	);
function createButton(name: string, _defaultValue: string, _label = name) {
	const Button = React.forwardRef(
		({ children, defaultValue, label, ...props }: any, ref) => (
			<PageItem
				{...props}
				ref={ref}
			>
				<span aria-hidden="true">{children || defaultValue}</span>
				<span className="visually-hidden">{label}</span>
			</PageItem>
		)
	);
	Button.displayName = name;
	return Button;
}

const First = createButton('First', '«');
const Prev = createButton('Prev', '‹', 'Previous');
const Ellipsis = createButton('Ellipsis', '…', 'More');
const Next = createButton('Next', '›');
const Last = createButton('Last', '»');

PageItem.defaultProps = defaultProps;

const _default = Object.assign(PageItem, {
	First,
	Prev,
	Ellipsis,
	Next,
	Last
});
export default _default;
