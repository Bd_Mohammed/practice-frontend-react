import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import { IPrefixWithAsProps } from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

import PageItem from './PageItem';

interface PaginationProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLUListElement> {
	size?: 'sm' | 'lg';
}

const Pagination: React.ForwardRefExoticComponent<
	PaginationProps & React.RefAttributes<HTMLUListElement>
> = React.forwardRef(({ componentPrefix, className, size, ...props }, ref) => {
	const decoratedBsPrefix = useThemePrefix(componentPrefix, 'pagination');
	return (
		<ul
			ref={ref}
			{...props}
			className={classnames(
				className,
				decoratedBsPrefix,
				size && `${decoratedBsPrefix}-${size}`
			)}
		/>
	);
});

const _default = Object.assign(Pagination, {
	First: PageItem.First,
	Prev: PageItem.Prev,
	Ellipsis: PageItem.Ellipsis,
	Item: PageItem,
	Next: PageItem.Next,
	Last: PageItem.Last
});
export default _default;
