import React from 'react';

import { classnames } from '../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';
import {
	useThemeMinBreakpoint,
	useThemeBreakpoints,
	useThemePrefix
} from '../../../utils/ThemePrefix';
type RowColWidth =
	| number
	| '1'
	| '2'
	| '3'
	| '4'
	| '5'
	| '6'
	| '7'
	| '8'
	| '9'
	| '10'
	| '11'
	| '12'
	| 'auto';
type RowColumns =
	| RowColWidth
	| {
			cols?: RowColWidth;
	  };
interface RowProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	xs?: RowColumns;
	sm?: RowColumns;
	md?: RowColumns;
	lg?: RowColumns;
	xl?: RowColumns;
	xxl?: RowColumns;
	[key: string]: any;
}
export const Row: IComponentPrefixRefForwardingComponent<'div', RowProps> =
	/*#__PURE__*/ React.forwardRef(
		(
			{
				componentPrefix,
				className,
				// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
				as: Component = 'div',
				...props
			},
			ref
		) => {
			const decoratedBsPrefix = useThemePrefix(componentPrefix, 'row');
			const breakpoints = useThemeBreakpoints();
			const minBreakpoint = useThemeMinBreakpoint();
			const sizePrefix = `${decoratedBsPrefix}-cols`;
			const classes: string[] = [];
			breakpoints.forEach((brkPoint) => {
				const propValue = props[brkPoint];
				delete props[brkPoint];
				let cols;
				if (propValue != null && typeof propValue === 'object') {
					({ cols } = propValue);
				} else {
					cols = propValue;
				}
				const infix = brkPoint !== minBreakpoint ? `-${brkPoint}` : '';
				if (cols != null) classes.push(`${sizePrefix}${infix}-${cols}`);
			});
			return (
				<Component
					ref={ref}
					{...props}
					className={classnames(className, decoratedBsPrefix, ...classes)}
				/>
			);
		}
	);
