import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import { IPrefixWithAsProps } from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

type AspectRatio = '1x1' | '4x3' | '16x9' | '21x9' | string;
interface RatioProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLDivElement> {
	children: React.ReactChild;
	aspectRatio?: AspectRatio | number;
}

const defaultProps = {
	aspectRatio: '1x1'
};
function toPercent(num: number): string {
	if (num <= 0) return '100%';
	if (num < 1) return `${num * 100}%`;
	return `${num}%`;
}

const Ratio: React.ForwardRefExoticComponent<
	RatioProps & React.RefAttributes<HTMLDivElement>
> = React.forwardRef(
	(
		{ componentPrefix, className, children, aspectRatio, style, ...props },
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'ratio');
		const isCustomRatio = typeof aspectRatio === 'number';
		return (
			<div
				ref={ref}
				{...props}
				style={{
					...style,
					...(isCustomRatio && {
						'--bs-aspect-ratio': toPercent(aspectRatio)
					})
				}}
				className={classnames(
					componentPrefix,
					className,
					!isCustomRatio && `${componentPrefix}-${aspectRatio}`
				)}
			>
				{' '}
				{React.Children.only(children)}
			</div>
		);
	}
);
Ratio.defaultProps = defaultProps;
export default Ratio;
