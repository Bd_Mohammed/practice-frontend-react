import useTimeout from '@restart/hooks/useTimeout';
import { TransitionComponent } from '@restart/ui/types';
import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps,
	Variant
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

import ToastContext from './ToastContext/indes';
import ToastFade from './ToastFade';

interface ToastProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	animation?: boolean;
	autohide?: boolean;
	delay?: number;
	onClose?: (e?: React.MouseEvent | React.KeyboardEvent) => void;
	show?: boolean;
	transition?: TransitionComponent;
	bg?: Variant;
}

//@ts-ignore
const Toast: IComponentPrefixRefForwardingComponent<'div', ToastProps> =
	React.forwardRef<any, any>(
		(
			{
				componentPrefix,
				className,
				transition: Transition = ToastFade,
				show = true,
				animation = true,
				delay = 5000,
				autohide = false,
				onClose,
				bg,
				...props
			},
			ref
		) => {
			componentPrefix = useThemePrefix(componentPrefix, 'toast');

			// We use refs for these, because we don't want to restart the auto-hide
			// timer in case these values change.
			const delayRef = React.useRef(delay);
			const onCloseRef = React.useRef(onClose);
			React.useEffect(() => {
				delayRef.current = delay;
				onCloseRef.current = onClose;
			}, [delay, onClose]);
			const autohideTimeout = useTimeout();
			const autohideToast = !!(autohide && show);
			const autohideFunc = React.useCallback(() => {
				if (autohideToast) {
					onCloseRef.current == null ? void 0 : onCloseRef.current();
				}
			}, [autohideToast]);
			React.useEffect(() => {
				// Only reset timer if show or autohide changes.
				autohideTimeout.set(autohideFunc, delayRef.current);
			}, [autohideTimeout, autohideFunc]);
			const toastContext = React.useMemo(
				() => ({
					onClose
				}),
				[onClose]
			);
			const hasAnimation = !!(Transition && animation);
			const toast = (
				<div
					{...props}
					ref={ref}
					className={classnames(
						componentPrefix,
						className,
						bg && `bg-${bg}`,
						!hasAnimation && (show ? 'show' : 'hide')
					)}
					role="alert"
					aria-live="assertive"
					aria-atomic="true"
				/>
			);
			return (
				<ToastContext.Provider value={toastContext}>
					{hasAnimation && Transition ? (
						<Transition
							in={show}
							unmountOnExit={true}
						>
							{toast}
						</Transition>
					) : null}
				</ToastContext.Provider>
			);
		}
	);

export default Toast;
