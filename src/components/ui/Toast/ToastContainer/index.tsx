import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

type ToastPosition =
	| 'top-start'
	| 'top-center'
	| 'top-end'
	| 'middle-start'
	| 'middle-center'
	| 'middle-end'
	| 'bottom-start'
	| 'bottom-center'
	| 'bottom-end';

interface ToastContainerProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	position?: ToastPosition;
	containerPosition?: string;
}

const positionClasses = {
	'top-start': 'top-0 start-0',
	'top-center': 'top-0 start-50 translate-middle-x',
	'top-end': 'top-0 end-0',
	'middle-start': 'top-50 start-0 translate-middle-y',
	'middle-center': 'top-50 start-50 translate-middle',
	'middle-end': 'top-50 end-0 translate-middle-y',
	'bottom-start': 'bottom-0 start-0',
	'bottom-center': 'bottom-0 start-50 translate-middle-x',
	'bottom-end': 'bottom-0 end-0'
};

const ToastContainer: IComponentPrefixRefForwardingComponent<
	'div',
	ToastContainerProps
> = React.forwardRef(
	(
		{
			componentPrefix,
			position,
			containerPosition = 'absolute',
			className,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'div',
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'toast-container');
		return (
			<Component
				ref={ref}
				{...props}
				className={classnames(
					componentPrefix,
					position && [
						containerPosition ? `position-${containerPosition}` : null,
						positionClasses[position]
					],
					className
				)}
			/>
		);
	}
);
export default ToastContainer;
