import * as React from 'react';
interface ToastContextType {
	onClose?: (e?: React.MouseEvent | React.KeyboardEvent) => void;
}
//@ts-ignore
const ToastContext: React.Context<ToastContextType> = React.createContext({
	// eslint-disable-next-line @typescript-eslint/no-empty-function
	onClose() {}
});
export default ToastContext;
