import * as React from 'react';
import Transition, {
	ENTERING,
	EXITING
} from 'react-transition-group/Transition';

import { Fade, FadeProps } from '../../Fade';

const fadeStyles = {
	[ENTERING]: 'showing',
	[EXITING]: 'showing show'
};

const ToastFade: React.ForwardRefExoticComponent<
	FadeProps & React.RefAttributes<Transition<any>>
> = React.forwardRef((props, ref) => (
	<Fade
		{...props}
		ref={ref}
		transitionClasses={fadeStyles}
	/>
));
export default ToastFade;
