import useEventCallback from '@restart/hooks/useEventCallback';
import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	CloseButtonVariant,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import { CloseButton } from '../../CloseButton';
import ToastContext from '../ToastContext/indes';

interface ToastHeaderProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLDivElement> {
	closeLabel?: string;
	closeVariant?: CloseButtonVariant;
	closeButton?: boolean;
}

const defaultProps = {
	closeLabel: 'Close',
	closeButton: true
};
const ToastHeader: React.ForwardRefExoticComponent<
	ToastHeaderProps & React.RefAttributes<HTMLDivElement>
> = React.forwardRef(
	(
		{
			componentPrefix,
			closeLabel,
			closeVariant,
			closeButton,
			className,
			children,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'toast-header');
		const context = React.useContext(ToastContext);
		const handleClick = useEventCallback((e: any) => {
			context == null
				? void 0
				: context.onClose == null
					? void 0
					: context.onClose(e);
		});
		return (
			<div
				ref={ref}
				{...props}
				className={classnames(componentPrefix, className)}
			>
				{children}
				{closeButton ? (
					<CloseButton
						aria-label={closeLabel}
						variant={closeVariant}
						onClick={handleClick}
						data-dismiss="toast"
					/>
				) : null}
			</div>
		);
	}
);
ToastHeader.defaultProps = defaultProps;
export default ToastHeader;
