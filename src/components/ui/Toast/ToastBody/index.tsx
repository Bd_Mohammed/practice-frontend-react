import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

const ToastBody: IComponentPrefixRefForwardingComponent<'div', unknown> =
	createWithComponentPrefix('toast-body');

export default ToastBody;
