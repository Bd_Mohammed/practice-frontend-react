import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

const PopoverBody: IComponentPrefixRefForwardingComponent<'div', unknown> =
	createWithComponentPrefix('popover-body');
export default PopoverBody;
