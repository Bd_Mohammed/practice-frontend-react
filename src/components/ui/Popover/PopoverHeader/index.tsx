import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

const PopoverHeader: IComponentPrefixRefForwardingComponent<'div', unknown> =
	createWithComponentPrefix('popover-header');
export default PopoverHeader;
