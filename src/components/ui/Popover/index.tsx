import { OverlayArrowProps } from '@restart/ui/Overlay';
import * as React from 'react';

import { Placement, PopperRef } from 'react-bootstrap/esm/types';

import getInitialPopperStyles from '../../../utils/getInitialPopperStyles';
import getOverlayDirection from '../../../utils/getOverlayDirection';
import { IPrefixWithAsProps } from '../../../utils/helper';

import { useThemeIsRTL, useThemePrefix } from '../../../utils/ThemePrefix';

import PopoverBody from './PopoverBody';
import PopoverHeader from './PopoverHeader';

interface PopoverProps
	extends React.HTMLAttributes<HTMLDivElement>,
		IPrefixWithAsProps {
	placement?: Placement;
	title?: string;
	arrowProps?: Partial<OverlayArrowProps>;
	body?: boolean;
	popper?: PopperRef;
	show?: boolean;
	hasDoneInitialMeasure?: boolean;
}

const defaultProps: { placement: Placement } = {
	placement: 'right'
};

const Popover: React.ForwardRefExoticComponent<
	PopoverProps & React.RefAttributes<HTMLDivElement>
> = React.forwardRef(
	(
		{
			componentPrefix,
			placement,
			className,
			style,
			children,
			body,
			arrowProps,
			hasDoneInitialMeasure,
			popper,
			show,
			...props
		},
		ref
	) => {
		const decoratedBsPrefix = useThemePrefix(componentPrefix, 'popover');
		const isRTL = useThemeIsRTL();
		const [primaryPlacement] =
			(placement == null ? void 0 : placement.split('-')) || [];
		const bsDirection = getOverlayDirection(primaryPlacement, isRTL);
		let computedStyle = style;
		if (show && !hasDoneInitialMeasure) {
			computedStyle = {
				...style,
				...getInitialPopperStyles(popper == null ? void 0 : popper.strategy)
			};
		}
		return (
			<div
				ref={ref}
				role="tooltip"
				style={computedStyle}
				x-placement={primaryPlacement}
				className={`${className} ${decoratedBsPrefix} ${primaryPlacement && `bs-popover-${bsDirection}`}`}
				{...props}
			>
				<div
					className="popover-arrow"
					{...arrowProps}
				></div>
				{body ? <PopoverBody>{children}</PopoverBody> : children}
			</div>
		);
	}
);
Popover.defaultProps = defaultProps;
const _default = Object.assign(Popover, {
	Header: PopoverHeader,
	Body: PopoverBody,
	POPPER_OFFSET: [0, 8]
});
export default _default;
