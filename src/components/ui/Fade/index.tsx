import { TransitionCallbacks } from '@restart/ui/types';
import * as React from 'react';
import Transition from 'react-transition-group/Transition';

import { classnames } from '../../../utils/classnames';
import { transitionEndListener } from '../../../utils/transitionEndListener';
import triggerBrowserReflow from '../../../utils/triggerBrowserReflow';
import { TransitionWrapper } from '../TransitionWrapper';
export interface FadeProps extends TransitionCallbacks {
	className?: string;
	in?: boolean;
	mountOnEnter?: boolean;
	unmountOnExit?: boolean;
	appear?: boolean;
	timeout?: number;
	children: React.ReactElement;
	transitionClasses?: Record<string, string>;
}
const defaultProps = {
	in: false,
	timeout: 300,
	mountOnEnter: false,
	unmountOnExit: false,
	appear: false
};
const fadeStyles = {
	entering: 'show',
	entered: 'show'
};
const Fade: React.ForwardRefExoticComponent<
	FadeProps & React.RefAttributes<Transition<any>>
> = React.forwardRef(
	(
		{ className, children, transitionClasses = {}, ...props }: FadeProps,
		ref
	) => {
		const handleEnter = React.useCallback(
			(node: HTMLElement, isAppearing: any) => {
				triggerBrowserReflow(node);
				props.onEnter == null ? void 0 : props.onEnter(node, isAppearing);
			},
			[props]
		);

		return (
			<TransitionWrapper
				ref={ref}
				addEndListener={transitionEndListener}
				{...props}
				onEnter={handleEnter}
				childRef={(children as any).ref}
				children={(status: keyof typeof fadeStyles, innerProps: any) =>
					React.cloneElement(children, {
						...innerProps,
						className: classnames(
							'fade',
							className,
							children.props.className,
							fadeStyles[status],
							transitionClasses[status]
						)
					})
				}
			/>
		);
	}
);

Fade.defaultProps = defaultProps;
export { Fade };
