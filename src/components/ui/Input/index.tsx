import React, { useRef } from 'react';
import styled from 'styled-components';

import Errors from '../Errors';
import { Icon } from '../Icon';

interface IStyledInput {
	className?: string;
	children?: JSX.Element;
	dataTestId: string;
	disabled?: boolean;
	onClick?: React.MouseEventHandler<HTMLButtonElement>;
	style?: React.CSSProperties;
	name: string;
	onChange?: React.ChangeEventHandler<HTMLInputElement>;
	required?: boolean;
	value?: string;
	placeholder: string;
	readOnly?: boolean;
	iconClassName?: string;
	type?: React.HTMLInputTypeAttribute;
	wrapperStyle?: React.CSSProperties;
	containerStyle?: React.CSSProperties;
	inputStyle?: React.CSSProperties;
	errors?: boolean;
	icon?: string;
}

const InputComponent: React.FC<IStyledInput> = ({
	className,
	containerStyle,
	errors,
	disabled,
	iconClassName,
	inputStyle,
	name,
	onChange,
	placeholder,
	readOnly,
	required,
	type,
	value,
	wrapperStyle
}) => {
	const inputRef = useRef<HTMLInputElement | null>(null);

	const handleClick = () => {
		if (inputRef && inputRef.current) inputRef.current.focus();
	};

	return (
		<div
			className={className}
			style={wrapperStyle}
		>
			<div
				onClick={handleClick}
				className="container"
				style={containerStyle}
			>
				{iconClassName && (
					<Icon
						dataTestId={`icon-${name}`}
						className={iconClassName}
					/>
				)}
				<input
					ref={inputRef}
					aria-label={name}
					data-testid={name}
					tabIndex={0}
					type={type}
					name={name}
					onChange={onChange}
					placeholder={placeholder}
					value={value}
					style={inputStyle}
					disabled={disabled}
					readOnly={readOnly}
				/>
			</div>
			{errors && !value && required && (
				<Errors data-testid="errors">Required!</Errors>
			)}
		</div>
	);
};

const Input = styled(InputComponent)<{
	errors?: boolean;
	value?: boolean;
	required?: boolean;
}>`
	height: 65px;
	position: relative;
	width: 100%;

	.container {
		width: 100%;
		margin-bottom: 10px;
		:not(:hover) {
			svg {
				color: ${({ errors, value, required }) =>
					errors && !value && required ? '#e80700' : '#ccc'};
			}
		}
	}

	input {
		color: #f7f7f7;
		padding: ${({ icon }) => `12px 0 12px ${icon ? 48 : 17}px`};
		width: 100%;
		font-size: 12px;
		border: 1px solid
			${({ errors, value, required }) =>
				errors && !value && required ? '#e80700' : '#888'};
		border-radius: 10px;
		width: 100%;
		transition:
			border,
			color 0.2s ease-in-out;
		background: transparent;

		:-webkit-autofill {
			-webkit-text-fill-color: #fff;
			box-shadow: 0 0 0px 1000px #222b36 inset;

			:focus {
				box-shadow: 0 0 0px 1000px #266798 inset;
			}
		}

		::placeholder {
			color: #ccc;
		}

		:hover {
			border: 1px solid #ccc;
		}

		:focus {
			outline: 0;
			border: 1px solid #ccc;
			// background: #266798;
		}
		width: 100%;
		padding: 10px;
		text-align: center;
	}
	i {
		padding: 10px;
		color: green;
		min-width: 50px;
		text-align: center;
		position: absolute;
	}
`;

export default Input;
