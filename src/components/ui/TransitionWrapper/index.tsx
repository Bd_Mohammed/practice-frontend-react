//@ts-nocheck
import useMergedRefs from '@restart/hooks/useMergedRefs';
import { forwardRef, useRef, useCallback, cloneElement } from 'react';
import { findDOMNode } from 'react-dom';
import Transition, {
	TransitionProps,
	TransitionStatus
} from 'react-transition-group/Transition';

type TransitionWrapperProps = TransitionProps & {
	childRef?: React.Ref<unknown>;
	children:
		| React.ReactElement
		| ((
				status: TransitionStatus,
				props: Record<string, unknown>
		  ) => React.ReactNode);
};

const safeFindDOMNode = (
	componentOrElement: React.Component | Element | null | undefined
): Element | Text | null => {
	if (componentOrElement && 'setState' in componentOrElement) {
		return findDOMNode(componentOrElement);
	}
	return componentOrElement != null ? componentOrElement : null;
};

export const TransitionWrapper: React.ForwardRefExoticComponent<
	(
		| Pick<
				import('react-transition-group/Transition').TimeoutProps<undefined> & {
					childRef?: React.Ref<unknown> | undefined;
					children:
						| React.ReactElement<any, string | React.JSXElementConstructor<any>>
						| ((
								status: TransitionStatus,
								props: Record<string, unknown>
						  ) => React.ReactNode);
				},
				keyof import('react-transition-group/Transition').TimeoutProps<undefined>
		  >
		| Pick<
				import('react-transition-group/Transition').EndListenerProps<undefined> & {
					childRef?: React.Ref<unknown> | undefined;
					children:
						| React.ReactElement<any, string | React.JSXElementConstructor<any>>
						| ((
								status: TransitionStatus,
								props: Record<string, unknown>
						  ) => React.ReactNode);
				},
				keyof import('react-transition-group/Transition').EndListenerProps<undefined>
		  >
	) &
		React.RefAttributes<Transition<any>>
> = forwardRef(
	(
		{
			onEnter,
			onEntering,
			onEntered,
			onExit,
			onExiting,
			onExited,
			addEndListener,
			children,
			childRef,
			...props
		}: TransitionWrapperProps,
		ref
	) => {
		const nodeRef = useRef(null);
		const mergedRef = useMergedRefs(nodeRef, childRef);
		const attachRef = (r: React.Component | Element | null | undefined) => {
			mergedRef(safeFindDOMNode(r));
		};
		const normalize = (callback: any) => (param: any) => {
			if (callback && nodeRef.current) {
				callback(nodeRef.current, param);
			}
		};

		/* eslint-disable react-hooks/exhaustive-deps */
		const handleEnter = useCallback(normalize(onEnter), [onEnter]);
		const handleEntering = useCallback(normalize(onEntering), [onEntering]);
		const handleEntered = useCallback(normalize(onEntered), [onEntered]);
		const handleExit = useCallback(normalize(onExit), [onExit]);
		const handleExiting = useCallback(normalize(onExiting), [onExiting]);
		const handleExited = useCallback(normalize(onExited), [onExited]);
		const handleAddEndListener = useCallback(normalize(addEndListener), [
			addEndListener
		]);
		/* eslint-enable react-hooks/exhaustive-deps */
		return (
			<Transition
				ref={ref}
				{...props}
				onEnter={handleEnter}
				onEntered={handleEntered}
				onEntering={handleEntering}
				onExit={handleExit}
				onExited={handleExited}
				onExiting={handleExiting}
				addEndListener={handleAddEndListener}
				nodeRef={nodeRef}
			>
				{typeof children === 'function'
					? (status: any, innerProps: any) =>
							children(status, {
								...innerProps,
								ref: attachRef
							})
					: cloneElement(children as any, {
							ref: attachRef
						})}
			</Transition>
		);
	}
);
