import React, { forwardRef } from 'react';

import { classnames } from '../../../utils/classnames';
import { CloseButtonVariant } from '../../../utils/helper';

interface CloseButtonProps
	extends React.ButtonHTMLAttributes<HTMLButtonElement> {
	variant?: CloseButtonVariant;
	'aria-label'?: string;
}

const defaultProps = {
	'aria-label': 'Close'
};
const CloseButton: React.ForwardRefExoticComponent<
	CloseButtonProps & React.RefAttributes<HTMLButtonElement>
> = forwardRef(({ className, variant, ...props }, ref) => (
	<button
		ref={ref}
		type="button"
		className={classnames(
			'btn-close',
			variant && `btn-close-${variant}`,
			className
		)}
		{...props}
	/>
));

CloseButton.defaultProps = defaultProps;
export { CloseButton };
