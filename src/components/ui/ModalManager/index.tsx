import ModalManager, { ModalManagerOptions } from '@restart/ui/ModalManager';
import addClass from 'dom-helpers/addClass';
import css from 'dom-helpers/css';
import querySelectorAll from 'dom-helpers/querySelectorAll';
import removeClass from 'dom-helpers/removeClass';

const Selector = {
	FIXED_CONTENT: '.fixed-top, .fixed-bottom, .is-fixed, .sticky-top',
	STICKY_CONTENT: '.sticky-top',
	NAVBAR_TOGGLER: '.navbar-toggler'
};

class CustomModalManager extends ModalManager {
	private adjustAndStore(prop: any, element: HTMLElement, adjust: any) {
		const actual = element.style[prop];
		// TODO: DOMStringMap and CSSStyleDeclaration aren't strictly compatible
		// @ts-ignore
		element.dataset[prop] = actual;
		css(element, {
			[prop]: `${parseFloat(css(element, prop)) + adjust}px`
		});
	}
	private restore(prop: any, element: any) {
		const value = element.dataset[prop];
		if (value !== undefined) {
			delete element.dataset[prop];
			css(element, {
				[prop]: value
			});
		}
	}
	setContainerStyle(containerState: any): void {
		super.setContainerStyle(containerState);
		const container = this.getElement();
		addClass(container, 'modal-open');
		if (!containerState.scrollBarWidth) return;
		const paddingProp = this.isRTL ? 'paddingLeft' : 'paddingRight';
		const marginProp = this.isRTL ? 'marginLeft' : 'marginRight';
		querySelectorAll(container, Selector.FIXED_CONTENT).forEach((el) =>
			this.adjustAndStore(paddingProp, el, containerState.scrollBarWidth)
		);
		querySelectorAll(container, Selector.STICKY_CONTENT).forEach((el) =>
			this.adjustAndStore(marginProp, el, -containerState.scrollBarWidth)
		);
		querySelectorAll(container, Selector.NAVBAR_TOGGLER).forEach((el) =>
			this.adjustAndStore(marginProp, el, containerState.scrollBarWidth)
		);
	}
	removeContainerStyle(containerState: any): void {
		super.removeContainerStyle(containerState);
		const container = this.getElement();
		removeClass(container, 'modal-open');
		const paddingProp = this.isRTL ? 'paddingLeft' : 'paddingRight';
		const marginProp = this.isRTL ? 'marginLeft' : 'marginRight';
		querySelectorAll(container, Selector.FIXED_CONTENT).forEach((el) =>
			this.restore(paddingProp, el)
		);
		querySelectorAll(container, Selector.STICKY_CONTENT).forEach((el) =>
			this.restore(marginProp, el)
		);
		querySelectorAll(container, Selector.NAVBAR_TOGGLER).forEach((el) =>
			this.restore(marginProp, el)
		);
	}
}

let sharedManager: CustomModalManager;
export function getSharedManager(
	options?: ModalManagerOptions
): CustomModalManager {
	if (!sharedManager) sharedManager = new CustomModalManager(options);
	return sharedManager;
}

export default ModalManager;
