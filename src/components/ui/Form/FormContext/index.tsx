import * as React from 'react';

interface FormContextType {
	controlId?: any;
}

const FormContext: React.Context<FormContextType> =
	React.createContext<FormContextType>({});
export default FormContext;
