import * as React from 'react';

import { classnames } from '../../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../../utils/helper';
import { useThemePrefix } from '../../../../../utils/ThemePrefix';
import FormContext from '../../FormContext';

type FormCheckInputType = 'checkbox' | 'radio';
export interface FormCheckInputProps
	extends IPrefixWithAsProps,
		React.InputHTMLAttributes<HTMLInputElement> {
	type?: FormCheckInputType;
	isValid?: boolean;
	isInvalid?: boolean;
}
const FormCheckInput: IComponentPrefixRefForwardingComponent<
	'input',
	FormCheckInputProps
> = React.forwardRef(
	(
		{
			id,
			componentPrefix,
			className,
			type = 'checkbox',
			isValid = false,
			isInvalid = false,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'input',
			...props
		},
		ref
	) => {
		const { controlId } = React.useContext(FormContext);
		componentPrefix = useThemePrefix(componentPrefix, 'form-check-input');
		return (
			<Component
				{...props}
				ref={ref}
				type={type}
				id={id || controlId}
				className={classnames(
					className,
					componentPrefix,
					isValid && 'is-valid',
					isInvalid && 'is-invalid'
				)}
			/>
		);
	}
);
export default FormCheckInput;
