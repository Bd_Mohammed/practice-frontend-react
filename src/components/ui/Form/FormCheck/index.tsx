import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import ElementChildren from '../../../../utils/elementChildren';
import {
	FeedbackType,
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import Feedback from '../../Feedback';

import FormContext from '../FormContext';

import FormCheckInput from './FormCheckInput';
import FormCheckLabel from './FormCheckLabel';

type FormCheckType = 'checkbox' | 'radio' | 'switch';
interface FormCheckProps
	extends IPrefixWithAsProps,
		React.InputHTMLAttributes<HTMLInputElement> {
	inline?: boolean;
	reverse?: boolean;
	disabled?: boolean;
	label?: React.ReactNode;
	type?: FormCheckType;
	isValid?: boolean;
	isInvalid?: boolean;
	feedbackTooltip?: boolean;
	feedback?: React.ReactNode;
	feedbackType?: FeedbackType;
	bsSwitchPrefix?: string;
}

const FormCheck: IComponentPrefixRefForwardingComponent<
	'input',
	FormCheckProps
> = React.forwardRef(
	(
		{
			id,
			componentPrefix,
			bsSwitchPrefix,
			inline = false,
			reverse = false,
			disabled = false,
			isValid = false,
			isInvalid = false,
			feedbackTooltip = false,
			feedback,
			feedbackType,
			className,
			style,
			title = '',
			type = 'checkbox',
			label,
			children,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as = 'input',
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'form-check');
		bsSwitchPrefix = useThemePrefix(bsSwitchPrefix, 'form-switch');
		const { controlId } = React.useContext(FormContext);
		const innerFormContext = React.useMemo(
			() => ({
				controlId: id || controlId
			}),
			[controlId, id]
		);
		const hasLabel =
			(!children && label != null && label !== false) ||
			ElementChildren.hasChildOfType(children, FormCheckLabel);
		const input = (
			<FormCheckInput
				{...props}
				typ={type === 'switch' ? 'checkbox' : type}
				ref={ref}
				isValid={isValid}
				isInvalid={isInvalid}
				disabled={disabled}
				as={as}
			/>
		);
		return (
			<FormContext.Provider value={innerFormContext}>
				<div
					style={style}
					className={classnames(
						className,
						hasLabel && componentPrefix,
						inline && `${componentPrefix}-inline`,
						reverse && `${componentPrefix}-reverse`,
						type === 'switch' && bsSwitchPrefix
					)}
				>
					{children || (
						<>
							{input}
							{hasLabel && <FormCheckLabel title={title}>{label}</FormCheckLabel>}
							{feedback && (
								<Feedback
									type={feedbackType}
									tooltip={feedbackTooltip}
								>
									{feedback}
								</Feedback>
							)}
						</>
					)}
				</div>
			</FormContext.Provider>
		);
	}
);

const _default = Object.assign(FormCheck, {
	Input: FormCheckInput,
	Label: FormCheckLabel
});
export default _default;
