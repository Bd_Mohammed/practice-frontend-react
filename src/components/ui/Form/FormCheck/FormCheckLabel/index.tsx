import * as React from 'react';

import { classnames } from '../../../../../utils/classnames';
import { IPrefixWithAsProps } from '../../../../../utils/helper';
import { useThemePrefix } from '../../../../../utils/ThemePrefix';
import FormContext from '../../FormContext';

interface FormCheckLabelProps
	extends React.LabelHTMLAttributes<HTMLLabelElement>,
		IPrefixWithAsProps {}
const FormCheckLabel: React.ForwardRefExoticComponent<
	FormCheckLabelProps & React.RefAttributes<HTMLLabelElement>
> = React.forwardRef(
	({ componentPrefix, className, htmlFor, ...props }, ref) => {
		const { controlId } = React.useContext(FormContext);
		componentPrefix = useThemePrefix(componentPrefix, 'form-check-label');
		return (
			<label
				{...props}
				ref={ref}
				htmlFor={htmlFor || controlId}
				className={classnames(className, componentPrefix)}
			/>
		);
	}
);
export default FormCheckLabel;
