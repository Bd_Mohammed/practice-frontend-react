import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IPrefixProps,
	IPrefixRefForwardingComponent
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import FormContext from '../FormContext';

interface FormSelectProps
	extends IPrefixProps,
		Omit<React.SelectHTMLAttributes<HTMLSelectElement>, 'size'> {
	htmlSize?: number;
	size?: 'sm' | 'lg';
	isValid?: boolean;
	isInvalid?: boolean;
}
const FormSelect: IPrefixRefForwardingComponent<'select', FormSelectProps> =
	React.forwardRef<any, FormSelectProps>(
		(
			{
				componentPrefix,
				size,
				htmlSize,
				className,
				isValid = false,
				isInvalid = false,
				id,
				...props
			},
			ref
		) => {
			const { controlId } = React.useContext(FormContext);
			componentPrefix = useThemePrefix(componentPrefix, 'form-select');
			return (
				<select
					{...props}
					size={htmlSize}
					ref={ref}
					className={classnames(
						className,
						componentPrefix,
						size && `${componentPrefix}-${size}`,
						isValid && `is-valid`,
						isInvalid && `is-invalid`
					)}
					id={id || controlId}
				/>
			);
		}
	);

export default FormSelect;
