import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

const FormFloating: IComponentPrefixRefForwardingComponent<'div', unknown> =
	createWithComponentPrefix('form-floating');
export default FormFloating;
