import * as React from 'react';

import warning from 'warning';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import Feedback from '../../Feedback';

import FormContext from '../FormContext';

type FormControlElement = HTMLInputElement | HTMLTextAreaElement;
interface FormControlProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<FormControlElement> {
	htmlSize?: number;
	size?: 'sm' | 'lg';
	plaintext?: boolean;
	readOnly?: boolean;
	disabled?: boolean;
	value?: string | string[] | number;
	onChange?: React.ChangeEventHandler<FormControlElement>;
	type?: string;
	isValid?: boolean;
	isInvalid?: boolean;
}

const FormControl: IComponentPrefixRefForwardingComponent<
	'input',
	FormControlProps
> = React.forwardRef(
	(
		{
			componentPrefix,
			type,
			size,
			htmlSize,
			id,
			className,
			isValid = false,
			isInvalid = false,
			plaintext,
			readOnly,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'input',
			...props
		},
		ref
	) => {
		const { controlId } = React.useContext(FormContext);
		componentPrefix = useThemePrefix(componentPrefix, 'form-control');
		let classes;
		if (plaintext) {
			classes = {
				[`${componentPrefix}-plaintext`]: true
			};
		} else {
			classes = {
				[componentPrefix]: true,
				[`${componentPrefix}-${size}`]: size
			};
		}
		process.env.NODE_ENV !== 'production'
			? warning(
					controlId == null || !id,
					'`controlId` is ignored on `<FormControl>` when `id` is specified.'
				)
			: void 0;
		return (
			<Component
				{...props}
				type={type}
				size={htmlSize}
				ref={ref}
				readOnly={readOnly}
				id={id || controlId}
				className={classnames(
					className,
					classes,
					isValid && `is-valid`,
					isInvalid && `is-invalid`,
					type === 'color' && `${componentPrefix}-color`
				)}
			/>
		);
	}
);

const _default = Object.assign(FormControl, {
	Feedback: Feedback
});
export default _default;
