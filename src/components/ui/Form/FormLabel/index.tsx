import * as React from 'react';

import warning from 'warning';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

import Col, { ColProps } from '../../Col';
import FormContext from '../FormContext';

interface FormLabelBaseProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	htmlFor?: string;
	visuallyHidden?: boolean;
}
interface FormLabelOwnProps extends FormLabelBaseProps {
	column?: false;
}
interface FormLabelWithColProps extends FormLabelBaseProps, ColProps {
	column: true | 'sm' | 'lg';
}
type FormLabelProps = FormLabelWithColProps | FormLabelOwnProps;

const defaultProps = {
	column: false,
	visuallyHidden: false
};

const FormLabel: IComponentPrefixRefForwardingComponent<
	'label',
	FormLabelProps
> = React.forwardRef(
	(
		{
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'label',
			componentPrefix,
			column,
			visuallyHidden,
			className,
			htmlFor,
			...props
		},
		ref
	) => {
		const { controlId } = React.useContext(FormContext);
		componentPrefix = useThemePrefix(componentPrefix, 'form-label');
		let columnClass = 'col-form-label';
		if (typeof column === 'string')
			columnClass = `${columnClass} ${columnClass}-${column}`;
		const classes = classnames(
			className,
			componentPrefix,
			visuallyHidden && 'visually-hidden',
			column && columnClass
		);
		process.env.NODE_ENV !== 'production'
			? warning(
					controlId == null || !htmlFor,
					'`controlId` is ignored on `<FormLabel>` when `htmlFor` is specified.'
				)
			: void 0;
		htmlFor = htmlFor || controlId;
		if (column)
			return (
				<Col
					ref={ref}
					as="label"
					className={classes}
					htmlFor={htmlFor}
					{...props}
				/>
			);
		return (
			/*#__PURE__*/
			// eslint-disable-next-line jsx-a11y/label-has-for, jsx-a11y/label-has-associated-control
			<Component
				ref={ref}
				className={classes}
				htmlFor={htmlFor}
				{...props}
			/>
		);
	}
);

FormLabel.defaultProps = defaultProps;
export default FormLabel;
