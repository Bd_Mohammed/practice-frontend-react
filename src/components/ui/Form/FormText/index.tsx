import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
interface FormTextProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	muted?: boolean;
}

const FormText: IComponentPrefixRefForwardingComponent<'small', FormTextProps> =
	React.forwardRef(
		// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
		(
			{ componentPrefix, className, as: Component = 'small', muted, ...props },
			ref
		) => {
			componentPrefix = useThemePrefix(componentPrefix, 'form-text');
			return (
				<Component
					{...props}
					ref={ref}
					className={classnames(className, componentPrefix, muted && 'text-muted')}
				/>
			);
		}
	);
export default FormText;
