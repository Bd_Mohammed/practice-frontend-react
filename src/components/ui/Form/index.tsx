import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import {
	IAsProp,
	IComponentPrefixRefForwardingComponent
} from '../../../utils/helper';

import FloatingLabel from '../FloatingLabel';

import { Switch } from '../Switch';

import FormCheck from './FormCheck';
import FormControl from './FormControl';
import FormFloating from './FormFloating';
import FormGroup from './FormGroup';
import FormLabel from './FormLabel';
import FormRange from './FormRange';
import FormSelect from './FormSelect';

import FormText from './FormText';

interface FormProps extends React.FormHTMLAttributes<HTMLFormElement>, IAsProp {
	validated?: boolean;
}

const Form: IComponentPrefixRefForwardingComponent<'form', FormProps> =
	React.forwardRef(
		(
			{
				className,
				validated,
				// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
				as: Component = 'form',
				...props
			},
			ref
		) => (
			<Component
				{...props}
				ref={ref}
				className={classnames(className, validated && 'was-validated')}
			/>
		)
	);

const _default = Object.assign(Form, {
	Group: FormGroup,
	Control: FormControl,
	Floating: FormFloating,
	Check: FormCheck,
	Switch: Switch,
	Label: FormLabel,
	Text: FormText,
	Range: FormRange,
	Select: FormSelect,
	FloatingLabel: FloatingLabel
});

export default _default;
