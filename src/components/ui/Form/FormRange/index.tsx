import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import { IPrefixProps } from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import FormContext from '../FormContext';

interface FormRangeProps
	extends IPrefixProps,
		Omit<React.InputHTMLAttributes<HTMLInputElement>, 'type'> {}

const FormRange: React.ForwardRefExoticComponent<
	FormRangeProps & React.RefAttributes<HTMLInputElement>
> = React.forwardRef(({ componentPrefix, className, id, ...props }, ref) => {
	const { controlId } = React.useContext(FormContext);
	componentPrefix = useThemePrefix(componentPrefix, 'form-range');
	return (
		<input
			{...props}
			type="range"
			ref={ref}
			className={classnames(className, componentPrefix)}
			id={id || controlId}
		/>
	);
});

export default FormRange;
