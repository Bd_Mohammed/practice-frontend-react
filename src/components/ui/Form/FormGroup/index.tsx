import * as React from 'react';

import {
	IAsProp,
	IComponentPrefixRefForwardingComponent
} from '../../../../utils/helper';
import FormContext from '../FormContext';

export interface FormGroupProps
	extends React.HTMLAttributes<HTMLElement>,
		IAsProp {
	controlId?: string;
}

const FormGroup: IComponentPrefixRefForwardingComponent<'div', FormGroupProps> =
	React.forwardRef(
		(
			{
				controlId,
				// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
				as: Component = 'div',
				...props
			},
			ref
		) => {
			const context = React.useMemo(
				() => ({
					controlId
				}),
				[controlId]
			);
			return (
				<FormContext.Provider value={context}>
					<Component
						{...props}
						ref={ref}
					></Component>
				</FormContext.Provider>
			);
		}
	);
export default FormGroup;
