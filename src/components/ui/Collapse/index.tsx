import transitionEnd from 'dom-helpers/transitionEnd';
import React, { cloneElement, forwardRef, useMemo } from 'react';

import Transition from 'react-transition-group/Transition';

import { MARGINS, collapseStyles } from '../../../data/constants';
import { classnames } from '../../../utils/classnames';
import { createChainedFunction } from '../../../utils/createChainedFunction';
import { style } from '../../../utils/dom-helper/css';
import { TransitionCallbacks } from '../../../utils/helper';
import { TransitionWrapper } from '../TransitionWrapper';

const getDefaultDimensionValue = (dimension: any, elem: any) => {
	const offset = `offset${dimension[0].toUpperCase()}${dimension.slice(1)}`;
	const value = elem[offset];
	// @ts-ignore
	const margins = MARGINS[dimension];

	return (
		value +
		// @ts-ignore
		parseInt(style(elem, margins[0]), 10) +
		// @ts-ignore
		parseInt(style(elem, margins[1]), 10)
	);
};

// function parseDuration(node: HTMLElement, property: string) {
// 	//@ts-ignore
// 	const str = style(node, property) || '';
// 	const mult = str.indexOf('ms') === -1 ? 1000 : 1;
// 	return parseFloat(str) * mult;
// }
// function transitionEndListener(element: HTMLElement, handler: (e: TransitionEvent) => void): void {
//     const duration = parseDuration(element, 'transitionDuration');
//     const delay = parseDuration(element, 'transitionDelay');
//     const remove = transitionEnd(element, e => {
//         if (e.target === element) {
//             remove();
//             handler(e);
//         }
//     }, duration + delay);
// }
// reading a dimension prop will cause the browser to recalculate,
// which will let our animations work
function triggerBrowserReflow(node: HTMLElement): void {
	// eslint-disable-next-line @typescript-eslint/no-unused-expressions
	node.offsetHeight;
}

type Dimension = 'height' | 'width';

interface CollapseProps
	extends TransitionCallbacks,
		Pick<React.HTMLAttributes<HTMLElement>, 'role'> {
	className?: string;
	in?: boolean;
	mountOnEnter?: boolean;
	unmountOnExit?: boolean;
	appear?: boolean;
	timeout?: number;
	dimension?: Dimension | (() => Dimension);
	getDimensionValue?: (dimension: Dimension, element: HTMLElement) => number;
	children: React.ReactElement;
}
export const Collapse: React.ForwardRefExoticComponent<
	CollapseProps & React.RefAttributes<Transition<any>>
> = forwardRef(
	(
		{
			onEnter,
			onEntering,
			onEntered,
			onExit,
			onExiting,
			className,
			children,
			dimension = 'height',
			getDimensionValue = getDefaultDimensionValue,
			...props
		}: CollapseProps,
		ref
	) => {
		/* Compute dimension */
		const computedDimension =
			typeof dimension === 'function' ? dimension() : dimension;

		/* -- Expanding -- */
		const handleEnter = useMemo(
			() =>
				createChainedFunction((elem: any) => {
					elem.style[computedDimension] = '0';
				}, onEnter),
			[computedDimension, onEnter]
		);
		const handleEntering = useMemo(
			() =>
				createChainedFunction((elem: any) => {
					const scroll = `scroll${computedDimension[0].toUpperCase()}${computedDimension.slice(1)}`;
					elem.style[computedDimension] = `${elem[scroll]}px`;
				}, onEntering),
			[computedDimension, onEntering]
		);
		const handleEntered = useMemo(
			() =>
				createChainedFunction((elem: any) => {
					elem.style[computedDimension] = null;
				}, onEntered),
			[computedDimension, onEntered]
		);

		/* -- Collapsing -- */
		const handleExit = useMemo(
			() =>
				createChainedFunction((elem: any) => {
					elem.style[computedDimension] =
						`${getDimensionValue(computedDimension, elem)}px`;
					triggerBrowserReflow(elem);
				}, onExit),
			[onExit, getDimensionValue, computedDimension]
		);
		const handleExiting = useMemo(
			() =>
				createChainedFunction((elem: any) => {
					elem.style[computedDimension] = null;
				}, onExiting),
			[computedDimension, onExiting]
		);
		return (
			<TransitionWrapper
				ref={ref}
				addEndListener={transitionEnd}
				{...props}
				aria-expanded={props.role ? props.in : null}
				onEnter={handleEnter}
				onEntering={handleEntering}
				onEntered={handleEntered}
				onExit={handleExit}
				onExiting={handleExiting}
				childRef={(children as any).ref}
				children={(state: keyof typeof collapseStyles, innerProps: any) =>
					cloneElement(children, {
						...innerProps,
						className: classnames(
							className,
							children.props.className,
							collapseStyles[state],
							computedDimension === 'width' && 'collapse-horizontal'
						)
					})
				}
			/>
		);
	}
);
