import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

export const ModalBody: IComponentPrefixRefForwardingComponent<'div', unknown> =
	createWithComponentPrefix('modal-body');
