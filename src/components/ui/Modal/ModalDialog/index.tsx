import React from 'react';

import { classnames } from '../../../../utils/classnames';
import { IPrefixProps } from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

export interface ModalDialogProps
	extends React.HTMLAttributes<HTMLDivElement>,
		IPrefixProps {
	size?: 'sm' | 'lg' | 'xl';
	fullscreen?:
		| true
		| string
		| 'sm-down'
		| 'md-down'
		| 'lg-down'
		| 'xl-down'
		| 'xxl-down';
	centered?: boolean;
	scrollable?: boolean;
	contentClassName?: string;
}

export const ModalDialog: React.ForwardRefExoticComponent<
	ModalDialogProps & React.RefAttributes<HTMLDivElement>
> = React.forwardRef(
	(
		{
			componentPrefix,
			className,
			contentClassName,
			centered,
			size,
			fullscreen,
			children,
			scrollable,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'modal');
		const dialogClass = `${componentPrefix}-dialog`;
		const fullScreenClass =
			typeof fullscreen === 'string'
				? `${componentPrefix}-fullscreen-${fullscreen}`
				: `${componentPrefix}-fullscreen`;
		return (
			<div
				{...props}
				ref={ref}
				className={classnames(
					dialogClass,
					className,
					size && `${componentPrefix}-${size}`,
					centered && `${dialogClass}-centered`,
					scrollable && `${dialogClass}-scrollable`,
					fullscreen && fullScreenClass
				)}
				children={
					<div
						className={classnames(`${componentPrefix}-content`, contentClassName)}
						children={children}
					/>
				}
			/>
		);
	}
);
