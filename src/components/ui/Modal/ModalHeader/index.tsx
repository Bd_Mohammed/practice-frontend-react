import React from 'react';
import { AbstractModalHeaderProps } from 'react-bootstrap/esm/AbstractModalHeader';

import { classnames } from '../../../../utils/classnames';
import { IPrefixProps } from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import { AbstractModalHeader } from '../../AbstractModalHeader';

export interface ModalHeaderProps
	extends AbstractModalHeaderProps,
		IPrefixProps {}
const defaultProps = {
	closeLabel: 'Close',
	closeButton: false
};

const ModalHeader: React.ForwardRefExoticComponent<
	ModalHeaderProps & React.RefAttributes<HTMLDivElement>
> = React.forwardRef(({ componentPrefix, className, ...props }, ref) => {
	componentPrefix = useThemePrefix(componentPrefix, 'modal-header');
	return (
		<AbstractModalHeader
			ref={ref}
			{...props}
			className={classnames(className, componentPrefix)}
		/>
	);
});

ModalHeader.defaultProps = defaultProps;
export { ModalHeader };
