import * as React from 'react';

type Modal = {
	onHide: () => void;
};

const ModalContext: React.Context<Modal> = React.createContext<Modal>({
	onHide: () => {}
});
export default ModalContext;
