import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

export const ModalFooter: IComponentPrefixRefForwardingComponent<
	'div',
	unknown
> = createWithComponentPrefix('modal-footer');
