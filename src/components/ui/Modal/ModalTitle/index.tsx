import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { divWithClassName } from '../../../../utils/divWithClassName';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

const DivStyledAsH4 = divWithClassName('h4');
export const ModalTitle: IComponentPrefixRefForwardingComponent<
	'div',
	unknown
> = createWithComponentPrefix('modal-title', {
	Component: DivStyledAsH4
});
