/* eslint-disable no-restricted-globals */

// import { ModalDialog } from './ModalDialog';

import useCallbackRef from '@restart/hooks/useCallbackRef';

import useEventCallback from '@restart/hooks/useEventCallback';
import useMergedRefs from '@restart/hooks/useMergedRefs';

import useWillUnmount from '@restart/hooks/useWillUnmount';
import { BaseModalProps } from '@restart/ui/Modal';
import canUseDOM from 'dom-helpers/canUseDOM';
import ownerDocument from 'dom-helpers/ownerDocument';
import removeEventListener from 'dom-helpers/removeEventListener';
import scrollbarSize from 'dom-helpers/scrollbarSize';
import transitionEnd from 'dom-helpers/transitionEnd';
import React from 'react';

import { classnames } from '../../../utils/classnames';
import { IComponentPrefixRefForwardingComponent } from '../../../utils/helper';
import { getSharedManager } from '../../../utils/ModalManager';
import { useThemeIsRTL, useThemePrefix } from '../../../utils/ThemePrefix';
import { Fade, FadeProps } from '../Fade';

export interface ModalProps
	extends Omit<
		BaseModalProps,
		| 'role'
		| 'renderBackdrop'
		| 'renderDialog'
		| 'transition'
		| 'backdropTransition'
		| 'children'
	> {
	size?: 'sm' | 'lg' | 'xl';
	fullscreen?:
		| true
		| string
		| 'sm-down'
		| 'md-down'
		| 'lg-down'
		| 'xl-down'
		| 'xxl-down';
	bsPrefix?: string;
	centered?: boolean;
	backdropClassName?: string;
	animation?: boolean;
	dialogClassName?: string;
	contentClassName?: string;
	dialogAs?: React.ElementType;
	scrollable?: boolean;
	[other: string]: any;
}

// const defaultProps = {
//     show: false,
//     backdrop: true,
//     keyboard: true,
//     autoFocus: true,
//     enforceFocus: true,
//     restoreFocus: true,
//     animation: true,
//     dialogAs: ModalDialog
// };

function DialogTransition(props: FadeProps) {
	return <Fade {...props} />;
}
function BackdropTransition(props: FadeProps) {
	return <Fade {...props} />;
}

export const Modal: IComponentPrefixRefForwardingComponent<'div', ModalProps> =
	React.forwardRef(
		(
			{
				componentPrefix,
				className,
				style,
				dialogClassName,
				contentClassName,
				children,
				dialogAs: Dialog,
				'aria-labelledby': ariaLabelledby,
				'aria-describedby': ariaDescribedby,
				'aria-label': ariaLabel,
				/* BaseModal props */

				show,
				animation,
				backdrop,
				keyboard,
				onEscapeKeyDown,
				onShow,
				onHide,
				container,
				autoFocus,
				enforceFocus,
				restoreFocus,
				restoreFocusOptions,
				onEntered,
				onExit,
				onExiting,
				onEnter,
				onEntering,
				onExited,
				backdropClassName,
				manager: propsManager,
				...props
			},
			ref
		) => {
			const [modalStyle, setStyle] = React.useState({});
			const [animateStaticModal, setAnimateStaticModal] = React.useState(false);
			const waitingForMouseUpRef = React.useRef(false);
			const ignoreBackdropClickRef = React.useRef(false);
			const removeStaticModalAnimationRef = React.useRef(null);
			const [modal, setModalRef] = useCallbackRef();
			const mergedRef = useMergedRefs(ref, setModalRef);
			// const handleHide = useEventCallback(onHide);
			const isRTL = useThemeIsRTL();
			componentPrefix = useThemePrefix(componentPrefix, 'modal');
			// const modalContext = React.useMemo(() => ({
			//     onHide: handleHide
			// }), [handleHide]);
			function getModalManager() {
				if (propsManager) return propsManager;
				return getSharedManager({
					isRTL
				});
			}
			function updateDialogStyle(node: HTMLElement) {
				if (!canUseDOM) return;
				const containerIsOverflowing = getModalManager().getScrollbarWidth() > 0;
				const modalIsOverflowing =
					node.scrollHeight > ownerDocument(node).documentElement.clientHeight;
				setStyle({
					paddingRight:
						containerIsOverflowing && !modalIsOverflowing
							? scrollbarSize()
							: undefined,
					paddingLeft:
						!containerIsOverflowing && modalIsOverflowing
							? scrollbarSize()
							: undefined
				});
			}
			const handleWindowResize = useEventCallback(() => {
				if (modal) {
					updateDialogStyle((modal as any).dialog);
				}
			});
			useWillUnmount(() => {
				removeEventListener(window as any, 'resize', handleWindowResize);
				removeStaticModalAnimationRef.current == null
					? void 0
					: (removeStaticModalAnimationRef as any).current();
			});

			// We prevent the modal from closing during a drag by detecting where the
			// click originates from. If it starts in the modal and then ends outside
			// don't close.
			const handleDialogMouseDown = () => {
				waitingForMouseUpRef.current = true;
			};
			const handleMouseUp = (e: any) => {
				if (
					waitingForMouseUpRef.current &&
					modal &&
					e.target === (modal as any).dialog
				) {
					ignoreBackdropClickRef.current = true;
				}
				waitingForMouseUpRef.current = false;
			};
			const handleStaticModalAnimation = () => {
				setAnimateStaticModal(true);
				(removeStaticModalAnimationRef as any).current = transitionEnd(
					(modal as any).dialog,
					() => {
						setAnimateStaticModal(false);
					}
				);
			};
			const handleStaticBackdropClick = (e: any) => {
				if (e.target !== e.currentTarget) {
					return;
				}
				handleStaticModalAnimation();
			};
			const handleClick = (e: any) => {
				if (backdrop === 'static') {
					handleStaticBackdropClick(e);
					return;
				}
				if (ignoreBackdropClickRef.current || e.target !== e.currentTarget) {
					ignoreBackdropClickRef.current = false;
					return;
				}
				onHide == null ? void 0 : onHide();
			};
			const handleEscapeKeyDown = (e: any) => {
				if (keyboard) {
					onEscapeKeyDown == null ? void 0 : onEscapeKeyDown(e);
				} else {
					// Call preventDefault to stop modal from closing in @restart/ui.
					e.preventDefault();
					if (backdrop === 'static') {
						// Play static modal animation.
						handleStaticModalAnimation();
					}
				}
			};
			const handleEnter = (node: HTMLElement, isAppearing: boolean) => {
				if (node) {
					updateDialogStyle(node);
				}
				onEnter == null ? void 0 : onEnter(node, isAppearing);
			};
			const handleExit = (node: HTMLElement) => {
				removeStaticModalAnimationRef.current == null
					? void 0
					: (removeStaticModalAnimationRef as any).current();
				onExit == null ? void 0 : onExit(node);
			};
			const handleEntering = (node: HTMLElement, isAppearing: boolean) => {
				onEntering == null ? void 0 : onEntering(node, isAppearing);

				// FIXME: This should work even when animation is disabled.
				addEventListener(window as any, 'resize' as any, handleWindowResize as any);
			};
			const handleExited = (node: HTMLElement) => {
				if (node) node.style.display = ''; // RHL removes it sometimes
				onExited == null ? void 0 : onExited(node);

				// FIXME: This should work even when animation is disabled.
				removeEventListener(window as any, 'resize', handleWindowResize);
			};
			const renderBackdrop = React.useCallback(
				(backdropProps: any) => (
					<div
						{...backdropProps}
						className={classnames(
							`${componentPrefix}-backdrop`,
							backdropClassName,
							!animation && 'show'
						)}
					/>
				),
				[animation, backdropClassName, componentPrefix]
			);

			const baseModalStyle = {
				...style,
				...modalStyle
			};

			// If `display` is not set to block, autoFocus inside the modal fails
			// https://github.com/react-bootstrap/react-bootstrap/issues/5102
			baseModalStyle.display = 'block';
			const renderDialog = (dialogProps: any) => (
				<div
					role={'dialog'}
					{...dialogProps}
					style={baseModalStyle}
					className={classnames(
						className,
						componentPrefix,
						animateStaticModal && `${componentPrefix}-static`,
						!animation && 'show'
					)}
					onClick={backdrop ? handleClick : undefined}
					onMouseUp={handleMouseUp}
					aria-label={ariaLabel}
					aria-labelledby={ariaLabelledby}
					aria-describedby={ariaDescribedby}
					children={React.createElement('Dialog', {
						...props,
						onMouseDown: handleDialogMouseDown,
						classNam: dialogClassName,
						contentClassName: contentClassName,
						children: children
					})}
				/>
			);
			return (
				<Modal
					show={show}
					ref={mergedRef}
					backdrop={backdrop}
					container={container}
					keyboard={true} // Always set true - see handleEscapeKeyDown
					autoFocus={autoFocus}
					enforceFocus={enforceFocus}
					restoreFocus={restoreFocus}
					restoreFocusOptions={restoreFocusOptions}
					onEscapeKeyDown={handleEscapeKeyDown}
					onShow={onShow}
					onHide={onHide}
					onEnter={handleEnter}
					onEntering={handleEntering}
					onEntered={onEntered}
					onExit={handleExit}
					onExiting={onExiting}
					onExited={handleExited}
					manager={getModalManager()}
					transition={animation ? DialogTransition : undefined}
					backdropTransition={animation ? BackdropTransition : undefined}
					renderBackdrop={renderBackdrop}
					renderDialog={renderDialog}
				/>
			);
		}
	);
