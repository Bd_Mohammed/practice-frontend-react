import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import { IPrefixWithAsProps } from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

export interface ImageProps
	extends IPrefixWithAsProps,
		React.ImgHTMLAttributes<HTMLImageElement> {
	fluid?: boolean;
	rounded?: boolean;
	roundedCircle?: boolean;
	thumbnail?: boolean;
}

// const propTypes: {
//     /**
//      * @default 'img'
//      */
//     bsPrefix: PropTypes.Requireable<string>;
//     /**
//      * Sets image as fluid image.
//      */
//     fluid: PropTypes.Requireable<boolean>;
//     /**
//      * Sets image shape as rounded.
//      */
//     rounded: PropTypes.Requireable<boolean>;
//     /**
//      * Sets image shape as circle.
//      */
//     roundedCircle: PropTypes.Requireable<boolean>;
//     /**
//      * Sets image shape as thumbnail.
//      */
//     thumbnail: PropTypes.Requireable<boolean>;
// };

const defaultProps = {
	fluid: false,
	rounded: false,
	roundedCircle: false,
	thumbnail: false
};

const Image: React.ForwardRefExoticComponent<
	ImageProps & React.RefAttributes<HTMLImageElement>
> = React.forwardRef(
	(
		{
			componentPrefix,
			className,
			fluid,
			rounded,
			roundedCircle,
			thumbnail,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'img');
		// eslint-disable-line jsx-a11y/alt-text
		return (
			<img
				ref={ref}
				{...props}
				className={classnames(
					className,
					fluid && `${componentPrefix}-fluid`,
					rounded && `rounded`,
					roundedCircle && `rounded-circle`,
					thumbnail && `${componentPrefix}-thumbnail`
				)}
			/>
		);
	}
);

Image.defaultProps = defaultProps;
export default Image;
