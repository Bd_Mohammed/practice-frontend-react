import Anchor, { AnchorProps } from '@restart/ui/Anchor';

export type AnchorProp = AnchorProps;

export default Anchor;
