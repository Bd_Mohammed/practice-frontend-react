import useEventCallback from '@restart/hooks/useEventCallback';
import { useNavItem } from '@restart/ui/NavItem';
import { makeEventKey } from '@restart/ui/SelectableContext';
import * as React from 'react';

import warning from 'warning';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

type Variant =
	| 'primary'
	| 'secondary'
	| 'success'
	| 'danger'
	| 'warning'
	| 'info'
	| 'dark'
	| 'light'
	| string;
interface ListGroupItemProps
	extends Omit<
			//BaseNavItemProps
			any,
			'onSelect'
		>,
		IPrefixWithAsProps {
	action?: boolean;
	onClick?: React.MouseEventHandler;
	variant?: Variant;
}

const ListGroupItem: IComponentPrefixRefForwardingComponent<
	'a',
	ListGroupItemProps
> = React.forwardRef(
	(
		{
			componentPrefix,
			active,
			disabled,
			eventKey,
			className,
			variant,
			action,
			as,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'list-group-item');
		const [navItemProps, meta] = useNavItem({
			key: makeEventKey(eventKey, props.href),
			active,
			...props
		});
		const handleClick = useEventCallback((event) => {
			if (disabled) {
				event.preventDefault();
				event.stopPropagation();
				return;
			}
			navItemProps.onClick(event);
		});
		if (disabled && props.tabIndex === undefined) {
			props.tabIndex = -1;
			props['aria-disabled'] = true;
		}

		// eslint-disable-next-line no-nested-ternary
		const Component = as || (action ? (props.href ? 'a' : 'button') : 'div');
		process.env.NODE_ENV !== 'production'
			? warning(
					as || !(!action && props.href),
					'`action=false` and `href` should not be used together.'
				)
			: void 0;
		return (
			<Component
				ref={ref}
				{...props}
				{...navItemProps}
				onClick={handleClick}
				className={classnames(
					className,
					componentPrefix,
					meta.isActive && 'active',
					disabled && 'disabled',
					variant && `${componentPrefix}-${variant}`,
					action && `${componentPrefix}-action`
				)}
			/>
		);
	}
);

export default ListGroupItem;
