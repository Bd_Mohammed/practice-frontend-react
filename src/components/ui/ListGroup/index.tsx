import { NavProps as BaseNavProps } from '@restart/ui/Nav';
import { EventKey } from '@restart/ui/types';
import * as React from 'react';

import { Nav } from 'react-bootstrap';
import { useUncontrolled } from 'uncontrollable';

import warning from 'warning';

import { classnames } from '../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';

import { useThemePrefix } from '../../../utils/ThemePrefix';

import ListGroupItem from './ListGroupItem';

interface ListGroupProps extends IPrefixWithAsProps, BaseNavProps {
	variant?: 'flush' | string;
	horizontal?: boolean | string | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';
	defaultActiveKey?: EventKey;
	numbered?: boolean;
}

const ListGroup: IComponentPrefixRefForwardingComponent<'div', ListGroupProps> =
	React.forwardRef((props, ref) => {
		const {
			className,
			componentPrefix: initialBsPrefix,
			variant,
			horizontal,
			numbered,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as = 'div',
			...controlledProps
		} = useUncontrolled(props, {
			activeKey: 'onSelect'
		});
		const componentPrefix = useThemePrefix(initialBsPrefix, 'list-group');
		let horizontalVariant;
		if (horizontal) {
			horizontalVariant =
				horizontal === true ? 'horizontal' : `horizontal-${horizontal}`;
		}
		process.env.NODE_ENV !== 'production'
			? warning(
					!(horizontal && variant === 'flush'),
					'`variant="flush"` and `horizontal` should not be used together.'
				)
			: void 0;
		return (
			<Nav
				ref={ref}
				{...controlledProps}
				as={as}
				className={classnames(
					className,
					componentPrefix,
					variant && `${componentPrefix}-${variant}`,
					horizontalVariant && `${componentPrefix}-${horizontalVariant}`,
					numbered && `${componentPrefix}-numbered`
				)}
			/>
		);
	});

const _default = Object.assign(ListGroup, {
	Item: ListGroupItem
});
export default _default;
