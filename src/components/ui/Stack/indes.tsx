import * as React from 'react';

import { ResponsiveUtilityValue } from 'react-bootstrap/esm/createUtilityClasses';
import { GapValue } from 'react-bootstrap/esm/types';

import { classnames } from '../../../utils/classnames';
import { createWithUtilityClassName } from '../../../utils/createWithUtilityClassName';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';
import {
	useThemeMinBreakpoint,
	useThemeBreakpoints,
	useThemePrefix
} from '../../../utils/ThemePrefix';

export type StackDirection = 'horizontal' | 'vertical';
export interface StackProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	direction?: StackDirection;
	gap?: ResponsiveUtilityValue<GapValue>;
}
const Stack: IComponentPrefixRefForwardingComponent<'span', StackProps> =
	React.forwardRef(
		(
			{
				as: Component = 'div',
				componentPrefix,
				className,
				direction,
				gap,
				...props
			},
			ref
		) => {
			componentPrefix = useThemePrefix(
				componentPrefix,
				direction === 'horizontal' ? 'hstack' : 'vstack'
			);
			const breakpoints = useThemeBreakpoints();
			const minBreakpoint = useThemeMinBreakpoint();
			return (
				<Component
					{...props}
					ref={ref}
					className={classnames(
						className,
						componentPrefix,
						...createWithUtilityClassName(
							{
								gap
							},
							breakpoints,
							minBreakpoint
						)
					)}
				/>
			);
		}
	);
export default Stack;
