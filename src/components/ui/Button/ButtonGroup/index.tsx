import React, { forwardRef } from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

export interface ButtonGroupProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	size?: 'sm' | 'lg';
	vertical?: boolean;
}

const defaultProps = {
	vertical: false,
	role: 'group'
};

const ButtonGroup: IComponentPrefixRefForwardingComponent<
	'div',
	ButtonGroupProps
> = forwardRef(
	(
		{
			componentPrefix,
			size,
			vertical,
			className,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'div',
			...rest
		},
		ref
	) => {
		const prefix = useThemePrefix(componentPrefix, 'btn-group');
		let baseClass = prefix;
		if (vertical) baseClass = `${prefix}-vertical`;
		return (
			<Component
				{...rest}
				ref={ref}
				className={classnames(className, baseClass, size && `${prefix}-${size}`)}
			/>
		);
	}
);

ButtonGroup.defaultProps = defaultProps;
export default ButtonGroup;
