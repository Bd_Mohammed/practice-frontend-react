import React, { forwardRef } from 'react';

import { classnames } from '../../../../utils/classnames';
import { IPrefixWithAsProps } from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

const defaultProps = {
	role: 'toolbar'
};

interface ButtonToolbarProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {}

const ButtonToolbar: React.ForwardRefExoticComponent<
	ButtonToolbarProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ componentPrefix, className, ...props }, ref) => {
	const prefix = useThemePrefix(componentPrefix, 'btn-toolbar');
	return (
		<div
			{...props}
			ref={ref}
			className={classnames(className, prefix)}
		/>
	);
});
ButtonToolbar.defaultProps = defaultProps;
export default ButtonToolbar;
