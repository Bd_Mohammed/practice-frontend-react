import { ButtonProps as BaseButtonProps } from '@restart/ui/Button';
import { useButtonProps } from '@restart/ui/Button';
import React, { forwardRef } from 'react';

import { classnames } from '../../../utils/classnames';
import {
	CloseButtonVariant,
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

export interface ButtonProps
	extends BaseButtonProps,
		Omit<IPrefixWithAsProps, 'as'> {
	active?: boolean;
	variant?: CloseButtonVariant;
	size?: 'sm' | 'lg';
}

const defaultProps = {
	variant: 'primary',
	active: false,
	disabled: false
};

const Button: IComponentPrefixRefForwardingComponent<'button', ButtonProps> =
	forwardRef(
		(
			{ as, componentPrefix, variant, size, active, className, ...props },
			ref
		) => {
			const prefix = useThemePrefix(componentPrefix, 'btn');
			const [buttonProps, { tagName }] = useButtonProps({
				tagName: as,
				...props
			});
			const Component = tagName;
			return (
				<Component
					{...buttonProps}
					{...props}
					ref={ref}
					className={classnames(
						className,
						prefix,
						active && 'active',
						variant && `${prefix}-${variant}`,
						size && `${prefix}-${size}`,
						props.href && props.disabled && 'disabled'
					)}
				/>
			);
		}
	);

Button.defaultProps = defaultProps;

export default Button;
