import _propTypes from 'prop-types';
import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import {
	FeedbackType,
	IAsProp,
	IPrefixRefForwardingComponent
} from '../../../utils/helper';

interface FeedbackProps extends IAsProp, React.HTMLAttributes<HTMLElement> {
	bsPrefix?: never;
	type?: FeedbackType;
	tooltip?: boolean;
}

const Feedback: IPrefixRefForwardingComponent<'div', FeedbackProps> =
	React.forwardRef(
		// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
		(
			{
				as: Component = 'div',
				className,
				type = 'valid',
				tooltip = false,
				...props
			},
			ref
		) => (
			<Component
				{...props}
				ref={ref}
				className={classnames(
					className,
					`${type}-${tooltip ? 'tooltip' : 'feedback'}`
				)}
			/>
		)
	);

export default Feedback;
