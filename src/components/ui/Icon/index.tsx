import React from 'react';
import styled from 'styled-components';

interface IStyledIcon {
	className: string;
	children?: JSX.Element;
	dataTestId: string;
	disabled?: boolean;
	onClick?: React.MouseEventHandler<HTMLButtonElement>;
	style?: React.CSSProperties;
}

const IconComponent: React.FC<IStyledIcon> = ({
	className,
	dataTestId,
	onClick,
	style,
	children
}) => (
	<i
		role="presentation"
		aria-hidden="true"
		className={className}
		data-testid={dataTestId}
		onClick={onClick}
		style={style}
		tabIndex={-1}
	>
		{children}
	</i>
);

const Icon = styled(IconComponent)`
	position: absolute;
	font-size: 20px;
	padding-left: 16px;
	padding-top: 9px;
	transition: all 0.3s ease-in-out;
	z-index: 1;
`;

export { Icon, IconComponent };
