import React from 'react';
import styled from 'styled-components';

const CheckboxComponent: React.FC<JSX.IntrinsicElements['input']> = ({
	className,
	children,
	...props
}) => {
	// const inputRef = useRef<HTMLInputElement | null>(null);

	// const handleClick = () => {
	//     if (inputRef && inputRef.current) inputRef.current.focus();
	// };

	return (
		<label className={className}>
			<input
				type="checkbox"
				{...props}
			/>
			{children}
		</label>
	);
};

export const Checkbox = styled(CheckboxComponent)`
	margin-right: 1em;
`;
