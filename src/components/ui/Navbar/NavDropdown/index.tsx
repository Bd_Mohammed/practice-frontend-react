import { DropdownMenuVariant } from '../../../../utils/helper';
import { DropdownProps } from '../../Dropdown';

export interface NavDropdownProps extends Omit<DropdownProps, 'title'> {
	title: React.ReactNode;
	disabled?: boolean;
	active?: boolean;
	menuRole?: string;
	renderMenuOnMount?: boolean;
	rootCloseEvent?: 'click' | 'mousedown';
	menuVariant?: DropdownMenuVariant;
}
