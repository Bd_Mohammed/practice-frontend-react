import * as React from 'react';

type NavbarContextType = {
	onToggle: () => void;
	componentPrefix?: string;
	expanded: boolean;
	expand?: boolean | string | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';
};

const NavbarContext: React.Context<NavbarContextType> =
	React.createContext<NavbarContextType>({
		onToggle: () => {},
		componentPrefix: '',
		expanded: false,
		expand: 'md'
	});

export default NavbarContext;
