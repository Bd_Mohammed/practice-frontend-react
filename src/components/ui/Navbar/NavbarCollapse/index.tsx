import React, { forwardRef, useContext } from 'react';

import { CollapseProps } from 'react-bootstrap';

import { IPrefixProps } from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import { Collapse } from '../../Collapse';
import NavbarContext from '../NavbarContext';

interface INavbarCollapseProps
	extends Omit<CollapseProps, 'children'>,
		React.HTMLAttributes<HTMLDivElement>,
		IPrefixProps {}
export const NavbarCollapse: React.ForwardRefExoticComponent<
	INavbarCollapseProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(
	({ children, componentPrefix, ...props }: INavbarCollapseProps, ref) => {
		componentPrefix = useThemePrefix(componentPrefix, 'navbar-collapse');
		const { expanded } = useContext(NavbarContext);
		return (
			<Collapse
				in={!!expanded}
				{...props}
				children={
					<div
						ref={ref}
						className={componentPrefix}
						children={children}
					></div>
				}
			/>
		);
	}
);
