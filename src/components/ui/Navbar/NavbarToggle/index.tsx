import useEventCallback from '@restart/hooks/useEventCallback';
import React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
import NavbarContext from '../NavbarContext';

export interface NavbarToggleProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	label?: string;
}
const defaultProps = {
	label: 'Toggle navigation'
};
const NavbarToggle: IComponentPrefixRefForwardingComponent<
	'button',
	NavbarToggleProps
> = React.forwardRef(
	(
		{
			componentPrefix,
			className,
			children,
			label,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'button',
			onClick,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'navbar-toggler');
		const { onToggle, expanded } = React.useContext(NavbarContext);
		const handleClick = useEventCallback((e: any) => {
			if (onClick) onClick(e);
			if (onToggle) onToggle();
		});
		if (Component === 'button') {
			(props as any).type = 'button';
		}
		return (
			<Component
				{...props}
				ref={ref}
				onClick={handleClick}
				aria-label={label}
				className={classnames(className, componentPrefix, !expanded && 'collapsed')}
				children={children || <span className={`${componentPrefix}-icon`} />}
			/>
		);
	}
);

NavbarToggle.defaultProps = defaultProps;
export { NavbarToggle };
