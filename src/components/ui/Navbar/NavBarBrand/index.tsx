import React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

interface INavbarBrandProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	href?: string;
}

export const NavBarBrand: IPrefixRefForwardingComponent<
	'a',
	INavbarBrandProps
> = React.forwardRef(({ componentPrefix, className, as, ...props }, ref) => {
	componentPrefix = useThemePrefix(componentPrefix, 'navbar-brand');
	const Component = as || (props.href ? 'a' : 'span');
	return React.createElement(Component, {
		...props,
		ref: ref,
		className: classnames(className, componentPrefix)
	});
});
