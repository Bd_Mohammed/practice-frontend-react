import * as React from 'react';

import Offcanvas, { OffcanvasProps } from '../../offcanvas';
import NavbarContext from '../NavbarContext';

export type NavbarOffcanvasProps = Omit<OffcanvasProps, 'show'>;

const NavbarOffcanvas: React.ForwardRefExoticComponent<
	Pick<NavbarOffcanvasProps, keyof OffcanvasProps> &
		React.RefAttributes<HTMLDivElement>
> = React.forwardRef((props, ref) => {
	const context = React.useContext(NavbarContext);
	return (
		<Offcanvas
			ref={ref}
			show={!!(context != null && context.expanded)}
			{...props}
			renderStaticNode={true}
		/>
	);
});
export default NavbarOffcanvas;
