import SelectableContext from '@restart/ui/SelectableContext';
import { SelectCallback } from '@restart/ui/types';
import React, { forwardRef, useCallback } from 'react';
import { useUncontrolled } from 'uncontrollable';

import { classnames } from '../../../utils/classnames';
import { IPrefixWithAsProps } from '../../../utils/helper';

import style from './Nav.module.css';
import { NavBarBrand } from './NavBarBrand';
import { NavbarCollapse } from './NavbarCollapse';
// import { createWithComponentPrefix } from "../../../utils/createWithComponenetPrefix";
import NavbarContext from './NavbarContext';
import NavbarOffcanvas from './NavbarOffcanvas';
import { NavbarToggle } from './NavbarToggle';

export interface INavbarProps
	extends IPrefixWithAsProps,
		Omit<React.HTMLAttributes<HTMLElement>, 'onSelect'> {
	variant: 'light' | 'dark' | string;
	expand?: boolean | string | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';
	bg?: string;
	fixed?: 'top' | 'bottom';
	sticky?: 'top';
	onToggle?: (expanded: boolean) => void;
	onSelect?: SelectCallback;
	collapseOnSelect?: boolean;
	expanded?: boolean;
	className?: string;
	children?: JSX.Element;
}

// const NavbarText = createWithComponentPrefix('navbar-text', {
//     Component: 'span'
// });

const defaultProps = {
	expand: true,
	variant: 'light',
	collapseOnSelect: false
};

const Navbar = forwardRef((props: INavbarProps, ref) => {
	const {
		componentPrefix: initialCmpPrefix,
		expand,
		variant,
		bg,
		fixed,
		sticky,
		className,
		as: Component = 'nav',
		expanded,
		onToggle,
		onSelect,
		collapseOnSelect,
		...controlledProps
	} = useUncontrolled(props, {
		expanded: 'onToggle'
	});

	const componentPrefix = 'navbar';

	const handleCollapse = useCallback(
		(...args: any[]) => {
			if (onSelect) {
				//@ts-expect-error
				onSelect(...args);
			}
			if (collapseOnSelect && expanded) {
				onToggle?.(false);
			}
		},
		[onSelect, collapseOnSelect, expanded, onToggle]
	);

	if (controlledProps.role === undefined && Component !== 'nav') {
		controlledProps.role = 'navigation';
	}

	let expandClass = `${componentPrefix}-expand`;
	if (typeof expand === 'string')
		expandClass = style[`${expandClass}-${expand}`];

	// const navbarContext = useMemo(() => ({
	//     onToggle: () => onToggle?.(!expanded),
	//     componentPrefix,
	//     expanded: !!expanded,
	//     expand,
	// }), [componentPrefix, expanded, expand, onToggle]);

	return (
		<SelectableContext.Provider value={handleCollapse}>
			<Component
				ref={ref}
				{...controlledProps}
				className={classnames(
					className,
					componentPrefix,
					expand && expandClass,
					variant && style[`${componentPrefix}-${variant}`],
					bg && style[`bg-${bg}`],
					sticky && style[`sticky-${sticky}`],
					fixed && style[`fixed-${fixed}`]
				)}
			/>
		</SelectableContext.Provider>
	);
});

Navbar.defaultProps = defaultProps;

export default Object.assign(Navbar, {
	Brand: NavBarBrand,
	Collapse: NavbarCollapse,
	Offcanvas: NavbarOffcanvas,
	Text: NavbarContext,
	Toggle: NavbarToggle
});
