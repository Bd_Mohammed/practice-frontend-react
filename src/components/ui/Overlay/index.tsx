import useEventCallback from '@restart/hooks/useEventCallback';
import useIsomorphicEffect from '@restart/hooks/useIsomorphicEffect';
import useMergedRefs from '@restart/hooks/useMergedRefs';
import {
	OverlayProps as BaseOverlayProps,
	OverlayArrowProps
} from '@restart/ui/Overlay';
import Overlay_ from '@restart/ui/Overlay';
import * as React from 'react';
import {
	Placement,
	PopperRef,
	RootCloseEvent
} from 'react-bootstrap/esm/types';

import { classnames } from '../../../utils/classnames';
import { TransitionType } from '../../../utils/helper';
import useOverlayOffset from '../../../utils/useOverlayOffset';
import { Fade } from '../Fade';

interface OverlayInjectedProps {
	ref: React.RefCallback<HTMLElement>;
	style: React.CSSProperties;
	'aria-labelledby'?: string;
	arrowProps: Partial<OverlayArrowProps>;
	show: boolean;
	placement: Placement | undefined;
	popper: PopperRef;
	hasDoneInitialMeasure?: boolean;
	[prop: string]: any;
}

export type OverlayChildren =
	| React.ReactElement<OverlayInjectedProps>
	| ((injected: OverlayInjectedProps) => React.ReactNode);
export interface OverlayProps
	extends Omit<BaseOverlayProps, 'children' | 'transition' | 'rootCloseEvent'> {
	children: OverlayChildren;
	transition?: TransitionType;
	placement?: Placement;
	rootCloseEvent?: RootCloseEvent;
}

const defaultProps: {
	transition: TransitionType;
	placement: Placement;
	rootClose: boolean;
	show: boolean;
} = {
	transition: Fade,
	rootClose: false,
	show: false,
	placement: 'top'
};

// function wrapRefs(props: OverlayInjectedProps, arrowProps: Partial<OverlayArrowProps>) {
//     const {
//         ref
//     } = props;
//     const {
//         ref: aRef
//     } = arrowProps;
//     props.ref = ref.__wrapped || (ref.__wrapped = r => ref((0, _safeFindDOMNode.default)(r)));
//     arrowProps.ref = aRef.__wrapped || (aRef.__wrapped = r => aRef((0, _safeFindDOMNode.default)(r)));
// }

const Overlay: React.ForwardRefExoticComponent<
	OverlayProps & React.RefAttributes<HTMLElement>
> = React.forwardRef<any, OverlayProps & React.RefAttributes<HTMLElement>>(
	(
		{ children: overlay, transition, popperConfig = {}, ...outerProps },
		outerRef
	) => {
		const popperRef = React.useRef<any>({});
		const [firstRenderedState, setFirstRenderedState] = React.useState(null);
		const [ref, modifiers] = useOverlayOffset(outerProps.offset);
		const mergedRef = useMergedRefs(outerRef, ref);
		const actualTransition = transition === true ? Fade : transition || undefined;
		const handleFirstUpdate = useEventCallback((state) => {
			setFirstRenderedState(state);
			popperConfig == null
				? void 0
				: popperConfig.onFirstUpdate == null
					? void 0
					: popperConfig.onFirstUpdate(state);
		});
		useIsomorphicEffect(() => {
			if (firstRenderedState) {
				popperRef.current.scheduleUpdate == null
					? void 0
					: popperRef.current.scheduleUpdate();
			}
		}, [firstRenderedState]);
		React.useEffect(() => {
			if (!outerProps.show) {
				setFirstRenderedState(null);
			}
		}, [outerProps.show]);
		return (
			<Overlay_
				{...outerProps}
				ref={mergedRef}
				popperConfig={{
					...popperConfig,
					modifiers: modifiers.concat(popperConfig.modifiers || []),
					onFirstUpdate: handleFirstUpdate
				}}
				transition={actualTransition}
			>
				{(overlayProps, { arrowProps, popper: popperObj, show }) => {
					// wrapRefs(overlayProps, arrowProps);
					// Need to get placement from popper object, handling case when overlay is flipped using 'flip' prop
					const updatedPlacement = popperObj?.placement;

					const popper = Object.assign(popperRef.current, {
						state: popperObj?.state,
						scheduleUpdate: popperObj?.update,
						placement: updatedPlacement,
						outOfBoundaries:
							popperObj?.state?.modifiersData.hide?.isReferenceHidden || false,
						strategy: popperConfig.strategy
					});

					const hasDoneInitialMeasure = !!firstRenderedState;

					if (typeof overlay === 'function') {
						return overlay({
							...overlayProps,
							placement: updatedPlacement,
							show,
							...(transition && show && { className: 'show' }),
							popper,
							arrowProps,
							hasDoneInitialMeasure
						});
					}

					return React.cloneElement(overlay, {
						...overlayProps,
						placement: updatedPlacement,
						arrowProps,
						popper,
						hasDoneInitialMeasure,
						className: classnames(
							overlay.props.className,
							!transition && show && 'show'
						),
						style: {
							...overlay.props.style,
							...overlayProps.style
						}
					});
				}}
			</Overlay_>
		);
	}
);
Overlay.defaultProps = defaultProps;
export default Overlay;
