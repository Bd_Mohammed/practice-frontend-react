import { createWithComponentPrefix } from '../../../../utils/createWithComponenetPrefix';
import { IComponentPrefixRefForwardingComponent } from '../../../../utils/helper';

const cardGroup: IComponentPrefixRefForwardingComponent<'div', unknown> =
	createWithComponentPrefix('card-group');
export default cardGroup;
