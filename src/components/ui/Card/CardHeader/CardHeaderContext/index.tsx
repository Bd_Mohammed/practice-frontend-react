import * as React from 'react';

interface CardHeaderContextValue {
	cardHeaderBsPrefix: string;
}

const CardHeaderContext: React.Context<CardHeaderContextValue | null> =
	React.createContext<CardHeaderContextValue | null>(null);
export default CardHeaderContext;
