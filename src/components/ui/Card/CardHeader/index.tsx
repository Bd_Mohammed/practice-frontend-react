import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

import CardHeaderContext from './CardHeaderContext';

interface CardHeaderProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {}

const CardHeader: IPrefixRefForwardingComponent<'div', CardHeaderProps> =
	/*#__PURE__*/ React.forwardRef(
		(
			{
				componentPrefix,
				className,
				// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
				as: Component = 'div',
				...props
			},
			ref
		) => {
			const prefix = useThemePrefix(componentPrefix, 'card-header');
			const contextValue = React.useMemo(
				() => ({
					cardHeaderBsPrefix: prefix
				}),
				[prefix]
			);
			return (
				<CardHeaderContext.Provider value={contextValue}>
					<Component
						ref={ref}
						{...props}
						className={classnames(className, prefix)}
					/>
				</CardHeaderContext.Provider>
			);
		}
	);
export default CardHeader;
