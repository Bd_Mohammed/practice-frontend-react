import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import { createWithComponentPrefix } from '../../../utils/createWithComponenetPrefix';
import { divWithClassName } from '../../../utils/divWithClassName';
import {
	Color,
	IPrefixRefForwardingComponent,
	IPrefixWithAsProps,
	Variant
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

interface CardProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	bg?: Variant;
	text?: Color;
	border?: Variant;
	body?: boolean;
}

const DivStyledAsH5 = divWithClassName('h5');
const DivStyledAsH6 = divWithClassName('h6');
const CardBody = createWithComponentPrefix('card-body');
const CardTitle = createWithComponentPrefix('card-title', {
	Component: DivStyledAsH5
});
const CardSubtitle = createWithComponentPrefix('card-subtitle', {
	Component: DivStyledAsH6
});
const CardLink = createWithComponentPrefix('card-link', {
	Component: 'a'
});
const CardText = createWithComponentPrefix('card-text', {
	Component: 'p'
});
const CardFooter = createWithComponentPrefix('card-footer');
const CardImgOverlay = createWithComponentPrefix('card-img-overlay');
const defaultProps = {
	body: false
};
const Card: IPrefixRefForwardingComponent<'div', CardProps> = React.forwardRef(
	(
		{
			componentPrefix,
			className,
			bg,
			text,
			border,
			body,
			children,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'div',
			...props
		},
		ref
	) => {
		const prefix = useThemePrefix(componentPrefix, 'card');
		return (
			<Component
				ref={ref}
				{...props}
				className={classnames(
					className,
					prefix,
					bg && `bg-${bg}`,
					text && `text-${text}`,
					border && `border-${border}`
				)}
			>
				{body ? <CardBody>{children}</CardBody> : null}
			</Component>
		);
	}
);
Card.defaultProps = defaultProps;

const _default: IPrefixRefForwardingComponent<'div', CardProps> & {
	// Img: IPrefixRefForwardingComponent<"img", import("./CardImg").CardImgProps>;
	Title: IPrefixRefForwardingComponent<
		React.ForwardRefExoticComponent<
			Pick<
				React.DetailedHTMLProps<
					React.HTMLAttributes<HTMLDivElement>,
					HTMLDivElement
				>,
				'key' | keyof React.HTMLAttributes<HTMLDivElement>
			> &
				React.RefAttributes<HTMLDivElement>
		>,
		unknown
	>;
	Subtitle: IPrefixRefForwardingComponent<
		React.ForwardRefExoticComponent<
			Pick<
				React.DetailedHTMLProps<
					React.HTMLAttributes<HTMLDivElement>,
					HTMLDivElement
				>,
				'key' | keyof React.HTMLAttributes<HTMLDivElement>
			> &
				React.RefAttributes<HTMLDivElement>
		>,
		unknown
	>;
	Body: IPrefixRefForwardingComponent<'div', unknown>;
	Link: IPrefixRefForwardingComponent<'a', unknown>;
	Text: IPrefixRefForwardingComponent<'p', unknown>;
	// Header: IPrefixRefForwardingComponent<"div", import("./CardHeader").CardHeaderProps>;
	Footer: IPrefixRefForwardingComponent<'div', unknown>;
	ImgOverlay: IPrefixRefForwardingComponent<'div', unknown>;
} = Object.assign(Card, {
	// Img: IPrefixRefForwardingComponent<"img", import("./CardImg").CardImgProps>;
	Title: CardTitle,
	Subtitle: CardSubtitle,
	Body: CardBody,
	Link: CardLink,
	Text: CardText,
	// Header: IPrefixRefForwardingComponent<"div", import("./CardHeader").CardHeaderProps>;
	Footer: CardFooter,
	ImgOverlay: CardImgOverlay
});

export default _default;
