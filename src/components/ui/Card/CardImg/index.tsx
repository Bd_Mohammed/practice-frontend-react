import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';
interface CardImgProps
	extends IPrefixWithAsProps,
		React.ImgHTMLAttributes<HTMLImageElement> {
	variant?: 'top' | 'bottom' | string;
}

const CardImg: IPrefixRefForwardingComponent<'img', CardImgProps> =
	React.forwardRef(
		// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
		(
			{ componentPrefix, className, variant, as: Component = 'img', ...props },
			ref
		) => {
			const prefix = useThemePrefix(componentPrefix, 'card-img');
			return (
				<Component
					ref={ref}
					className={classnames(
						variant ? `${prefix}-${variant}` : prefix,
						className
					)}
					{...props}
				/>
			);
		}
	);

export default CardImg;
