import * as React from 'react';

import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';
import usePlaceholder, {
	UsePlaceholderProps
} from '../../../utils/usePlaceholder';

import PlaceholderButton from './PlaceholderButton';

interface PlaceholderProps extends UsePlaceholderProps, IPrefixWithAsProps {}

const Placeholder: IComponentPrefixRefForwardingComponent<
	'span',
	PlaceholderProps
> = React.forwardRef(({ as: Component = 'span', ...props }, ref) => {
	const placeholderProps = usePlaceholder(props);
	return (
		<Component
			{...placeholderProps}
			ref={ref}
		/>
	);
});

const _default = Object.assign(Placeholder, {
	Button: PlaceholderButton
});
export default _default;
