import * as React from 'react';

import {
	CloseButtonVariant,
	IComponentPrefixRefForwardingComponent
} from '../../../../utils/helper';
import usePlaceholder, {
	UsePlaceholderProps
} from '../../../../utils/usePlaceholder';
import Button from '../../Button';

interface PlaceholderButtonProps extends UsePlaceholderProps {
	variant?: CloseButtonVariant;
}
const PlaceholderButton: IComponentPrefixRefForwardingComponent<
	'button',
	PlaceholderButtonProps
> = React.forwardRef((props, ref) => {
	const placeholderProps = usePlaceholder(props);
	return (
		<Button
			{...placeholderProps}
			ref={ref}
			disabled={true}
			tabIndex={-1}
		/>
	);
});
export default PlaceholderButton;
