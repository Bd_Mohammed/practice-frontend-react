import * as React from 'react';
import { FormCheckProps } from 'react-bootstrap';

import { FormCheckLabelProps } from 'react-bootstrap/esm/FormCheckLabel';

import { IComponentPrefixRefForwardingComponent } from '../../../utils/helper';
import FormCheck from '../Form/FormCheck';
import { FormCheckInputProps } from '../Form/FormCheck/FormCheckInput';

type SwitchProps = Omit<FormCheckProps, 'type'>;

export const Switch: IComponentPrefixRefForwardingComponent<
	IComponentPrefixRefForwardingComponent<'input', FormCheckProps> & {
		Input: IComponentPrefixRefForwardingComponent<'input', FormCheckInputProps>;
		Label: React.ForwardRefExoticComponent<
			FormCheckLabelProps & React.RefAttributes<HTMLLabelElement>
		>;
	},
	SwitchProps
> = /*#__PURE__*/ React.forwardRef((props, ref) => (
	<FormCheck
		{...props}
		ref={ref}
		type="switch"
	/>
));

const _default = Object.assign(Switch, {
	Input: FormCheck.Input,
	Label: FormCheck.Label
});
export default _default;
