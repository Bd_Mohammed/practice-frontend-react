import * as React from 'react';

import { Color, Variant } from 'react-bootstrap/cjs/types';

import { classnames } from '../../../utils/classnames';
import {
	IPrefixWithAsProps,
	IComponentPrefixRefForwardingComponent
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

interface BadgeProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	bg?: Variant;
	pill?: boolean;
	text?: Color;
}

const defaultProps = {
	bg: 'primary',
	pill: false
};

const Badge: IComponentPrefixRefForwardingComponent<'span', BadgeProps> =
	React.forwardRef(
		(
			{
				componentPrefix,
				bg,
				pill,
				text,
				className,
				as: Component = 'span',
				...props
			},
			ref
		) => {
			const prefix = useThemePrefix(componentPrefix, 'badge');
			return (
				<Component
					ref={ref}
					{...props}
					className={classnames(
						className,
						prefix,
						pill && `rounded-pill`,
						text && `text-${text}`,
						bg && `bg-${bg}`
					)}
				/>
			);
		}
	);
Badge.defaultProps = defaultProps;
export default Badge;
