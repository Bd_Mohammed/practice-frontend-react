import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps,
	Variant
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';

interface SpinnerProps
	extends React.HTMLAttributes<HTMLElement>,
		IPrefixWithAsProps {
	animation?: 'border' | 'grow';
	size?: 'sm';
	variant?: Variant;
}

const Spinner: IComponentPrefixRefForwardingComponent<'div', SpinnerProps> =
	React.forwardRef(
		(
			{
				componentPrefix,
				variant,
				animation = 'border',
				size,
				// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
				as: Component = 'div',
				className,
				...props
			},
			ref
		) => {
			componentPrefix = useThemePrefix(componentPrefix, 'spinner');
			const bsSpinnerPrefix = `${componentPrefix}-${animation}`;
			return (
				<Component
					ref={ref}
					{...props}
					className={classnames(
						className,
						bsSpinnerPrefix,
						size && `${bsSpinnerPrefix}-${size}`,
						variant && `text-${variant}`
					)}
				/>
			);
		}
	);
export default Spinner;
