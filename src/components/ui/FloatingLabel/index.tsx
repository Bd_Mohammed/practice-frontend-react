import * as React from 'react';

import { classnames } from '../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';
import { useThemePrefix } from '../../../utils/ThemePrefix';
import FormGroup, { FormGroupProps } from '../Form/FormGroup';

interface FloatingLabelProps extends FormGroupProps, IPrefixWithAsProps {
	controlId?: string;
	label: React.ReactNode;
}

const FloatingLabel: IComponentPrefixRefForwardingComponent<
	'div',
	FloatingLabelProps
> = React.forwardRef(
	(
		{ componentPrefix, className, children, controlId, label, ...props },
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'form-floating');
		return (
			<FormGroup
				ref={ref}
				className={classnames(className, componentPrefix)}
				controlId={controlId}
				{...props}
			>
				{label && <label htmlFor={controlId}>{label}</label>}
			</FormGroup>
		);
	}
);

export default FloatingLabel;
