import useMergedRefs from '@restart/hooks/useMergedRefs';
import DropdownContext from '@restart/ui/DropdownContext';
import { useDropdownToggle } from '@restart/ui/DropdownToggle';
import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	CommonButtonProps,
	IComponentPrefixRefForwardingComponent
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

import useWrappedRefWithWarning from '../../../../utils/useWrappedRefWithWarning';
import Button, { ButtonProps } from '../../Button';

interface DropdownToggleProps extends Omit<ButtonProps, 'as'> {
	as?: React.ElementType;
	split?: boolean;
	childBsPrefix?: string;
}
type DropdownToggleComponent = IComponentPrefixRefForwardingComponent<
	'button',
	DropdownToggleProps
>;
export type PropsFromToggle = Partial<
	Pick<React.ComponentPropsWithRef<DropdownToggleComponent>, CommonButtonProps>
>;

const DropdownToggle: DropdownToggleComponent = React.forwardRef(
	(
		{
			componentPrefix,
			split,
			className,
			childBsPrefix,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = Button,
			...props
		},
		ref
	) => {
		const prefix = useThemePrefix(componentPrefix, 'dropdown-toggle');
		const dropdownContext = React.useContext(DropdownContext);
		if (childBsPrefix !== undefined) {
			componentPrefix = childBsPrefix;
		}
		const [toggleProps] = useDropdownToggle();
		toggleProps.ref = useMergedRefs(
			toggleProps.ref,
			useWrappedRefWithWarning(ref, 'DropdownToggle')
		);

		// This intentionally forwards size and variant (if set) to the
		// underlying component, to allow it to render size and style variants.
		return (
			<Component
				className={classnames(
					className,
					prefix,
					split && `${prefix}-split`,
					(dropdownContext == null ? void 0 : dropdownContext.show) && 'show'
				)}
				{...toggleProps}
				{...props}
			/>
		);
	}
);
export default DropdownToggle;
