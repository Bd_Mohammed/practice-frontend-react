import useEventCallback from '@restart/hooks/useEventCallback';
import Ddown, {
	DropdownProps as BaseDropdownProps
} from '@restart/ui/Dropdown';

import React from 'react';

import { AlignType } from 'react-bootstrap/esm/types';
import uncontrollable from 'uncontrollable';

import { classnames } from '../../../utils/classnames';
import { createWithComponentPrefix } from '../../../utils/createWithComponenetPrefix';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../utils/helper';

import { useThemeIsRTL, useThemePrefix } from '../../../utils/ThemePrefix';

import InputGroupContext from '../InputGroup/InputGroupContext';

import { DropDirection } from './DropdownContext';
import DropdownItem from './DropdownItem';
import { DropdownMenu, getDropdownMenuPlacement } from './DropdownMenu';
import DropdownToggle from './DropdownToggle';

export interface DropdownProps
	extends BaseDropdownProps,
		IPrefixWithAsProps,
		Omit<React.HTMLAttributes<HTMLElement>, 'onSelect' | 'children'> {
	drop?: DropDirection;
	align?: AlignType;
	focusFirstItemOnShow?: boolean | 'keyboard';
	navbar?: boolean;
	autoClose?: boolean | 'outside' | 'inside';
}

const DropdownHeader = createWithComponentPrefix('dropdown-header', {
	defaultProps: {
		role: 'heading'
	}
});
const DropdownDivider = createWithComponentPrefix('dropdown-divider', {
	Component: 'hr',
	defaultProps: {
		role: 'separator'
	}
});
const DropdownItemText = createWithComponentPrefix('dropdown-item-text', {
	Component: 'span'
});

const defaultProps: any = {
	navbar: false,
	align: 'start',
	autoClose: true,
	drop: 'down'
};

const Dropdown: IComponentPrefixRefForwardingComponent<'div', DropdownProps> =
	React.forwardRef((pProps, ref) => {
		const {
			componentPrefix,
			drop,
			show,
			className,
			align,
			onSelect,
			onToggle,
			focusFirstItemOnShow,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'div',
			navbar: _4,
			autoClose,
			...props
		} = uncontrollable.useUncontrolled(pProps, {
			show: 'onToggle'
		});
		const isInputGroup = React.useContext(InputGroupContext);
		const prefix = useThemePrefix(componentPrefix, 'dropdown');
		const isRTL = useThemeIsRTL();
		const isClosingPermitted = (source: string) => {
			// autoClose=false only permits close on button click
			if (autoClose === false) return source === 'click';

			// autoClose=inside doesn't permit close on rootClose
			if (autoClose === 'inside') return source !== 'rootClose';

			// autoClose=outside doesn't permit close on select
			if (autoClose === 'outside') return source !== 'select';
			return true;
		};

		const handleToggle = useEventCallback((nextShow, meta) => {
			if (
				meta.originalEvent.currentTarget === document &&
				(meta.source !== 'keydown' || meta.originalEvent.key === 'Escape')
			)
				meta.source = 'rootClose';
			if (isClosingPermitted(meta.source))
				onToggle == null ? void 0 : onToggle(nextShow, meta);
		});

		const alignEnd = align === 'end';
		const placement = getDropdownMenuPlacement(alignEnd, drop, isRTL);
		// const contextValue = React.useMemo(() => ({
		//     align,
		//     drop,
		//     isRTL
		// }), [align, drop, isRTL]);
		const directionClasses: any = {
			down: prefix,
			'down-centered': `${prefix}-center`,
			up: 'dropup',
			'up-centered': 'dropup-center dropup',
			end: 'dropend',
			start: 'dropstart'
		};
		return (
			<Ddown
				placement={placement}
				show={show}
				onSelect={onSelect}
				onToggle={handleToggle}
				focusFirstItemOnShow={focusFirstItemOnShow}
				itemSelector={`.${prefix}-item:not(.disabled):not(:disabled)`}
				children={
					isInputGroup ? (
						props.children
					) : (
						<Component
							{...props}
							ref={ref}
							className={classnames(
								className,
								show && 'show',
								directionClasses[drop as any]
							)}
						/>
					)
				}
			/>
		);
	});

Dropdown.defaultProps = defaultProps;

const _default = Object.assign(Dropdown, {
	Toggle: DropdownToggle,
	Menu: DropdownMenu,
	Item: DropdownItem,
	ItemText: DropdownItemText,
	Divider: DropdownDivider,
	Header: DropdownHeader
});

export default _default;
