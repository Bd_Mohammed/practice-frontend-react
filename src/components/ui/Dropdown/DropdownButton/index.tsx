import * as React from 'react';

import Dropdown, { DropdownProps } from '..';
import {
	DropdownMenuVariant,
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { DropdownMenu } from '../DropdownMenu';
import DropdownToggle, { PropsFromToggle } from '../DropdownToggle';

export interface DropdownButtonProps
	extends Omit<DropdownProps, 'title'>,
		PropsFromToggle,
		IPrefixWithAsProps {
	title: React.ReactNode;
	menuRole?: string;
	renderMenuOnMount?: boolean;
	rootCloseEvent?: 'click' | 'mousedown';
	menuVariant?: DropdownMenuVariant;
	flip?: boolean;
}

/**
 * A convenience component for simple or general use dropdowns. Renders a `Button` toggle and all `children`
 * are passed directly to the default `Dropdown.Menu`. This component accepts all of
 * [`Dropdown`'s props](#dropdown-props).
 *
 * _All unknown props are passed through to the `Dropdown` component._ Only
 * the Button `variant`, `size` and `bsPrefix` props are passed to the toggle,
 * along with menu-related props are passed to the `Dropdown.Menu`
 */
const DropdownButton: IComponentPrefixRefForwardingComponent<
	'div',
	DropdownButtonProps
> = React.forwardRef(
	(
		{
			title,
			children,
			componentPrefix,
			rootCloseEvent,
			variant,
			size,
			menuRole,
			renderMenuOnMount,
			disabled,
			href,
			id,
			menuVariant,
			flip,
			...props
		},
		ref
	) => (
		<Dropdown
			ref={ref}
			{...props}
		>
			<DropdownToggle
				id={id}
				href={href}
				size={size}
				variant={variant}
				disabled={disabled}
				childBsPrefix={componentPrefix}
			>
				{title}
			</DropdownToggle>
			<DropdownMenu
				role={menuRole}
				renderOnMount={renderMenuOnMount}
				rootCloseEvent={rootCloseEvent}
				variant={menuVariant}
				flip={flip}
			>
				{children}
			</DropdownMenu>
		</Dropdown>
	)
);

export default DropdownButton;
