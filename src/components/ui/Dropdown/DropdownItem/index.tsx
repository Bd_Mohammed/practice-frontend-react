import BaseDropdownItem, {
	DropdownItemProps as BaseDropdownItemProps
} from '@restart/ui/DropdownItem';
import { useDropdownItem } from '@restart/ui/DropdownItem';
import * as React from 'react';

import { classnames } from '../../../../utils/classnames';
import {
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';
import { useThemePrefix } from '../../../../utils/ThemePrefix';

import Anchor from '../../Anchor';

interface DropdownItemProps extends BaseDropdownItemProps, IPrefixWithAsProps {}
const DropdownItem: IComponentPrefixRefForwardingComponent<
	typeof BaseDropdownItem,
	DropdownItemProps
> = React.forwardRef(
	(
		{
			componentPrefix,
			className,
			eventKey,
			disabled = false,
			onClick,
			active,
			as: Component = Anchor,
			...props
		},
		ref
	) => {
		const prefix = useThemePrefix(componentPrefix, 'dropdown-item');
		const [dropdownItemProps, meta] = useDropdownItem({
			key: eventKey,
			href: props.href,
			disabled,
			onClick,
			active
		});
		return (
			<Component
				{...props}
				{...dropdownItemProps}
				ref={ref}
				className={classnames(
					className,
					prefix,
					meta.isActive && 'active',
					disabled && 'disabled'
				)}
			/>
		);
	}
);
export default DropdownItem;
