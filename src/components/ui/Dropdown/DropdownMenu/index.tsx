import useIsomorphicEffect from '@restart/hooks/useIsomorphicEffect';
import useMergedRefs from '@restart/hooks/useMergedRefs';
import { UseDropdownMenuOptions } from '@restart/ui/DropdownMenu';
import { useDropdownMenu } from '@restart/ui/DropdownMenu';
import React from 'react';
import { AlignType } from 'react-bootstrap/esm/types';

import warning from 'warning';

import { classnames } from '../../../../utils/classnames';
import {
	DropdownMenuVariant,
	IComponentPrefixRefForwardingComponent,
	IPrefixWithAsProps
} from '../../../../utils/helper';

import { useThemePrefix } from '../../../../utils/ThemePrefix';

import useWrappedRefWithWarning from '../../../../utils/useWrappedRefWithWarning';

import InputGroupContext from '../../InputGroup/InputGroupContext';
import NavbarContext from '../../Navbar/NavbarContext';
import DropdownContext, { DropDirection } from '../DropdownContext';

export interface DropdownMenuProps
	extends IPrefixWithAsProps,
		React.HTMLAttributes<HTMLElement> {
	show?: boolean;
	renderOnMount?: boolean;
	flip?: boolean;
	align?: AlignType;
	rootCloseEvent?: 'click' | 'mousedown';
	popperConfig?: UseDropdownMenuOptions['popperConfig'];
	variant?: DropdownMenuVariant;
}
const defaultProps = {
	flip: true
};
export const getDropdownMenuPlacement = (
	alignEnd: boolean,
	dropDirection?: DropDirection,
	isRTL?: boolean
):
	| 'bottom'
	| 'top'
	| 'top-start'
	| 'top-end'
	| 'bottom-start'
	| 'bottom-end'
	| 'right-start'
	| 'right-end'
	| 'left-start'
	| 'left-end' => {
	const topStart = isRTL ? 'top-end' : 'top-start';
	const topEnd = isRTL ? 'top-start' : 'top-end';
	const bottomStart = isRTL ? 'bottom-end' : 'bottom-start';
	const bottomEnd = isRTL ? 'bottom-start' : 'bottom-end';
	const leftStart = isRTL ? 'right-start' : 'left-start';
	const leftEnd = isRTL ? 'right-end' : 'left-end';
	const rightStart = isRTL ? 'left-start' : 'right-start';
	const rightEnd = isRTL ? 'left-end' : 'right-end';
	let placement:
		| 'bottom'
		| 'top'
		| 'top-start'
		| 'top-end'
		| 'bottom-start'
		| 'bottom-end'
		| 'right-start'
		| 'right-end'
		| 'left-start'
		| 'left-end' = alignEnd ? bottomEnd : bottomStart;
	if (dropDirection === 'up') placement = alignEnd ? topEnd : topStart;
	else if (dropDirection === 'end') placement = alignEnd ? rightEnd : rightStart;
	else if (dropDirection === 'start') placement = alignEnd ? leftEnd : leftStart;
	else if (dropDirection === 'down-centered') placement = 'bottom';
	else if (dropDirection === 'up-centered') placement = 'top';
	return placement;
};
const DropdownMenu: IComponentPrefixRefForwardingComponent<
	'div',
	DropdownMenuProps
> = React.forwardRef(
	(
		{
			componentPrefix,
			className,
			align,
			rootCloseEvent,
			flip,
			show: showProps,
			renderOnMount,
			// Need to define the default "as" during prop destructuring to be compatible with styled-components github.com/react-bootstrap/react-bootstrap/issues/3595
			as: Component = 'div',
			popperConfig,
			variant,
			...props
		},
		ref
	) => {
		let alignEnd = false;
		const isNavbar = React.useContext(NavbarContext);
		const prefix = useThemePrefix(componentPrefix, 'dropdown-menu');
		const {
			align: contextAlign,
			drop,
			isRTL
		} = React.useContext(DropdownContext);
		align = align || contextAlign;
		const isInputGroup = React.useContext(InputGroupContext);
		const alignClasses = [];
		if (align) {
			if (typeof align === 'object') {
				const keys = Object.keys(align);
				process.env.NODE_ENV !== 'production'
					? warning(
							keys.length === 1,
							'There should only be 1 breakpoint when passing an object to `align`'
						)
					: void 0;
				if (keys.length) {
					const brkPoint: any = keys[0];
					const direction = (align as any)[brkPoint];

					// .dropdown-menu-end is required for responsively aligning
					// left in addition to align left classes.
					alignEnd = direction === 'start';
					alignClasses.push(`${prefix}-${brkPoint}-${direction}`);
				}
			} else if (align === 'end') {
				alignEnd = true;
			}
		}
		const placement = getDropdownMenuPlacement(alignEnd, drop, isRTL);
		const [menuProps, { hasShown, popper, show, toggle }] = useDropdownMenu({
			flip,
			rootCloseEvent,
			show: showProps,
			usePopper: !isNavbar && alignClasses.length === 0,
			offset: [0, 2],
			popperConfig,
			placement
		});
		menuProps.ref = useMergedRefs(
			useWrappedRefWithWarning(ref, 'DropdownMenu'),
			menuProps.ref
		);
		useIsomorphicEffect(() => {
			// Popper's initial position for the menu is incorrect when
			// renderOnMount=true. Need to call update() to correct it.
			if (show) popper == null ? void 0 : popper.update();
		}, [show]);
		if (!hasShown && !renderOnMount && !isInputGroup) return null;

		// For custom components provide additional, non-DOM, props;
		if (typeof Component !== 'string') {
			menuProps.show = show;
			menuProps.close = () => (toggle == null ? void 0 : toggle(false));
			menuProps.align = align;
		}
		let style = props.style;
		if (popper != null && popper.placement) {
			// we don't need the default popper style,
			// menus are display: none when not shown.
			style = {
				...props.style,
				...menuProps.style
			};
			//@ts-ignore
			props['x-placement'] = popper.placement;
		}
		return (
			<Component
				{...props}
				{...menuProps}
				style={style}
				// Bootstrap css requires this data attrib to style responsive menus.
				{...(alignClasses.length || isNavbar
					? {
							'data-bs-popper': 'static'
						}
					: {})}
				className={classnames(
					className,
					prefix,
					show && 'show',
					alignEnd && `${prefix}-end`,
					variant && `${prefix}-${variant}`,
					...alignClasses
				)}
			/>
		);
	}
);

DropdownMenu.defaultProps = defaultProps;
export { DropdownMenu };
