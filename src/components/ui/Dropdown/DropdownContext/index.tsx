import * as React from 'react';
import { AlignType } from 'react-bootstrap/esm/types';

export type DropDirection =
	| 'up'
	| 'up-centered'
	| 'start'
	| 'end'
	| 'down'
	| 'down-centered';

export type IDropdownContext = {
	align?: AlignType;
	drop?: DropDirection;
	isRTL?: boolean;
};
const DropdownContext = React.createContext<IDropdownContext>({
	align: 'start',
	drop: 'start',
	isRTL: false
});

export default DropdownContext;
