import useMergedRefs from '@restart/hooks/useMergedRefs';
import useTimeout from '@restart/hooks/useTimeout';
import contains from 'dom-helpers/contains';
import * as React from 'react';
import uncontrollable from 'uncontrollable';

import warning from 'warning';

import safeFindDOMNode from '../../../utils/safeFindDOMNode';
import Overlay, { OverlayChildren, OverlayProps } from '../Overlay';

type OverlayTriggerType = 'hover' | 'click' | 'focus';
type OverlayDelay =
	| number
	| {
			show: number;
			hide: number;
	  };
type OverlayInjectedProps = {
	onFocus?: (...args: any[]) => any;
};
type OverlayTriggerRenderProps = OverlayInjectedProps & {
	ref: React.Ref<any>;
};
interface OverlayTriggerProps
	extends Omit<OverlayProps, 'children' | 'target'> {
	children:
		| React.ReactElement
		| ((props: OverlayTriggerRenderProps) => React.ReactNode);
	trigger?: OverlayTriggerType | OverlayTriggerType[];
	delay?: OverlayDelay;
	show?: boolean;
	defaultShow?: boolean;
	onToggle?: (nextShow: boolean) => void;
	flip?: boolean;
	overlay: OverlayChildren;
	target?: never;
	onHide?: never;
}
function normalizeDelay(delay: any) {
	return delay && typeof delay === 'object'
		? delay
		: {
				show: delay,
				hide: delay
			};
}

// Simple implementation of mouseEnter and mouseLeave.
// React's built version is broken: https://github.com/facebook/react/issues/4251
// for cases when the trigger is disabled and mouseOut/Over can cause flicker
// moving from one child element to another.
function handleMouseOverOut(
	// eslint-disable-next-line @typescript-eslint/no-shadow
	handler: any,
	args: any,
	relatedNative: any
) {
	const [e] = args;
	const target = e.currentTarget;
	const related = e.relatedTarget || e.nativeEvent[relatedNative];
	if ((!related || related !== target) && !contains(target, related)) {
		handler(...args);
	}
}

const defaultProps = {
	defaultShow: false,
	trigger: ['hover', 'focus']
};

const OverlayTrigger = ({
	trigger,
	overlay,
	children,
	popperConfig = {},
	show: propsShow,
	defaultShow = false,
	onToggle,
	delay: propsDelay,
	placement,
	flip = placement && placement.indexOf('auto') !== -1,
	...props
}: OverlayTriggerProps) => {
	const triggerNodeRef = React.useRef(null);
	const mergedRef = useMergedRefs(triggerNodeRef, (children as any).ref);
	const timeout = useTimeout();
	const hoverStateRef = React.useRef('');
	const [show, setShow] = uncontrollable.useUncontrolledProp(
		propsShow,
		defaultShow,
		onToggle
	);
	const delay = normalizeDelay(propsDelay);
	const {
		onFocus = () => {},
		onBlur = () => {},
		onClick = () => {}
	} = typeof children !== 'function' ? React.Children.only(children).props : {};
	const attachRef = (r: React.Component | Element | null | undefined) => {
		mergedRef(safeFindDOMNode(r) as any);
	};
	const handleShow = React.useCallback(() => {
		timeout.clear();
		hoverStateRef.current = 'show';
		if (!delay.show) {
			setShow(true);
			return;
		}
		timeout.set(() => {
			if (hoverStateRef.current === 'show') setShow(true);
		}, delay.show);
	}, [delay.show, setShow, timeout]);
	const handleHide = React.useCallback(() => {
		timeout.clear();
		hoverStateRef.current = 'hide';
		if (!delay.hide) {
			setShow(false);
			return;
		}
		timeout.set(() => {
			if (hoverStateRef.current === 'hide') setShow(false);
		}, delay.hide);
	}, [delay.hide, setShow, timeout]);
	const handleFocus = React.useCallback(
		(...args: any[]) => {
			handleShow();
			onFocus == null ? void 0 : onFocus(...args);
		},
		[handleShow, onFocus]
	);
	const handleBlur = React.useCallback(
		(...args: any[]) => {
			handleHide();
			onBlur == null ? void 0 : onBlur(...args);
		},
		[handleHide, onBlur]
	);
	const handleClick = React.useCallback(
		(...args: any[]) => {
			setShow(!show);
			onClick == null ? void 0 : onClick(...args);
		},
		[onClick, setShow, show]
	);
	const handleMouseOver = React.useCallback(
		(...args: any[]) => {
			handleMouseOverOut(handleShow, args, 'fromElement');
		},
		[handleShow]
	);
	const handleMouseOut = React.useCallback(
		(...args: any[]) => {
			handleMouseOverOut(handleHide, args, 'toElement');
		},
		[handleHide]
	);
	const triggers: string[] = trigger == null ? [] : [].concat(trigger as any);
	const triggerProps: any = {
		ref: attachRef
	};
	if (triggers.indexOf('click') !== -1) {
		triggerProps.onClick = handleClick;
	}
	if (triggers.indexOf('focus') !== -1) {
		triggerProps.onFocus = handleFocus;
		triggerProps.onBlur = handleBlur;
	}
	if (triggers.indexOf('hover') !== -1) {
		process.env.NODE_ENV !== 'production'
			? warning(
					triggers.length > 1,
					'[react-bootstrap] Specifying only the `"hover"` trigger limits the visibility of the overlay to just mouse users. Consider also including the `"focus"` trigger so that touch and keyboard only users can see the overlay as well.'
				)
			: void 0;
		triggerProps.onMouseOver = handleMouseOver;
		triggerProps.onMouseOut = handleMouseOut;
	}
	return (
		<>
			{typeof children === 'function'
				? children(triggerProps)
				: React.cloneElement(children, triggerProps)}

			<Overlay
				{...props}
				show={show}
				onHide={handleHide}
				flip={flip}
				placement={placement}
				popperConfig={popperConfig}
				target={triggerNodeRef.current}
			>
				{overlay}
			</Overlay>
		</>
	);
};
OverlayTrigger.defaultProps = defaultProps;
export default OverlayTrigger;
