import { OverlayArrowProps } from '@restart/ui/Overlay';
import * as React from 'react';
import { Placement, PopperRef } from 'react-bootstrap/esm/types';

import { classnames } from '../../../utils/classnames';
import getInitialPopperStyles from '../../../utils/getInitialPopperStyles';
import getOverlayDirection from '../../../utils/getOverlayDirection';
import { IPrefixWithAsProps } from '../../../utils/helper';
import { useThemeIsRTL, useThemePrefix } from '../../../utils/ThemePrefix';

interface TooltipProps
	extends React.HTMLAttributes<HTMLDivElement>,
		IPrefixWithAsProps {
	placement?: Placement;
	arrowProps?: Partial<OverlayArrowProps>;
	show?: boolean;
	popper?: PopperRef;
	hasDoneInitialMeasure?: boolean;
}

const defaultProps: { placement: Placement } = {
	placement: 'right'
};
const Tooltip: React.ForwardRefExoticComponent<
	TooltipProps & React.RefAttributes<HTMLDivElement>
> = React.forwardRef(
	(
		{
			componentPrefix,
			placement,
			className,
			style,
			children,
			arrowProps,
			hasDoneInitialMeasure,
			popper,
			show,
			...props
		},
		ref
	) => {
		componentPrefix = useThemePrefix(componentPrefix, 'tooltip');
		const isRTL = useThemeIsRTL();
		const [primaryPlacement] =
			(placement == null ? void 0 : placement.split('-')) || [];
		const bsDirection = getOverlayDirection(primaryPlacement, isRTL);
		let computedStyle = style;
		if (show && !hasDoneInitialMeasure) {
			computedStyle = {
				...style,
				...getInitialPopperStyles(popper == null ? void 0 : popper.strategy)
			};
		}
		return (
			<div
				ref={ref}
				style={computedStyle}
				role="tooltip"
				x-placement={primaryPlacement}
				className={classnames(
					className,
					componentPrefix,
					`bs-tooltip-${bsDirection}`
				)}
				{...props}
			>
				<div
					className="tooltip-arrow"
					{...arrowProps}
				/>
				<div className={`${componentPrefix}-inner`}>{children}</div>
			</div>
		);
	}
);
Tooltip.defaultProps = defaultProps;

export default Tooltip;
