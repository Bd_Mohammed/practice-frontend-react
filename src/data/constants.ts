import {
	EXITED,
	EXITING,
	ENTERING,
	ENTERED
} from 'react-transition-group/Transition';
// import { TransitionComponent } from "../utils/helper";

export const DEFAULT_MIN_BREAKPOINT = 'xs';
export const DEFAULT_BREAKPOINTS = ['xxl', 'xl', 'lg', 'md', 'sm', 'xs'];
export const MARGINS = {
	height: ['marginTop', 'marginBottom'],
	width: ['marginLeft', 'marginRight']
};
export const collapseStyles = {
	[EXITED]: 'collapse',
	[EXITING]: 'collapsing',
	[ENTERING]: 'collapsing',
	[ENTERED]: 'collapse show'
};
