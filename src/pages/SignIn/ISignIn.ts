export interface ISingIn {
	email: string;
	password: string;
}
export interface IUser {
	email?: string;
	name?: string;
	// password?: string;
	role?: string;
	token?: string;
	is_deleted?: boolean;
	// _id?: string;
}
