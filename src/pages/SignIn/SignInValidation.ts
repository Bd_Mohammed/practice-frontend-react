import * as yup from 'yup';

export const signInValidationSchema = yup.object().shape({
	email: yup
		.string()
		.required('Please Enter your email')
		.email('please enter valid email'),
	password: yup
		.string()
		.required('Please Enter your password')
		.min(8, 'Password must be 8 characters long')
		.matches(/[0-9]/, 'Password requires a number')
		.matches(/[a-z]/, 'Password requires a lowercase letter')
		.matches(/[A-Z]/, 'Password requires an uppercase letter')
		.matches(/[^\w]/, 'Password requires a symbol')
});
