// import React, { useState } from 'react';
// import {
// 	MDBBtn,
// 	MDBCheckbox,
// 	MDBCol,
// 	MDBContainer,
// 	MDBRow
// } from 'mdb-react-ui-kit';
// import { Form, Formik, ErrorMessage } from 'formik';
// // import jwt from "jwt-decode";
// // import { authContext } from "../../AuthContext";
// // import { customAxios } from "../../Customaxios/Customasios";
// import { Container } from 'react-bootstrap';
// // import ImageSlider, { Slide } from "react-auto-image-slider";
// import MicrosoftTeamsimage from '../../Images/MicrosoftTeamsimage.png';
// import openeye from '../../Images/eye.png';
// import closeEye from '../../Images/hidden.png';
// import { toast } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
// import { ISingIn } from './ISignIn';
// import { signInValidationSchema } from './SignInValidation';
// import Input from '../../components/ui/Input';
// // import jwtDecode from "jwt-decode";

// const defaultSignInValue: ISingIn = {
// 	email: '',
// 	password: ''
// };

// export const SignIn = () => {
// 	//const { setAuthData } = useContext(authContext);
// 	const [showPasswordAsText, setShowPasswordAsText] = useState<boolean>(true);
// 	//const navigate = useNavigate();
// 	const [errorMessage] = useState<any[]>([]);

// 	const handleSubmit = async (_values: ISingIn) => {
// 		toast.success('You’re In!', {
// 			autoClose: 500,
// 			position: 'top-center'
// 		});
// 		// const response = await axios.post(
// 		//   "http://192.168.1.12:5001/api/auth/login",
// 		//   {
// 		//     email: values.email,
// 		//     password: values.password,
// 		//   }
// 		// ).catch(error=>{
// 		//   const err=error.response.data.message
// 		//   setErrorMessage(err)
// 		// })
// 		// const replacebearer = response?.data?.token.replace("Bearer ", "")
// 		// const user = jwt(replacebearer);
// 		// localStorage.setItem("token", replacebearer);
// 		// setAuthData({ token: replacebearer });
// 		// if (user.role === "2") {
// 		//   navigate("/admindashboard");
// 		// } else {
// 		//   navigate("/homepage");
// 		// }
// 	};
// 	const handleShow = () => {
// 		setShowPasswordAsText((prevValue) => !prevValue);
// 	};

// 	return (
// 		<>
// 			{/* <NavBar /> */}
// 			<Container
// 				fluid
// 				className="p0"
// 			>
// 				<div className="w100 pt100 d-flex">
// 					<div className="w60">
// 						<div className="imgloginslider position-relative">
// 							<div className="bloginloginimage"></div>
// 							<img
// 								loading="lazy"
// 								alt="MicrosoftTeamsimage"
// 								className="MicrosoftTeamsimage"
// 								src={MicrosoftTeamsimage}
// 							/>
// 						</div>
// 					</div>
// 					<div className="w40">
// 						<div className="formlogininput position-relative">
// 							<Formik
// 								initialValues={defaultSignInValue}
// 								validationSchema={signInValidationSchema}
// 								onSubmit={handleSubmit}
// 							>
// 								<Form className="">
// 									<div className="loginContent">
// 										<MDBContainer className="">
// 											<MDBRow className="justify-content-center mainformik">
// 												<MDBCol
// 													col="4"
// 													md="8"
// 												>
// 													<label>Email Id</label>
// 													<Input
// 														type="email"
// 														name="email"
// 														placeholder="Email"
// 														dataTestId="input-email"
// 														iconClassName="fa fa-envelope"
// 													/>
// 													<p className="text-danger">
// 														<ErrorMessage name="email" />
// 													</p>
// 													<div className="passfield">
// 														<label>Password</label>
// 														<Input
// 															// type="password"
// 															type={showPasswordAsText ? 'password' : 'text'}
// 															name="password"
// 															placeholder="Password"
// 															dataTestId="input-password"
// 															iconClassName="fa fa-key"
// 														/>
// 														<img
// 															loading="lazy"
// 															alt="eye"
// 															src={showPasswordAsText ? closeEye : openeye}
// 															onClick={handleShow}
// 															className=""
// 														/>
// 														<p className="text-danger">
// 															<ErrorMessage name="password" />
// 														</p>
// 													</div>
// 													<div className="errorMessage">{errorMessage}</div>

// 													<div className="d-flex justify-content-between mx-4 mb-4">
// 														<MDBCheckbox
// 															name="flexCheck"
// 															value=""
// 															id="flexCheckDefault"
// 															// label="Remember me"
// 														/>

// 														<a href="!#">Forgot password?</a>
// 													</div>

// 													<MDBBtn
// 														className="mb-4 w-100 reg"
// 														size="lg"
// 													>
// 														Sign in
// 													</MDBBtn>
// 												</MDBCol>
// 											</MDBRow>
// 										</MDBContainer>
// 									</div>
// 								</Form>
// 							</Formik>
// 						</div>
// 					</div>
// 				</div>
// 			</Container>
// 		</>
// 	);
// };
