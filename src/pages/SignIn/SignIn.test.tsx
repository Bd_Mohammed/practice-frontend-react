// import React from "react";
// import { render, screen } from "@testing-library/react";
// import { SignIn } from "./SignIn";

// describe("SignIn Component", () => {
//   it("renders correctly", () => {
//     render(<SignIn />);

//     const MicrosoftTeamsimage = screen.getByRole("img", {
//       name: /microsoftteamsimage/i,
//     });
//     expect(MicrosoftTeamsimage).toBeInTheDocument();

//     //email element ---------------------------------------------
//     const EmailLabelElement = screen.getByText(/email id/i);
//     expect(EmailLabelElement).toBeInTheDocument();

//     const EmailInputElement = screen.getByPlaceholderText(/email/i);
//     expect(EmailInputElement).toBeInTheDocument();

//     //password element ---------------------------------------------
//     const passwordLabelElement = screen.getByText('Password');
//     expect(passwordLabelElement).toBeInTheDocument();

//     const passwordInputElement = screen.getByPlaceholderText(/password/i);
//     expect(passwordInputElement).toBeInTheDocument();

//     //checkbox element ---------------------------------------------
//     const checkboxElement = screen.getByRole("checkbox");
//     expect(checkboxElement).toBeInTheDocument();

//     //ForgotPassword element ---------------------------------------------
//     const ForgotPassword = screen.getByRole("link", {
//       name: /forgot password\?/i,
//     });
//     expect(ForgotPassword).toBeInTheDocument();

//     //siginbutton element ---------------------------------------------
//     const SignBtnPassword = screen.getByRole('button', {  name: /sign in/i})
//     expect(SignBtnPassword).toBeInTheDocument();
//   });
// });
