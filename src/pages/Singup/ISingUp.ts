export interface ISingUp {
	name: string;
	email: string;
	password: string;
	confirmPassword?: string;
	role?: number;
}

export interface IUser {
	email?: string;
	name?: string;
	password?: string;
	role?: string;
	token?: string;
	is_deleted?: boolean;
	_id?: string;
}
