// import React from 'react';
// import { useState, useContext } from 'react';
// import {
// 	MDBBtn,
// 	MDBCheckbox,
// 	MDBCol,
// 	MDBContainer,
// 	MDBRow
// } from 'mdb-react-ui-kit';
// import 'mdb-react-ui-kit/dist/css/mdb.min.css';
// import { Form, Formik, ErrorMessage } from 'formik';
// import MicrosoftTeamsimage from '../../Images/MicrosoftTeamsimage.png';
// // import wave from "../../Images/wave.png";
// import openEye from '../../Images/eye.png';
// import closeEye from '../../Images/hidden.png';
// import { useNavigate } from 'react-router-dom';
// import { ISingUp } from './ISingUp';
// import { signUpValidationSchema } from './SingUpValidation';
// import { toast } from 'react-toastify';
// import { BlogApi } from '../../lib/axios/Blog-api';
// import { Container } from 'react-bootstrap';
// import Input from '../../components/ui/Input';
// import { AppContext } from '../../App';

// const singUpDefaultValue: ISingUp = {
// 	name: '',
// 	email: '',
// 	password: '',
// 	confirmPassword: ''
// };

// const SignUp = () => {
// 	const res = useContext(AppContext);
// 	console.log('res', res);
// 	const [showPasswordAsText, setShowPasswordAsText] = useState<boolean>(false);
// 	const [showConfirmPasswordAsText, setShowConfirmPasswordAsText] =
// 		useState<boolean>(false);
// 	const navigate = useNavigate();

// 	const handleSubmit = async (values: ISingUp) => {
// 		console.log(`values `, values);

// 		try {
// 			const response = await BlogApi.signUp.createUser({
// 				email: values.email,
// 				name: values.name,
// 				password: values.password,
// 				role: 1
// 			});
// 			console.log(response);
// 			if (response.statusText === 'OK' && response.status === 200) {
// 				BlogApi.signUp.setCookies('token', response.data.DATA.token!, 24);
// 				navigate('/homePage');
// 			}
// 		} catch (error) {
// 			console.warn(error as any);
// 			toast((error as any).response.data.MESSAGE);
// 		}
// 	};

// 	const handleShow = (): void => {
// 		setShowPasswordAsText((prevValue) => !prevValue);
// 	};
// 	const handleHide = (): void => {
// 		setShowConfirmPasswordAsText((prevValue) => !prevValue);
// 	};

// 	return (
// 		<>
// 			{/* <NavBar /> */}
// 			<Container
// 				fluid
// 				className="p0"
// 			>
// 				<div className="w100 pt100 d-flex">
// 					<div className="w60">
// 						<div className="imgloginslider position-relative">
// 							<div className="bloginloginimage"></div>
// 							<img
// 								className="MicrosoftTeamsimage"
// 								alt="MicrosoftTeamsimage"
// 								src={MicrosoftTeamsimage}
// 								loading="lazy"
// 							/>
// 						</div>
// 					</div>
// 					<div className="w40">
// 						<div className="formlogininput  position-relative">
// 							<Formik
// 								initialValues={singUpDefaultValue}
// 								validationSchema={signUpValidationSchema}
// 								onSubmit={handleSubmit}
// 							>
// 								<Form>
// 									<div className="loginContent">
// 										<MDBContainer className="">
// 											<MDBRow className="justify-content-center mainformik">
// 												<MDBCol
// 													col="4"
// 													md="8"
// 												>
// 													<label>Name :</label>
// 													<Input
// 														type="text"
// 														name="name"
// 														placeholder="Name"
// 														dataTestId="input-name"
// 														iconClassName="fa fa-search"
// 													/>
// 													<p className="text-danger">
// 														<ErrorMessage name="name" />
// 													</p>
// 													<label>Email :</label>
// 													<Input
// 														type="email"
// 														name="email"
// 														placeholder="Email"
// 														dataTestId="input-email"
// 														iconClassName="fa fa-envelope"
// 													/>
// 													<p className="text-danger">
// 														<ErrorMessage name="email" />
// 													</p>
// 													<div className="passfield">
// 														<label>Password :</label>
// 														<Input
// 															type={showPasswordAsText ? 'text' : 'password'}
// 															name="password"
// 															placeholder="Password"
// 															dataTestId="input-password"
// 															iconClassName="fa fa-key"
// 														/>
// 														<img
// 															loading="lazy"
// 															src={showPasswordAsText ? openEye : closeEye}
// 															onClick={handleShow}
// 															alt="eye"
// 															className=""
// 														/>
// 														<p className="text-danger">
// 															<ErrorMessage name="password" />
// 														</p>
// 													</div>
// 													<div className="passfield">
// 														<label>Conform Password :</label>
// 														<Input
// 															type={
// 																showConfirmPasswordAsText ? 'text' : 'password'
// 															}
// 															name="confirmPassword"
// 															placeholder="Confirm Password"
// 															dataTestId="input-confirmPassword"
// 															iconClassName="fa fa-key"
// 														/>
// 														<img
// 															src={
// 																showConfirmPasswordAsText ? openEye : closeEye
// 															}
// 															alt="eye"
// 															loading="lazy"
// 															onClick={handleHide}
// 														/>
// 														<p className="text-danger">
// 															<ErrorMessage name="confirmPassword" />
// 														</p>
// 													</div>
// 													<div className="d-flex justify-content-between mx-4 mb-4">
// 														<MDBCheckbox
// 															name="flexCheck"
// 															value=""
// 															id="flexCheckDefault"
// 															label="Remember me"
// 														/>
// 														<a href="!#">Forgot password?</a>
// 													</div>
// 													<MDBBtn
// 														type="submit"
// 														className="mb-4 w-100 reg"
// 														size="lg"
// 													>
// 														Sign Up
// 													</MDBBtn>
// 												</MDBCol>
// 											</MDBRow>
// 										</MDBContainer>
// 									</div>
// 								</Form>
// 							</Formik>
// 						</div>
// 					</div>
// 				</div>
// 			</Container>
// 		</>
// 	);
// };
// export default SignUp;
