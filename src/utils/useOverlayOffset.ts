import { Offset, Options } from '@restart/ui/usePopper';

import hasClass from 'dom-helpers/hasClass';
import * as React from 'react';

import Popover from '../components/ui/Popover';

import { useThemePrefix } from './ThemePrefix';

export default function useOverlayOffset(
	customOffset?: Offset
): [React.RefObject<HTMLElement>, Options['modifiers']] {
	const overlayRef = React.useRef(null);
	const popoverClass = useThemePrefix(undefined, 'popover');
	const offset = React.useMemo(
		() => ({
			name: 'offset',
			options: {
				offset: () => {
					if (overlayRef.current && hasClass(overlayRef.current, popoverClass)) {
						return customOffset || Popover.POPPER_OFFSET;
					}
					return customOffset || [0, 0];
				}
			}
		}),
		[customOffset, popoverClass]
	);
	return [overlayRef, [offset]];
}
