import { ColProps, useCol } from '../components/ui/Col';

import { classnames } from './classnames';
import { useThemePrefix } from './ThemePrefix';

type PlaceholderAnimation = 'glow' | 'wave';
type PlaceholderSize = 'xs' | 'sm' | 'lg';
type Variant =
	| 'primary'
	| 'secondary'
	| 'success'
	| 'danger'
	| 'warning'
	| 'info'
	| 'dark'
	| 'light'
	| string;
export interface UsePlaceholderProps extends Omit<ColProps, 'as'> {
	animation?: PlaceholderAnimation;
	bg?: Variant;
	size?: PlaceholderSize;
}
export default function usePlaceholder({
	animation,
	bg,
	bsPrefix,
	size,
	...props
}: UsePlaceholderProps): any {
	bsPrefix = useThemePrefix(bsPrefix, 'placeholder');
	const [{ className, ...colProps }] = useCol(props);
	return {
		...colProps,
		className: classnames(
			className,
			animation ? `${bsPrefix}-${animation}` : bsPrefix,
			size && `${bsPrefix}-${size}`,
			bg && `bg-${bg}`
		)
	};
}
