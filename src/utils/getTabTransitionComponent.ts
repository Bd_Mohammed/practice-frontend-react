import NoopTransition from '@restart/ui/NoopTransition';
import { TransitionComponent } from '@restart/ui/types';

import { Fade } from '../components/ui/Fade';

import { TransitionType } from './helper';

export default function getTabTransitionComponent(
	transition?: TransitionType
): TransitionComponent | undefined {
	if (typeof transition === 'boolean') {
		return transition ? Fade : NoopTransition;
	}
	return transition;
}
