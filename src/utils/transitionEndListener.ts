import transitionEnd from 'dom-helpers/transitionEnd';

import { style } from './dom-helper/css';

function parseDuration(node: HTMLElement, property: string) {
	//@ts-ignore
	const str = style(node, property) || '';
	const mult = str.indexOf('ms') === -1 ? 1000 : 1;
	return parseFloat(str) * mult;
}

export function transitionEndListener(
	element: HTMLElement,
	handler: (e: TransitionEvent) => void
): void {
	const duration = parseDuration(element, 'transitionDuration');
	const delay = parseDuration(element, 'transitionDelay');
	const remove = transitionEnd(
		element,
		(e) => {
			if (e.target === element) {
				remove();
				handler(e);
			}
		},
		duration + delay
	);
}
