import _propTypes from 'prop-types';

type ResponsiveUtilityValue<T> =
	| T
	| {
			xs?: T;
			sm?: T;
			md?: T;
			lg?: T;
			xl?: T;
			xxl?: T;
	  };

export function responsivePropType(propType: any): _propTypes.Requireable<any> {
	return _propTypes.oneOfType([
		propType,
		_propTypes.shape({
			xs: propType,
			sm: propType,
			md: propType,
			lg: propType,
			xl: propType,
			xxl: propType
		})
	]);
}

export function createWithUtilityClassName(
	utilityValues: Record<string, ResponsiveUtilityValue<unknown>>,
	breakpoints?: string[],
	minBreakpoint?: string
): string[] {
	const classes: string[] = [];
	Object.entries(utilityValues).forEach(([utilName, utilValue]: any) => {
		if (utilValue != null) {
			if (typeof utilValue === 'object') {
				breakpoints?.forEach((brkPoint: any) => {
					const bpValue = utilValue[brkPoint];
					if (bpValue != null) {
						const infix = brkPoint !== minBreakpoint ? `-${brkPoint}` : '';
						classes.push(`${utilName}${infix}-${bpValue}`);
					}
				});
			} else {
				classes.push(`${utilName}-${utilValue}`);
			}
		}
	});
	return classes;
}
