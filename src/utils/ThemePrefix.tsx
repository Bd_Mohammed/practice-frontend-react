import * as React from 'react';

import { DEFAULT_BREAKPOINTS, DEFAULT_MIN_BREAKPOINT } from '../data/constants';

type ThemeContextValue = {
	prefixes: Record<string, string>;
	breakpoints: string[];
	minBreakpoint?: string;
	dir?: string;
};
interface ThemeProviderProps extends Partial<ThemeContextValue> {
	children: React.ReactNode;
}
const ThemeContext: React.Context<ThemeContextValue> =
	React.createContext<ThemeContextValue>({
		prefixes: {},
		breakpoints: DEFAULT_BREAKPOINTS,
		minBreakpoint: DEFAULT_MIN_BREAKPOINT
	});
const { Consumer, Provider } = ThemeContext;

function ThemeProvider({
	prefixes = {},
	breakpoints = DEFAULT_BREAKPOINTS,
	minBreakpoint = DEFAULT_MIN_BREAKPOINT,
	dir,
	children
}: ThemeProviderProps): JSX.Element {
	const contextValue: any = React.useMemo(
		() => ({
			prefixes: {
				...prefixes
			},
			breakpoints,
			minBreakpoint,
			dir
		}),
		[prefixes, breakpoints, minBreakpoint, dir]
	);
	return <Provider value={contextValue}>{children}</Provider>;
}
export function useThemePrefix(
	prefix: string | undefined,
	defaultPrefix: string
) {
	const { prefixes } = React.useContext(ThemeContext);
	return prefix || prefixes[defaultPrefix] || defaultPrefix;
}
export function useThemeBreakpoints(): string[] {
	const { breakpoints } = React.useContext(ThemeContext);
	return breakpoints;
}
export function useThemeMinBreakpoint(): string | undefined {
	const { minBreakpoint } = React.useContext(ThemeContext);
	return minBreakpoint;
}
export function useThemeIsRTL(): boolean {
	const { dir } = React.useContext(ThemeContext);
	return dir === 'rtl';
}
function createBootstrapComponent(Component: any, opts: any) {
	if (typeof opts === 'string')
		opts = {
			prefix: opts
		};
	const isClassy = Component.prototype && Component.prototype.isReactComponent;
	// If it's a functional component make sure we don't break it with a ref
	const { prefix, forwardRefAs = isClassy ? 'ref' : 'innerRef' } = opts;
	const Wrapped: React.ForwardRefExoticComponent<
		{
			bsPrefix?: string | undefined;
		} & React.RefAttributes<any>
	> = React.forwardRef(({ ...props }, ref) => {
		//@ts-ignore
		props[forwardRefAs] = ref;
		const bsPrefix = useThemePrefix(props.bsPrefix, prefix);
		return (
			<Component
				{...props}
				bsPrefix={bsPrefix}
			/>
		);
	});
	Wrapped.displayName = `Bootstrap(${Component.displayName || Component.name})`;
	return Wrapped;
}
export { createBootstrapComponent, Consumer as ThemeConsumer };
export default ThemeProvider;
