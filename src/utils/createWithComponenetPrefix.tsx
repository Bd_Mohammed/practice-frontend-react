import React from 'react';

import { classnames } from './classnames';
import { IComponentPrefixRefForwardingComponent } from './helper';
import { toCamelize } from './stringHelper';
import { useThemePrefix } from './ThemePrefix';

const toPascalCase = (str: string): string =>
	str[0].toUpperCase() + toCamelize(str).slice(1);

interface IComponentPrefixOptions<As extends React.ElementType = 'div'> {
	displayName?: string;
	Component?: As;
	defaultProps?: Partial<React.ComponentProps<As>>;
}

export const createWithComponentPrefix = <As extends React.ElementType = 'div'>(
	prefix: string,
	{
		displayName = toPascalCase(prefix),
		Component,
		defaultProps
	}: IComponentPrefixOptions<As> = {}
): IComponentPrefixRefForwardingComponent<As> => {
	const Cmp: any = React.forwardRef(
		(
			{ className, componentPrefix, as: Tag = Component || 'div', ...props }: any,
			ref
		) => {
			const resolvedPrefix = useThemePrefix(componentPrefix, prefix);
			return (
				<Tag
					{...props}
					ref={ref}
					className={classnames(className, resolvedPrefix)}
				/>
			);
		}
	);

	Cmp.displayName = displayName;
	Cmp.defaultProps = defaultProps;

	return Cmp;
};
