import { State, UsePopperOptions } from '@restart/ui/usePopper';
import PropTypes from 'prop-types';
import React from 'react';
/**
 * Remove any key from an given object <T = Given obj, T = key from given obj></T>
 */
export type Omit<T, U> = Pick<T, Exclude<keyof T, keyof U>>;

/**
 * Replace any element props from new one <Inner = typeof Element, T = new props>
 * Ex=>
 *
 * // Define a new set of props to replace or extend the Button props
 * type NewButtonProps = {
 *    // New props
 *    onClick: () => void;
 *    // Replace the 'color' prop
 *    color: 'red' | 'blue' | 'green';
 *  };
 *
 * // Use ReplaceProps to create a new type with the modified props
 * type ExtendedButtonProps = ReplaceProps<Button, NewButtonProps>;
 *
 */
export type ReplaceProps<Inner extends React.ElementType, P> = Omit<
	React.ComponentPropsWithRef<Inner>,
	P
> &
	P;

export interface IPrefixProps {
	componentPrefix?: string;
}

export interface IAsProp<As extends React.ElementType = React.ElementType> {
	as?: As;
}

export interface IPrefixWithAsProps<
	As extends React.ElementType = React.ElementType
> extends IPrefixProps,
		IAsProp<As> {}

export interface IComponentPrefixRefForwardingComponent<
	TInitial extends React.ElementType,
	P = unknown
> {
	<As extends React.ElementType = TInitial>(
		props: React.PropsWithChildren<ReplaceProps<As, IPrefixWithAsProps<As> & P>>,
		context?: any
	): React.ReactNode | null;
	propTypes?: WeakValidationMap<P> | undefined;
	contextTypes?: any;
	defaultProps?: Partial<P> | undefined;
	displayName?: string | undefined;
}

// export interface IComponentPrefixComponent<
//   As extends React.ElementType,
//   P = unknown
// > extends React.Component<ReplaceProps<As, IPrefixWithAsProps<As> & P>> {}

// export type IComponentPrefixComponentClass<
//   As extends React.ElementType,
//   P = unknown
// > = React.ComponentClass<ReplaceProps<As, IPrefixWithAsProps<As> & P>>;

export interface TransitionCallbacks {
	/**
	 * Callback fired before the component transitions in
	 */
	onEnter?(node: HTMLElement, isAppearing: boolean): any;
	/**
	 * Callback fired as the component begins to transition in
	 */
	onEntering?(node: HTMLElement, isAppearing: boolean): any;
	/**
	 * Callback fired after the component finishes transitioning in
	 */
	onEntered?(node: HTMLElement, isAppearing: boolean): any;
	/**
	 * Callback fired right before the component transitions out
	 */
	onExit?(node: HTMLElement): any;
	/**
	 * Callback fired as the component begins to transition out
	 */
	onExiting?(node: HTMLElement): any;
	/**
	 * Callback fired after the component finishes transitioning out
	 */
	onExited?(node: HTMLElement): any;
}

export interface TransitionProps extends TransitionCallbacks {
	in?: boolean;
	appear?: boolean;
	children: React.ReactElement;
	mountOnEnter?: boolean;
	unmountOnExit?: boolean;
}

export type TransitionComponent = React.ComponentType<TransitionProps>;

export type TransitionType = boolean | TransitionComponent;

export interface IPrefixRefForwardingComponent<
	TInitial extends React.ElementType,
	P = unknown
> {
	<As extends React.ElementType = TInitial>(
		props: React.PropsWithChildren<ReplaceProps<As, IPrefixWithAsProps<As> & P>>,
		context?: any
	): React.ReactNode | null;
	propTypes?: any;
	contextTypes?: any;
	defaultProps?: Partial<P>;
	displayName?: string;
}

export type Variant =
	| 'primary'
	| 'secondary'
	| 'success'
	| 'danger'
	| 'warning'
	| 'info'
	| 'dark'
	| 'light'
	| string;
export type ButtonVariant =
	| Variant
	| 'link'
	| 'outline-primary'
	| 'outline-secondary'
	| 'outline-success'
	| 'outline-danger'
	| 'outline-warning'
	| 'outline-info'
	| 'outline-dark'
	| 'outline-light';
export type Color =
	| 'primary'
	| 'secondary'
	| 'success'
	| 'danger'
	| 'warning'
	| 'info'
	| 'dark'
	| 'light'
	| 'white'
	| 'muted';
export type Placement = import('@restart/ui/usePopper').Placement;
export type AlignDirection = 'start' | 'end';
export type ResponsiveAlignProp =
	| {
			sm: AlignDirection;
	  }
	| {
			md: AlignDirection;
	  }
	| {
			lg: AlignDirection;
	  }
	| {
			xl: AlignDirection;
	  }
	| {
			xxl: AlignDirection;
	  }
	| Record<string, AlignDirection>;
export type AlignType = AlignDirection | ResponsiveAlignProp;
export type RootCloseEvent = 'click' | 'mousedown';
export type GapValue = 0 | 1 | 2 | 3 | 4 | 5;
export interface PopperRef {
	state: State | undefined;
	outOfBoundaries: boolean;
	placement: Placement | undefined;
	scheduleUpdate?: () => void;
	strategy: UsePopperOptions['strategy'];
}
export type CloseButtonVariant = 'white' | string;
export type CommonButtonProps = 'href' | 'size' | 'variant' | 'disabled';
export type DropdownMenuVariant = 'dark' | string;
export type FeedbackType = 'valid' | 'invalid';
