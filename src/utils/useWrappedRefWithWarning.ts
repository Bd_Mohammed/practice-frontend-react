import useMergedRefs from '@restart/hooks/useMergedRefs';
import invariant from 'invariant';
import * as React from 'react';

export default function useWrappedRefWithWarning(
	ref: any,
	componentName: any
): any {
	// @ts-ignore
	if (!(process.env.NODE_ENV !== 'production')) return ref;

	// eslint-disable-next-line react-hooks/rules-of-hooks
	const warningRef = React.useCallback(
		(refValue: any) => {
			!(refValue == null || !refValue.isReactComponent)
				? process.env.NODE_ENV !== 'production'
					? invariant(
							false,
							`${componentName} injected a ref to a provided \`as\` component that resolved to a component instance instead of a DOM element. ` +
								'Use `React.forwardRef` to provide the injected ref to the class component as a prop in order to pass it directly to a DOM element'
						)
					: //@ts-ignore
						invariant(false)
				: void 0;
		},
		[componentName]
	);
	// eslint-disable-next-line react-hooks/rules-of-hooks
	return useMergedRefs(warningRef, ref);
}
