import Manager, {
	ContainerState,
	ModalManagerOptions
} from '@restart/ui/ModalManager';
import addClass from 'dom-helpers/addClass';
import style from 'dom-helpers/css';
import querySelectorAll from 'dom-helpers/querySelectorAll';
import removeClass from 'dom-helpers/removeClass';

const Selector = {
	FIXED_CONTENT: '.fixed-top, .fixed-bottom, .is-fixed, .sticky-top',
	STICKY_CONTENT: '.sticky-top',
	NAVBAR_TOGGLER: '.navbar-toggler'
};

export class ModalManager extends Manager {
	adjustAndStore(prop: any, element: any, adjust: any) {
		const actual = element.style[prop];
		// TODO: DOMStringMap and CSSStyleDeclaration aren't strictly compatible
		element.dataset[prop] = actual;
		style(element, {
			// @ts-ignore
			[prop]: `${parseFloat(style(element, prop)) + adjust}px`
		});
	}
	restore(prop: any, element: HTMLElement) {
		const value = element.dataset[prop];
		if (value !== undefined) {
			delete element.dataset[prop];
			style(element, {
				[prop]: value
			});
		}
	}
	setContainerStyle(containerState: ContainerState): void {
		super.setContainerStyle(containerState);
		const container = this.getElement();
		addClass(container, 'modal-open');
		if (!containerState.scrollBarWidth) return;
		const paddingProp = this.isRTL ? 'paddingLeft' : 'paddingRight';
		const marginProp = this.isRTL ? 'marginLeft' : 'marginRight';
		querySelectorAll(container, Selector.FIXED_CONTENT).forEach((el) =>
			this.adjustAndStore(paddingProp, el, containerState.scrollBarWidth)
		);
		querySelectorAll(container, Selector.STICKY_CONTENT).forEach((el) =>
			this.adjustAndStore(marginProp, el, -containerState.scrollBarWidth)
		);
		querySelectorAll(container, Selector.NAVBAR_TOGGLER).forEach((el) =>
			this.adjustAndStore(marginProp, el, containerState.scrollBarWidth)
		);
	}
	removeContainerStyle(containerState: ContainerState): void {
		super.removeContainerStyle(containerState);
		const container = this.getElement();
		removeClass(container, 'modal-open');
		const paddingProp = this.isRTL ? 'paddingLeft' : 'paddingRight';
		const marginProp = this.isRTL ? 'marginLeft' : 'marginRight';
		querySelectorAll(container, Selector.FIXED_CONTENT).forEach((el) =>
			this.restore(paddingProp, el)
		);
		querySelectorAll(container, Selector.STICKY_CONTENT).forEach((el) =>
			this.restore(marginProp, el)
		);
		querySelectorAll(container, Selector.NAVBAR_TOGGLER).forEach((el) =>
			this.restore(marginProp, el)
		);
	}
}
let sharedManager: ModalManager;
export function getSharedManager(options?: ModalManagerOptions): ModalManager {
	if (!sharedManager) sharedManager = new ModalManager(options);
	return sharedManager;
}
