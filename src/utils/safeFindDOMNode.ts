import reactDom from 'react-dom';

export default function safeFindDOMNode(
	componentOrElement: React.Component | Element | null | undefined
): Element | Text | null {
	if (componentOrElement && 'setState' in componentOrElement) {
		return reactDom.findDOMNode(componentOrElement);
	}
	return componentOrElement != null ? componentOrElement : null;
}
