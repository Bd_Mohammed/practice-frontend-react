import * as React from 'react';

/**
 * Iterates through children that are typically specified as `props.children`,
 * but only maps over children that are "valid elements".
 *
 * The mapFunction provided index will be normalised to the components mapped,
 * so an invalid component would not increase the index.
 *
 */
export function map<P = any>(
	children: any,
	func: (el: React.ReactElement<P>, index: number) => any
): any {
	let index = 0;
	return React.Children.map(children, (child) =>
		React.isValidElement(child) ? func(child as any, index++) : child
	);
}

/**
 * Iterates through children that are "valid elements".
 *
 * The provided forEachFunc(child, index) will be called for each
 * leaf child with the index reflecting the position relative to "valid components".
 */
export function forEach<P = any>(
	children: any,
	func: (el: React.ReactElement<P>, index: number) => void
): void {
	let index = 0;
	React.Children.forEach(children, (child) => {
		if (/*#__PURE__*/ React.isValidElement(child)) func(child as any, index++);
	});
}

/**
 * Finds whether a component's `children` prop includes a React element of the
 * specified type.
 */
function hasChildOfType<P = any>(
	children: React.ReactNode,
	type: string | React.JSXElementConstructor<P>
): boolean {
	return React.Children.toArray(children).some(
		(child) => /*#__PURE__*/ React.isValidElement(child) && child.type === type
	);
}

const ElementChildren = {
	hasChildOfType,
	forEach,
	map
};
export default ElementChildren;
