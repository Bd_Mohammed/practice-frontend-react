// Regular expression to match hyphens followed by any character
const rHyphen = /-(.)/g;

/**
 * Converts a hyphenated string to camel case. Ex-> 'my-example-string' => 'myExampleString'
 * @param string - The hyphenated string to convert.
 * @returns The camel case version of the input string.
 */
export const toCamelize = (string: string): string => {
	return string.replace(rHyphen, function (_, chr) {
		return chr.toUpperCase();
	});
};
