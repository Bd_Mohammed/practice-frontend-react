import React from 'react';

import { classnames } from './classnames';

export const divWithClassName = (
	className: string
): React.ForwardRefExoticComponent<
	Pick<
		React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
		'key' | keyof React.HTMLAttributes<HTMLDivElement>
	> &
		React.RefAttributes<HTMLDivElement>
> =>
	React.forwardRef((p, ref) => (
		<div
			{...p}
			ref={ref}
			className={classnames(p.className, className)}
		/>
	));
