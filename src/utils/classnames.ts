type Value = string | number | boolean | undefined | null;
type Mapping = Record<string, unknown>;
interface ArgumentArray extends Array<Argument> {}
type Argument = Value | Mapping | ArgumentArray;

/**
 * A simple JavaScript utility for conditionally joining classNames together.
 */
const hasOwn = {}.hasOwnProperty;
const nativeCodeString = '[native code]';
export const classnames = (...args: ArgumentArray): string => {
	const classes: any[] = [];
	const argLen = args.length;
	for (let i = 0; i < argLen; i++) {
		const arg = args[i];
		if (!arg) continue;

		const argType = typeof arg;

		if (argType === 'string' || argType === 'number') {
			classes.push(arg);
		} else if (Array.isArray(arg)) {
			if (arg.length) {
				const inner = classnames(...arg);
				if (inner) {
					classes.push(inner);
				}
			}
		} else if (argType === 'object') {
			if (
				arg.toString !== Object.prototype.toString &&
				!arg.toString.toString().includes(nativeCodeString)
			) {
				classes.push(arg.toString());
				continue;
			}
			const ar = arg as any;
			for (const key in ar) {
				if (hasOwn.call(arg, key) && ar[key]) {
					classes.push(key);
				}
			}
		}
	}
	return classes.join(' ');
};
