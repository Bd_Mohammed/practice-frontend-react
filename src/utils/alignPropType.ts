import PropTypes from 'prop-types';

import { AlignDirection } from './helper';
const alignDirection = PropTypes.oneOf(['start', 'end']);
//@ts-ignore
export const alignPropType: PropTypes.Requireable<
	NonNullable<object | AlignDirection | null | undefined>
> = PropTypes.oneOfType([
	alignDirection,
	PropTypes.shape({
		sm: alignDirection
	}),
	PropTypes.shape({
		md: alignDirection
	}),
	PropTypes.shape({
		lg: alignDirection
	}),
	PropTypes.shape({
		xl: alignDirection
	}),
	PropTypes.shape({
		xxl: alignDirection
	}),
	PropTypes.object
]);
