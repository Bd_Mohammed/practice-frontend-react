import * as CSS from 'csstype';

// type Styles = keyof CSSStyleDeclaration;

export type HyphenProperty = keyof CSS.PropertiesHyphen;
export type CamelProperty = keyof CSS.Properties;

export type Property = HyphenProperty | CamelProperty;

const rUpper = /([A-Z])/g;
const msPattern = /^ms-/;

function hyphenate(string: string) {
	return string.replace(rUpper, '-$1').toLowerCase();
}

function hyphenateStyleName(string: string) {
	return hyphenate(string).replace(msPattern, '-ms-');
}

/**
 * Returns the owner document of a given element.
 *
 * @param node the element
 */
function ownerDocument(node?: Element): Document {
	return (node && node.ownerDocument) || document;
}

/**
 * Returns the owner window of a given element.
 *
 * @param node the element
 */
function ownerWindow(node?: Element): Window {
	const doc = ownerDocument(node);
	return (doc && doc.defaultView) || window;
}
/**
 * Returns one or all computed style properties of an element.
 *
 * @param node the element
 * @param psuedoElement the style property
 */
function getComputedStyle(
	node: HTMLElement,
	psuedoElement?: string
): CSSStyleDeclaration {
	return ownerWindow(node).getComputedStyle(node, psuedoElement);
}

type TransformValue =
	| 'translate'
	| 'translateY'
	| 'translateX'
	| 'translateZ'
	| 'translate3d'
	| 'rotate'
	| 'rotateY'
	| 'rotateX'
	| 'rotateZ'
	| 'rotate3d'
	| 'scale'
	| 'scaleY'
	| 'scaleX'
	| 'scaleZ'
	| 'scale3d'
	| 'matrix'
	| 'matrix3d'
	| 'perspective'
	| 'skew'
	| 'skewY'
	| 'skewX';
const supportedTransforms =
	/^((translate|rotate|scale)(X|Y|Z|3d)?|matrix(3d)?|perspective|skew(X|Y)?)$/i;

function isTransform(value: string): value is TransformValue {
	return !!(value && supportedTransforms.test(value));
}

export function style(
	node: HTMLElement,
	property: Partial<Record<Property, string>>
): void;
export function style<T extends HyphenProperty>(
	node: HTMLElement,
	property: T
): CSS.PropertiesHyphen[T];
export function style<T extends CamelProperty>(
	node: HTMLElement,
	property: T
): CSS.Properties[T];
export function style(node: HTMLElement, property: unknown): void | string {
	let css = '';
	let transforms = '';

	if (typeof property === 'string') {
		return (
			node.style.getPropertyValue(hyphenateStyleName(property)) ||
			getComputedStyle(node).getPropertyValue(hyphenateStyleName(property))
		);
	}

	Object.keys(property as HyphenProperty | CamelProperty).forEach(
		(key: string) => {
			const value = (property as any)[key];

			if (!value && value !== 0) {
				node.style.removeProperty(hyphenateStyleName(key));
			} else if (isTransform(key)) {
				transforms += key + '(' + value + ') ';
			} else {
				css += hyphenateStyleName(key) + ': ' + value + ';';
			}
		}
	);

	if (transforms) {
		css += 'transform: ' + transforms + ';';
	}

	node.style.cssText += ';' + css;
}
