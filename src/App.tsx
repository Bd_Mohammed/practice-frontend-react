// import React, { useReducer, createContext } from "react";
// import "./App.css";
// import SignUp from "./pages/Singup/Singup";
// import { Route, Routes, BrowserRouter } from "react-router-dom";
// import { ToastContainer } from "react-toastify";
// import { SignIn } from "./pages/SignIn/SignIn";
// import { TopNavBar } from "./layouts/Navbar/TopNavbar";
// import HomePage from "./pages/Home/HomePage";
// import { IAction, rootReducer } from "./context/reducers";
// // import { initialState } from "./context/states";

// interface IState  { };

// export const AppContext = createContext<{
//   state: IState,
//   dispatch: React.Dispatch<IAction>;
// }>({ state: {}, dispatch: () => { } });

// export const App = () => {
//   const [state, dispatch] = useReducer(rootReducer, {});

//   return (
//     <>
//       <AppContext.Provider value={{ state, dispatch }}>
//         <ToastContainer />
//         <BrowserRouter>
//           <Routes>
//             {/* <Route element={<ProtechtedRoutes/>}/> */}
//             <Route path='/homePage' element={<HomePage />} />
//             {/* <Route element={<ProtechtedRoutes />}> */}
//             {/* <Route path="/homePage" element={<HomePage />} exact /> */}
//             {/* <Route path="/blogdetails/:id" element={<BlogDetails />} /> */}
//             {/* <Route path="/myblog" element={<MyBlog />} /> */}
//             {/* <Route exact path="/admindashboard" element={<AdminDashboard />} /> */}
//             {/* <Route path="/blogsdetails" element={<AdminDetails />} /> */}
//             {/* <Route path="/AdminuserDetails" element={<AdminuserDetails />} /> */}
//             {/* <Route path="/userProfile" element={<UserProfile />} /> */}
//             {/* </Route> */}
//             <Route element={<TopNavBar />}>
//               <Route path="/" element={<SignUp />} />
//               <Route path="/signin" element={<SignIn />} />
//               <Route path="/signup" element={<SignUp />} />
//             </Route>
//             <Route path="*" element={<h1>Path Not Found</h1>} />
//           </Routes>
//         </BrowserRouter>
//       </AppContext.Provider>
//     </>
//   );
// };

// export default App;
