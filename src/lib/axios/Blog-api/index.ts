import { ApiProtocol, IApiConfig } from '../api';

import { BlogApis } from './api';

const config: IApiConfig = {
	host: 'localhost',
	protocol: ApiProtocol.Http,
	port: 8080
};
export const BlogApi = new BlogApis(config);
