import { Api, IApiConfig } from '../api';
import { SignInResource } from '../api-collections/UserLogin';
import { SignUpResource } from '../api-collections/userSignUp';

export class BlogApis extends Api<IApiConfig> {
	signUp = new SignUpResource(this);
	signIn = new SignInResource(this);

	constructor(public config: IApiConfig) {
		super(config);
		//   this.rejectionProcessors = this.rejectionProcessors.concat(plutoraRejectProcessors);
	}

	// #region Helpers

	/**
	 * Builds an expando object used for  POST /filter api requests.
	 * @param property The property you want to filter/search by.
	 * @param operator The operator you want to use to filter values.
	 * @param joinOperator The operator to join multiple filters/searches by.
	 * @param searchValues An array of values that you are checking against.
	 */
	// static BuildExpandoObject(
	//   property: string,
	//   operator: FilterOperator,
	//   joinOperator: JoinOperator,
	//   searchValues: string[],
	// ): IExpandoObject | null {
	//   if (searchValues.length == 0) {
	//     return null;
	//   }
	//   searchValues = searchValues.concat();
	//   if (searchValues.length == 1) {
	//     return {
	//       Left: property,
	//       Operator: operator,
	//       Right: searchValues[0],
	//     };
	//   }
	//   const firstHalf = searchValues.splice(0, Math.floor(searchValues.length / 2));
	//   const secondHalf = searchValues;
	//   const leftExpando = PlutoraApi.BuildExpandoObject(property, operator, joinOperator, firstHalf);
	//   const rightExpando = PlutoraApi.BuildExpandoObject(
	//     property,
	//     operator,
	//     joinOperator,
	//     secondHalf,
	//   );
	//   if (leftExpando != null && rightExpando != null) {
	//     return {
	//       Left: leftExpando,
	//       Operator: joinOperator,
	//       Right: rightExpando,
	//     };
	//   }
	//   return null;
	// }

	/**
	 * Builds a filter/search query used for GET / api requests.
	 * @param property The property you want to filter/search by.
	 * @param operator The operator you want to use to filter values.
	 * @param joinOperator The operator to join multiple filters/searches by.
	 * @param searchValues An array of values that you are check against.
	 */
	static BuildQuery(
		property: string,
		operator: string,
		joinOperator: string,
		searchValues: string[]
	): string {
		return searchValues
			.map((s) => `\`${property}\` ${operator} \`${s}\``)
			.join(` ${joinOperator} `);
	}

	/**
	 * Get OAuth token information.
	 */
	// async getToken(): Promise<IOAuthToken> {
	//   const config = this.config;
	//   const data = stringify({
	//     client_id: config.clientId, // eslint-disable-line
	//     client_secret: config.clientSecret, // eslint-disable-line
	//     grant_type: 'password', // eslint-disable-line
	//     username: config.username,
	//     password: config.password,
	//   });
	//   const logger = new Logger(true);
	//   const response = await this.call<IOAuthToken>(
	//     {
	//       method: RequestMethod.Post,
	//       path: 'oauth/token',
	//       host: this.config.oauthHost,
	//       body: data,
	//       headers: {
	//         'Content-Type': 'x-www-form-urlencoded; charset=urf-8',
	//         Accept: 'application/json',
	//       },
	//     },
	//     logger,
	//   );
	//   return response.body;
	// }

	// headers(): OutgoingHttpHeaders {
	//   const headers: OutgoingHttpHeaders = {};
	//   if (this.oauthToken) {
	//     headers.Authorization = `${this.oauthToken.token_type} ${this.oauthToken.access_token}`;
	//   }
	//   return headers;
	// }
	// #endregion Helpers
}
