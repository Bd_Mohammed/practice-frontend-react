import { OutgoingHttpHeaders } from 'http';

/**
 * Network Request Options
 */
export enum RequestMethod {
	Get = 'GET',
	Patch = 'PATCH',
	Put = 'PUT',
	Post = 'POST',
	Delete = 'DELETE'
}

export interface IRequest {
	/**
	 * HTTP Methods only REST supported aka 'GET', 'POST', 'PUT', 'DELETE', ...
	 */
	method: RequestMethod;
	/**
	 * The url path
	 * i.e. test.com/users path = '/users'
	 */
	path: string;
	/**
	 * The query/parameters of the url
	 * i.e. test.com/users?name=Nick query/parameters = 'name=Nick'
	 */
	query?: string;
	/**
	 * The request body in whatever content type is specified in the headers.
	 */
	body?: string;
	/**
	 * The host aka the subdomain and domain of the url.
	 * i.e. test.com/users host = test.com
	 */
	host: string;
	/**
	 * The request headers
	 */
	headers?: OutgoingHttpHeaders;
}
