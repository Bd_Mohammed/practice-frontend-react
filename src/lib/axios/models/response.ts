import { IncomingHttpHeaders } from 'http';

/**
 * A basic network response
 */
export interface IResponse<T> {
	/** The response body as an object. */
	readonly data: IResponseData<T>;
	/** The response status code, can be null if the request never made it to the server. */
	readonly status: number;
	/** The response headers */
	readonly headers: IncomingHttpHeaders;
	readonly statusText: string;
}

export interface IResponseData<IData> {
	DATA: IData;
	MESSAGE: string;
	STATUS: string;
}
