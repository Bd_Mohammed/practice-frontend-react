import axios from 'axios';

/**
 * fn used to set cookies by cookie name.
 *
 * @param {string} cname Cookie name.
 * @param {string} cvalue Cookie value.
 * @param {string} exdays Cookie expire time.
 */
export const setCookie = (
	cname: string,
	cvalue: string,
	exdays: number
): void => {
	const d: Date = new Date();
	d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
	const expires: string = 'expires=' + d.toUTCString();
	document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
};

/**
 * fn used to get Cookie by name.
 *
 * @param {string} cname Cookie name.
 * @return {string} return cookie value.
 */
export const getCookie = (cname: string): string => {
	const name = cname + '=';
	const decodedCookie = decodeURIComponent(document.cookie);
	const ca = decodedCookie.split(';');
	for (let i = 0; i < ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
};

/**
 * fn used to get Toekn where cookie name is "token".
 *
 * @return {string} return cookie value.
 */
const getToken = (): string => {
	const token = getCookie('token');
	if (token != '') {
		return token;
	} else {
		// token = prompt("Please enter your name:", "");
		// if (token != "" && token != null) {
		//     setCookie("token", token, 30);
		// }
		return '11111111111111111111111';
	}
};

export const customAxios = axios.create({
	baseURL: 'http://localhost:8080/',
	timeout: 1000,
	headers: {
		Accept: 'application/json',
		Authorization: getToken()
	}
});
