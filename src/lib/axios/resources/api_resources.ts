import { Api } from '../api';
import { IRequest, RequestMethod } from '../models/request';
import { IResponse } from '../models/response';

export class ApiResource {
	public readonly resource!: string;
	constructor(
		protected api: Api<any>,
		protected prefix: string = ''
	) {}

	SubResourceCreator = <SubR extends ApiResource>(creator: {
		new (api: Api<any>, prefix: string): SubR;
	}) => {
		return (id: string): SubR => {
			return new creator(this.api, `${this.prefix}/${this.resource}/${id}`);
		};
	};

	/* eslint-disable @typescript-eslint/no-explicit-any */
	protected async _get(uri: string, query?: string): Promise<IResponse<any>> {
		return await this._restCall({
			method: RequestMethod.Get,
			path: uri,
			query: query,
			host: this.api.config.host
		} as any);
	}

	protected async _post(
		uri: string,
		body?: any,
		query?: string
	): Promise<IResponse<any>> {
		return await this._restCall({
			method: RequestMethod.Post,
			path: uri,
			query: query,
			body: body,
			host: this.api.config.host
		} as any);
	}

	protected async _put(
		uri: string,
		body?: any,
		query?: string
	): Promise<IResponse<any>> {
		return await this._restCall({
			method: RequestMethod.Put,
			path: uri,
			query: query,
			body: JSON.stringify(body),
			host: this.api.config.host
		} as any);
	}

	protected async _patch(
		uri: string,
		body?: any,
		query?: string
	): Promise<IResponse<any>> {
		return await this._restCall({
			method: RequestMethod.Patch,
			path: uri,
			query: query,
			body: JSON.stringify(body),
			host: this.api.config.host
		} as any);
	}

	protected async _delete(uri: string, body?: any): Promise<IResponse<any>> {
		return await this._restCall({
			method: RequestMethod.Delete,
			path: uri,
			body: JSON.stringify(body),
			host: this.api.config.host
		});
	}

	private async _restCall(request: IRequest): Promise<IResponse<any>> {
		// const logger = new Logger(true);
		const res: IResponse<any> = await (this.api as any).call(request);
		// logger.run();
		return res;
	}
	/* eslint-enable @typescript-eslint/no-explicit-any */
}
