// import { stringify } from "querystring";
import { IResponse } from '../models/response';

import { ApiResource } from './api_resources';

export class BaseRestResource extends ApiResource {
	get uri(): string {
		let uri = this.resource;
		if (this.prefix) {
			uri = `${this.prefix}/${this.resource}`;
		}
		return uri;
	}

	responseProcessor<T>(res: IResponse<T>): any {
		return res;
	}
}

export class GetAllResource<Multi> extends BaseRestResource {
	/**
	 * Get All for the specified resource
	 * @param _ Any filters/options the get all will take in.
	 */
	public async all(_?: any): Promise<Multi> {
		return this.responseProcessor(
			await this._get(this.uri, JSON.stringify(_)).then((res) => res)
		);
	}
}
/* eslint-enable @typescript-eslint/no-explicit-any */

export class GetResource<Single> extends BaseRestResource {
	/**
	 * Get a single resource.
	 * @param id The unique identifier for the resource you want to get.
	 */
	public async get(id: string): Promise<Single> {
		return this.responseProcessor(await this._get(`${this.uri}/${id}`));
	}
}

export class CreateResource<Single, Create = Single> extends BaseRestResource {
	/**
	 * Create a single resource.
	 * @param newResource The object/properties of the resource you are creating.
	 */
	public async create(newResource: Create): Promise<Single> {
		return this.responseProcessor(await this._post(this.uri, newResource));
	}
}

export class UpdateResource<Single, Update = Single> extends BaseRestResource {
	/**
	 * Update a single resource.
	 * @param id The unqiue identifier of the resource to update.
	 * @param updatedResource The object/properties to update.
	 */
	public async update(id: string, updatedResource: Update): Promise<Single> {
		return this.responseProcessor(
			await this._put(`${this.uri}/${id}`, updatedResource)
		);
	}
}

export class DeleteResource<Single = void> extends BaseRestResource {
	/**
	 * Delete/Remove a single resource.
	 * @param id The unique identifier of the resource to delete.
	 */
	public async delete(id: string): Promise<Single> {
		return this.responseProcessor(await this._delete(`${this.uri}/${id}`));
	}
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export function applyMixins(derivedCtor: any, baseCtors: any[]): void {
	baseCtors.forEach((baseCtor) => {
		Object.getOwnPropertyNames(baseCtor.prototype).forEach((name) => {
			const descriptor = Object.getOwnPropertyDescriptor(baseCtor.prototype, name);
			if (descriptor) {
				Object.defineProperty(derivedCtor.prototype, name, descriptor);
			}
		});
	});
}
/* eslint-enable @typescript-eslint/no-explicit-any */
