import axios from 'axios';

import { IRequest, RequestMethod } from './models/request';
import { IResponse } from './models/response';

export enum ApiProtocol {
	/** Http protocol aka 'http://' */
	Http = 'http',
	/** Https protocol aka 'https://' */
	Https = 'https'
}

export interface IApiConfig {
	/** Host/Domain aka 'google.com', 'jira.com', 'plutora.com' no https:// or http:// needed */
	host: string;
	/**
	 * The protocol the api uses. aka 'https://', 'http://', or 'wss://'
	 * Defaults to 'https'
	 */
	protocol?: ApiProtocol;
	/**
	 * Give a custom port number the api is host on.
	 * Defaults to 443 if protocol is Https or 80 if Http
	 */
	port?: number;
}

export class Api<T extends IApiConfig> {
	private customAxios;

	public constructor(public config: T) {
		this.customAxios = axios.create({
			baseURL: `${config.protocol}://${config.host}:${config.port}/api`,
			timeout: 1000,
			headers: {
				Accept: 'application/json',
				Authorization: this.getToken() ?? ''
			}
		});
	}

	/**
	 * fn used to set cookies by cookie name.
	 *
	 * @param {string} cname Cookie name.
	 * @param {string} cvalue Cookie value.
	 * @param {string} exdays Cookie expire time.
	 */
	public setCookie = (cname: string, cvalue: string, exdays: number): void => {
		const d: Date = new Date();
		d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
		const expires: string = 'expires=' + d.toUTCString();
		document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
	};

	/**
	 * fn used to set cookies by cookie name.
	 *
	 * @param {string} cname Cookie name.
	 */
	public deleteCookie = (cname: string): void => {
		document.cookie = `${cname}=; Path=/; Expires=${new Date().toUTCString()};`;
	};

	/**
	 * fn used to get Toekn where cookie name is "token".
	 *
	 * @return {string} return cookie value.
	 */
	private getToken = (): string => {
		const token = this.getCookie('token');
		if (token != '') {
			return token;
		} else {
			// token = prompt("Please enter your name:", "");
			// if (token != "" && token != null) {
			//     setCookie("token", token, 30);
			// }
			return '';
		}
	};

	/**
	 * fn used to get Cookie by name.
	 *
	 * @param {string} cname Cookie name.
	 * @return {string} return cookie value.
	 */
	private getCookie = (cname: string): string => {
		const name = cname + '=';
		const decodedCookie = decodeURIComponent(document.cookie);
		const ca = decodedCookie.split(';');
		for (let i = 0; i < ca.length; i++) {
			let c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return '';
	};

	/**
	 * Creates a BasicAuth string.
	 * aka `Basic base64encodedUsername:base64encodedPassword`
	 * @param username Username to use for BasicAuth.
	 * @param password Password to use for BasicAuth.
	 */
	protected basicAuthorization(_username: string, _password: string): string {
		return this.getToken();
	}

	/**
	 * Make a network call /request.
	 * @param requestOptions The network request options to perform.
	 * @param logger The logger that will be used to view debug/log information
	 */
	/* eslint-disable @typescript-eslint/no-explicit-any */
	protected call<T>(requestOptions: IRequest): Promise<IResponse<T>> | void {
		switch (requestOptions.method) {
			case RequestMethod.Post: {
				if (requestOptions?.body)
					return this.customAxios.post(requestOptions.path, requestOptions.body);
			}
		}
	}
}
