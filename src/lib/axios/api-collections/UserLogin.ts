import { ISingIn, IUser } from '../../../pages/SignIn/ISignIn';
import { IResponse } from '../models/response';
import { BaseRestResource } from '../resources/rest_resources';

import { BlogResource } from '.';

export class SignInResource extends BaseRestResource {
	resource = BlogResource.SignIn;
	public async validateUser(newResource: ISingIn): Promise<IResponse<IUser>> {
		this.api.deleteCookie('token');
		return this.responseProcessor(await this._post(this.uri, newResource));
	}
	public setCookies(cname: string, cvalue: string, exdays: number) {
		this.api.setCookie(cname, cvalue, exdays);
	}
}

export interface SignUpResource extends BaseRestResource {}
// export interface SystemResource extends  {}

// applyMixins(SignUpResource, [CreateResource]);
