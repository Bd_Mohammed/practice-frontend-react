import { BlogResource } from '../../../lib/axios/api-collections/index';

import { IResponse } from '../models/response';
import { BaseRestResource } from '../resources/rest_resources';

import { IUser, ISingUp } from './../../../pages/Singup/ISingUp';

export class SignUpResource extends BaseRestResource {
	resource = BlogResource.SignUp;
	public async createUser(newResource: ISingUp): Promise<IResponse<IUser>> {
		this.api.deleteCookie('token');
		return this.responseProcessor(await this._post(this.uri, newResource));
	}
	public setCookies(cname: string, cvalue: string, exdays: number) {
		this.api.setCookie(cname, cvalue, exdays);
	}
}

export interface SignUpResource extends BaseRestResource {}
// export interface SystemResource extends  {}

// applyMixins(SignUpResource, [CreateResource]);
