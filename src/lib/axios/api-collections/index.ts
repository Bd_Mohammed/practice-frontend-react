export enum BlogResource {
	SignUp = '/auth/signup',
	SignIn = '/user/signin'
}
