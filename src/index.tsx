import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.css';
// import App from './App';
import reportWebVitals from './reportWebVitals';

import { ToastContainer } from 'react-toastify';

import { ErrorBoundary } from './components/ui/ErrorBoundary';

const root = ReactDOM.createRoot(
	document.getElementById('root') as HTMLElement
);
root.render(
	<React.StrictMode>
		<ErrorBoundary fallback={'Error'}>
			<ToastContainer />
			Hello World!a
		</ErrorBoundary>
	</React.StrictMode>
);

function sendToAnalytics(metric: any) {
	console.log(metric);
	// const body = JSON.stringify(metric);
	// const url = 'https://example.com/analytics';

	// // Use `navigator.sendBeacon()` if available, falling back to `fetch()`
	// if (navigator.sendBeacon) {
	//   navigator.sendBeacon(url, body);
	// } else {
	//   fetch(url, { body, method: 'POST', keepalive: true });
	// }
}
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(sendToAnalytics);
