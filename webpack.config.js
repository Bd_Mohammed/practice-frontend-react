const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const BundleAnalyzerPlugin =
	require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = [
	{
		entry: './src/index.tsx',
		mode: 'development',
		output: {
			filename: '[name].[fullhash].js',
			path: path.resolve(__dirname, 'dist')
		},
		plugins: [
			new HtmlWebpackPlugin({
				template: './public/index.html',
				filename: './index.html',
				favicon: './public/favicon.ico'
			}),
			// new CopyPlugin({
			// 	patterns: [{ from: './public/index.html' }]
			// }),
			new BundleAnalyzerPlugin({
				analyzerMode: 'static',
				openAnalyzer: false
			}),
			new MiniCssExtractPlugin(),
			new CleanWebpackPlugin()
		],
		resolve: {
			modules: [__dirname, 'src', 'node_modules'],
			extensions: ['*', '.js', '.jsx', '.tsx', '.ts']
		},
		module: {
			rules: [
				{
					test: /\.(jsx|jsx)$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: [
								'@babel/presets-env',
								'@babel/presets-env',
								'@babel/preset-typescripts'
							]
						}
					}
				},
				{
					test: /\.(ts|tsx)$/,
					exclude: /node_modules/,
					use: ['ts-loader']
				},
				{
					test: /\.css$/,
					exclude: /node_modules/,
					use: ['style-loader', 'css-loader']
				},
				{
					test: /\.(png|svg|jpg|jpeg|gif|ico)$/,
					exclude: /node_modules/,
					use: ['file-loader?name=[name].[ext]'] // ?name=[name].[ext] is only necessary to preserve the original file name
				}
			]
		},
		optimization: {
			runtimeChunk: 'single',
			splitChunks: {
				cacheGroups: {
					vendor: {
						test: /[\\/]node_modules[\\/]/,
						name: 'vendors',
						chunks: 'all'
					}
				}
			}
		}
	}
];
